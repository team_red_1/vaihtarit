﻿namespace DatabaseConnection.Database
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Category> Category { get; set; }
        public DbSet<GeoLocation> GeoLocation { get; set; }
        public DbSet<Item> Item { get; set; }
        public DbSet<ItemCategory> ItemCategory { get; set; }
        public DbSet<ItemImage> ItemImage { get; set; }
        public DbSet<LikedItem> LikedItem { get; set; }
        public DbSet<Match> Match { get; set; }
        public DbSet<Message> Message { get; set; }
        public DbSet<Notification> Notification { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<SoftDeleted> SoftDeleteInfo { get; set; }
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options) {}

        public bool CanConnectToDatabase()
        {
            try 
            {
                return this.Database.CanConnect();
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                return false;
            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
                .ToTable("Category");
            modelBuilder.Entity<Category>()
                .HasOne(e => e.Parent)
                .WithMany(e => e.ChildCategories);
            modelBuilder.Entity<Category>()
                .HasQueryFilter(e => EF.Property<bool>(e, "IsDeleted") == false);
            modelBuilder.Entity<Category>()
                .HasData(new Category { Id = 1, ChildCategories = null, IsDeleted = false, Title = "Muut" });

            modelBuilder.Entity<GeoLocation>()
                .ToTable("GeoLocation");
            modelBuilder.Entity<GeoLocation>()
                .HasQueryFilter(e => EF.Property<bool>(e, "IsDeleted") == false);

            modelBuilder.Entity<Item>()
                .ToTable("Item");
            modelBuilder.Entity<Item>()
                .Property(p => p.TradeCompleted)
                .HasDefaultValue(false);
            modelBuilder.Entity<Item>()
                .Property(p => p.Hidden)
                .HasDefaultValue(false);
            modelBuilder.Entity<Item>()
                .HasOne(p => p.User)
                .WithMany(p => p.Items);
            modelBuilder.Entity<Item>()
                .HasOne(p => p.GeoLocation)
                .WithOne(p => p.Item);
            modelBuilder.Entity<Item>().HasQueryFilter(e => EF.Property<bool>(e, "IsDeleted") == false);

            modelBuilder.Entity<ItemCategory>()
                .ToTable("ItemCategory");
            modelBuilder.Entity<ItemCategory>()
                .HasOne(p => p.Category)
                .WithMany(p => p.ItemCategories);
            modelBuilder.Entity<ItemCategory>()
                .HasOne(p => p.Item)
                .WithMany(p => p.ItemCategories);

            modelBuilder.Entity<ItemImage>()
                .ToTable("ItemImage");
            modelBuilder.Entity<ItemImage>()
                .HasOne(p => p.Item)
                .WithMany(p => p.ItemImages);
            modelBuilder.Entity<ItemImage>()
                .HasQueryFilter(e => EF.Property<bool>(e, "IsDeleted") == false);

            modelBuilder.Entity<LikedItem>()
                .ToTable("LikedItem");
            modelBuilder.Entity<LikedItem>()
                .Property(p => p.ItemOwnerAcknowledge)
                .HasDefaultValue(false);
            modelBuilder.Entity<LikedItem>()
                .HasOne(p => p.Item)
                .WithMany(p => p.Likes);

            modelBuilder.Entity<Match>()
                .ToTable("Match");
            modelBuilder.Entity<Match>()
                .Property(p => p.TargetOfLikeAgree)
                .HasDefaultValue(false);
            modelBuilder.Entity<Match>()
                .Property(p => p.ActiveLikerAgree)
                .HasDefaultValue(false);
            modelBuilder.Entity<Match>()
                .Property(p => p.Completed)
                .HasDefaultValue(false);
            modelBuilder.Entity<Match>()
                .HasOne(p => p.ActiveLiker)
                .WithMany(b => b.OwnedMatches);
            modelBuilder.Entity<Match>()
                .HasOne(e => e.TargetOfLike)
                .WithMany(e => e.ClientMatches);
            modelBuilder.Entity<Match>()
                .HasQueryFilter(e => EF.Property<bool>(e, "IsDeleted") == false);

            modelBuilder.Entity<Message>()
                .ToTable("Message");
            modelBuilder.Entity<Message>()
                .HasQueryFilter(e => EF.Property<bool>(e, "IsDeleted") == false);

            modelBuilder.Entity<Notification>()
                .ToTable("Notification");
            modelBuilder.Entity<Notification>()
                .HasQueryFilter(e => EF.Property<bool>(e, "IsDeleted") == false);
            modelBuilder.Entity<User>()
                .ToTable("User");
            modelBuilder.Entity<User>()
                .Property(p => p.Role)
                .HasDefaultValue(Role.User);
            modelBuilder.Entity<User>()
                .Property(p => p.Activated)
                .HasDefaultValue(false);
            modelBuilder.Entity<User>()
                .HasOne(p => p.GeoLocation)
                .WithOne(p => p.User);
            modelBuilder.Entity<User>()
                .HasQueryFilter(e => EF.Property<bool>(e, "IsDeleted") == false);

            modelBuilder.Entity<SoftDeleted>()
                .ToTable("SoftDeleteInfo");
        }
    }
}