﻿namespace DatabaseConnection.Database.Repositories
{
    public interface ISoftDeleteInfoRepository : IBaseRepository<SoftDeleted>
    {
    }
}
