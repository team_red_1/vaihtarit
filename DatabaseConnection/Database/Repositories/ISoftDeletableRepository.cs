﻿using System.Linq.Expressions;

namespace DatabaseConnection.Database.Repositories
{
    public interface ISoftDeletableRepository<T> : IBaseRepository<T>
        where T : class, ISoftDeletable, new()
    {
        bool ExistsIncludingSoftDeleted(Expression<Func<T, bool>> expression);
        Task<bool> ExistsIncludingSoftDeletedAsync(Expression<Func<T, bool>> expression);
        (bool isSuccess, T data) GetIncludingDeleted(long id);
        Task<(bool isSuccess, T data)> GetIncludingDeletedAsync(long id);
        void SoftDelete(long id);
        void SoftDelete(T item);
        void UnDelete(long id);
        void UnDelete(T item);
        (bool isSuccess, IEnumerable<T> data) QueryIncludingDeleted(Expression<Func<T, bool>> expression);
        Task<(bool isSuccess, IEnumerable<T> data)> QueryIncludingDeletedAsync(Expression<Func<T, bool>> expression);
    }
}