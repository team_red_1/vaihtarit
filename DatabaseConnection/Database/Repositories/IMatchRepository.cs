﻿namespace DatabaseConnection.Database.Repositories
{
    public interface IMatchRepository : ISoftDeletableRepository<Match>
    {
        bool OpenMatchExists(User client, User owner);
        Task<bool> OpenMatchExistsAsync(User client, User owner);
        Task<Match?> GetOpenMatch(User client, User owner);
    }
}
