﻿namespace DatabaseConnection.Database.Repositories
{
    public class ItemRepository : SoftDeletableRepository<Item>, IItemRepository
    {
        public ItemRepository(DatabaseContext databaseContext) : base(databaseContext)
        {
        }
    }
}
