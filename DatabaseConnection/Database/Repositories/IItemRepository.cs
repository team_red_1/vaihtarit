﻿namespace DatabaseConnection.Database.Repositories
{
    public interface IItemRepository : ISoftDeletableRepository<Item>
    {
    }
}
