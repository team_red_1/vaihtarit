﻿namespace DatabaseConnection.Database.Repositories
{
    public interface INotificationRepository : IBaseRepository<Notification>
    {
    }
}