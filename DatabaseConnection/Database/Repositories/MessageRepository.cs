﻿namespace DatabaseConnection.Database.Repositories
{
    public class MessageRepository : SoftDeletableRepository<Message>, IMessageRepository
    {
        public MessageRepository(DatabaseContext databaseContext) : base(databaseContext)
        {
        }
    }
}
