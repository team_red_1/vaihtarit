﻿using System.Linq.Expressions;

namespace DatabaseConnection.Database.Repositories
{
    public class SoftDeletableRepository<T> : BaseRepository<T>, ISoftDeletableRepository<T>
        where T : class, ISoftDeletable, new()
    {
        public SoftDeletableRepository(DatabaseContext databaseContext) : base(databaseContext)
        {
        }
        public void SoftDelete(T item)
        {
            item.IsDeleted = true;
            Update(item);
            SoftDeleted deleteInfo = new()
            {
                Id = item.Id,
                DeletedAt = DateTime.UtcNow
            };
            DatabaseContext.Set<SoftDeleted>().Add(deleteInfo);
            SaveChanges();
        }
        public void SoftDelete(long id)
        {
            var (isSuccess, item) = Get(id);
            if(isSuccess)
            {
                item.IsDeleted = true;
                Update(item);
                SoftDeleted deleteInfo = new()
                {
                    Id = item.Id,
                    DeletedAt = DateTime.UtcNow
                };
                DatabaseContext.Set<SoftDeleted>().Add(deleteInfo);
                SaveChanges();
            }
        }
        public void UnDelete(T item)
        {
            item.IsDeleted = false;
            Update(item);
            var deleteInfo = DatabaseContext.Set<SoftDeleted>().FirstOrDefault(e => e.Id == item.Id);
            if (deleteInfo != null)
            {
                DatabaseContext.Set<SoftDeleted>().Remove(deleteInfo);
                SaveChanges();
            }
        }
        public void UnDelete(long id)
        {
            var (isSuccess, item) = GetIncludingDeleted(id);
            if(isSuccess)
            {
                item.IsDeleted = false;
                Update(item);
                var deleteInfo = DatabaseContext.Set<SoftDeleted>().FirstOrDefault(e => e.Id == item.Id);
                if (deleteInfo != null)
                {
                    DatabaseContext.Set<SoftDeleted>().Remove(deleteInfo);
                    SaveChanges();
                }
            }
        }
        public bool ExistsIncludingSoftDeleted(Expression<Func<T, bool>> expression)
        {
            if(DatabaseContext.Set<T>().IgnoreQueryFilters().FirstOrDefault(expression) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public async Task<bool> ExistsIncludingSoftDeletedAsync(Expression<Func<T, bool>> expression)
        {
            if (await DatabaseContext.Set<T>().IgnoreQueryFilters().FirstOrDefaultAsync(expression) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public (bool isSuccess, T data) GetIncludingDeleted(long id)
        {
            var item = DatabaseContext.Set<T>().IgnoreQueryFilters().FirstOrDefault(e => e.Id == id);
            if (item == null)
            {
                return (false, new T());
            }
            return (true, item);
        }
        public async Task<(bool isSuccess, T data)> GetIncludingDeletedAsync(long id)
        {
            var item = await DatabaseContext.Set<T>().IgnoreQueryFilters().FirstOrDefaultAsync(e => e.Id == id);
            if (item == null)
            {
                return (false, new T());
            }
            return (true, item);
        }
        public (bool isSuccess, IEnumerable<T> data) QueryIncludingDeleted(Expression<Func<T, bool>> expression)
        {
            var items = DatabaseContext.Set<T>().IgnoreQueryFilters().AsNoTracking().Where(expression);
            if(items.Any())
            {
                return (true, items);
            }
            else
            {
                return (false, items);
            }
        }
        public async Task<(bool isSuccess, IEnumerable<T> data)> QueryIncludingDeletedAsync(Expression<Func<T, bool>> expression)
        {
            var items = DatabaseContext.Set<T>().IgnoreQueryFilters().AsNoTracking().Where(expression);
            if(await items.AnyAsync())
            {
                return (true, items);
            }
            else
            {
                return (false, items);
            }
        }
    }
}
