﻿namespace DatabaseConnection.Database.Repositories
{
    public class GeoLocationRepository : SoftDeletableRepository<GeoLocation>, IGeoLocationRepository
    {
        public GeoLocationRepository(DatabaseContext databaseContext) : base(databaseContext)
        {
        }
    }
}
