﻿namespace DatabaseConnection.Database.Repositories
{
    public class CategoryRepository : SoftDeletableRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(DatabaseContext databaseContext) : base(databaseContext) { }
        public (bool IsSuccess, IEnumerable<Category> Data) GetChildCategories(long id)
        {
            var (isSuccess, parent) = Get(id);
            if (isSuccess)
            {
                return GetChildCategories(parent);
            }
            return (false, new List<Category>());
        }
        public (bool IsSuccess, IEnumerable<Category> Data) GetChildCategories(Category parent)
        {
            var (childrenExists, children) = Query(x => x.ParentId == parent.Id);
            if (childrenExists)
            {
                List<Category> categories = new();
                foreach (Category child in children)
                {
                    var (childHasChildren, childsChildren) = GetChildCategories(child.Id);
                    if (childHasChildren)
                    {
                        foreach (Category c in childsChildren)
                        {
                            categories.Add(c);
                        }
                    }
                    categories.Add(child);
                }
                return (true, categories);
            }
            return (false, new List<Category>());
        }
        public async Task<(bool IsSuccess, IEnumerable<Category> Data)> GetChildCategoriesAsync(long id)
        {
            var (isSuccess, parent) = await GetAsync(id);
            if (isSuccess)
            {
                return await GetChildCategoriesAsync(parent);
            }
            return (false, new List<Category>());
        }
        public async Task<(bool IsSuccess, IEnumerable<Category> Data)> GetChildCategoriesAsync(Category parent)
        {
            var (childrenExists, children) = await QueryAsync(x => x.ParentId == parent.Id);
            if (childrenExists)
            {
                List<Category> categories = new();
                foreach (Category child in children)
                {
                    var (childHasChildren, childsChilren) = await GetChildCategoriesAsync(child);
                    if (childHasChildren)
                    {
                        foreach (Category c in childsChilren)
                        {
                            categories.Add(c);
                        }
                    }
                    categories.Add(child);
                }
                return (true, categories);
            }
            return (false, new List<Category>());
        }
        public void DeleteCategory(long id)
        {
            var (isSuccess, category) = Get(id);
            if (isSuccess)
            {
                DeleteChildCategories(category);
                Delete(category);
            }
        }
        public void DeleteCategory(Category category)
        {
            DeleteChildCategories(category);
            Delete(category);
        }
        public async Task DeleteCategoryAsync(long id)
        {
            var (isSuccess, category) = await GetAsync(id);
            if (isSuccess)
            {
                await DeleteChildCategoriesAsync(category);
                Delete(category);
            }
        }
        public async Task DeleteCategoryAsync(Category category)
        {
            await DeleteChildCategoriesAsync(category);
            Delete(category);
        }
        public void DeleteChildCategories(Category parent)
        {
            var (childrenExists, children) = GetChildCategories(parent);
            if (childrenExists)
            {
                foreach (Category child in children)
                {
                    DeleteChildCategories(child);
                    Delete(child);
                }
            }
        }
        public async Task DeleteChildCategoriesAsync(Category parent)
        {
            var (childrenExists, children) = await GetChildCategoriesAsync(parent);
            if (childrenExists)
            {
                foreach (Category child in children)
                {
                    await DeleteChildCategoriesAsync(child);
                    Delete(child);
                }
            }
        }
    }
}
