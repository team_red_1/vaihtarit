﻿namespace DatabaseConnection.Database.Repositories
{
    public class NotificationRepository : BaseRepository<Notification>, INotificationRepository
    {
        public NotificationRepository(DatabaseContext databaseContext) : base(databaseContext)
        {
        }
    }
}