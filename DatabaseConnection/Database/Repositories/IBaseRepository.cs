﻿using System.Linq.Expressions;

namespace DatabaseConnection.Database.Repositories
{
    public interface IBaseRepository<T>
        where T : class, IBaseEntity, new()
    {
        void Create(T item);
        bool Delete(long id);
        void Delete(T item);
        bool Exists(Expression<Func<T, bool>> expression);
        bool Exists(long id);
        bool Exists(T item);
        Task<bool> ExistsAsync(Expression<Func<T, bool>> expression);
        Task<bool> ExistsAsync(long id);
        Task<bool> ExistsAsync(T item);
        (bool IsSuccess, T Data) Get(Expression<Func<T, bool>> expression);
        (bool IsSuccess, T Data) Get(long id);
        (bool IsSuccess, IEnumerable<T> Data) GetAll();
        Task<(bool IsSuccess, IEnumerable<T> Data)> GetAllAsync();
        Task<(bool IsSuccess, T Data)> GetAsync(Expression<Func<T, bool>> expression);
        Task<(bool IsSuccess, T Data)> GetAsync(long id);
        bool Patch(T source);
        (bool IsSuccess, IEnumerable<T> Data) Query(Expression<Func<T, bool>> expression);
        Task<(bool IsSuccess, IEnumerable<T> Data)> QueryAsync(Expression<Func<T, bool>> expression);
        int SaveChanges();
        Task<int> SaveChangesAsync();
        void Update(T item);
        bool AnyExist();
        Task<bool> AnyExistAsync();
        T[] GetAllAsArray();
        Task<T[]> GetAllAsArrayAsync();
    }
}