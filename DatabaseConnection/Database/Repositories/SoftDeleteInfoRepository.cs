﻿namespace DatabaseConnection.Database.Repositories
{
    public class SoftDeleteInfoRepository : BaseRepository<SoftDeleted>, ISoftDeleteInfoRepository
    {
        public SoftDeleteInfoRepository(DatabaseContext databaseContext) : base(databaseContext)    {}
    }
}