﻿namespace DatabaseConnection.Database.Repositories
{
    public interface IMessageRepository : ISoftDeletableRepository<Message>
    {
    }
}
