﻿namespace DatabaseConnection.Database.Repositories
{
    public class MatchRepository : SoftDeletableRepository<Match>, IMatchRepository
    {
        public MatchRepository(DatabaseContext databaseContext) : base(databaseContext)
        {
        }

        public bool OpenMatchExists(User targetOfLike, User activeLiker)
        {
            if (Exists(x =>
                    x.Completed == false &&
                    (
                        (x.ActiveLikerUserId == activeLiker.Id && x.TargetOfLikeUserId == targetOfLike.Id) ||
                        (x.ActiveLikerUserId == targetOfLike.Id && x.TargetOfLikeUserId == activeLiker.Id))
                    ))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public async Task<bool> OpenMatchExistsAsync(User targetOfLike, User activeLiker)
        {
            if (await ExistsAsync(x =>
                    x.Completed == false &&
                    (
                        (x.ActiveLikerUserId == activeLiker.Id && x.TargetOfLikeUserId == targetOfLike.Id) ||
                        (x.ActiveLikerUserId == targetOfLike.Id && x.TargetOfLikeUserId == activeLiker.Id))
                    ))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public async Task<Match?> GetOpenMatch(User targetOfLike, User activeLiker)
        {
            var (isSuccess, matches) = await QueryAsync(x =>
                    x.Completed == false &&
                    (
                        (x.ActiveLikerUserId == activeLiker.Id && x.TargetOfLikeUserId == targetOfLike.Id) ||
                        (x.ActiveLikerUserId == targetOfLike.Id && x.TargetOfLikeUserId == activeLiker.Id))
                    );
            if (!isSuccess)
            {
                return null;
            }
            else
            {
                return matches.First();
            }
        }
    }
}
