﻿using System.Linq.Expressions;

namespace DatabaseConnection.Database.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T>
        where T : class, IBaseEntity, new()
    {
        protected DatabaseContext DatabaseContext { get; set; }
        public BaseRepository(DatabaseContext databaseContext)
        {
            DatabaseContext = databaseContext;
        }
        public (bool IsSuccess, IEnumerable<T> Data) GetAll()
        {
            bool isSuccess = false;
            IEnumerable<T> data = DatabaseContext.Set<T>().AsNoTracking().ToList();
            if (data.Any())
            {
                isSuccess = true;
            }
            return (isSuccess, data);
        }
        public async Task<(bool IsSuccess, IEnumerable<T> Data)> GetAllAsync()
        {
            bool isSuccess = false;
            IEnumerable<T> data = await DatabaseContext.Set<T>().AsNoTracking().ToListAsync();
            if (data.Any())
            {
                isSuccess = true;
            }
            return (isSuccess, data);
        }
        public (bool IsSuccess, IEnumerable<T> Data) Query(Expression<Func<T, bool>> expression)
        {
            bool isSuccess = false;
            IEnumerable<T> data = DatabaseContext.Set<T>().Where(expression).AsNoTracking().ToList();
            if (data.Any())
            {
                isSuccess = true;
            }
            return (isSuccess, data);
        }
        public async Task<(bool IsSuccess, IEnumerable<T> Data)> QueryAsync(Expression<Func<T, bool>> expression)
        {
            bool isSuccess = false;
            IEnumerable<T> data = await DatabaseContext.Set<T>().Where(expression).AsNoTracking().ToListAsync();
            if (data.Any())
            {
                isSuccess = true;
            }
            return (isSuccess, data);
        }
        public (bool IsSuccess, T Data) Get(long id)
        {
            try
            {
                T data = DatabaseContext.Set<T>().AsNoTracking().FirstOrDefault(entity => entity.Id == id);
                if (data != null)
                {
                    return (true, data);
                }
                return (false, new T());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                return (false, new T());
            }

        }
        public (bool IsSuccess, T Data) Get(Expression<Func<T, bool>> expression)
        {
            try
            {
                T data = DatabaseContext.Set<T>().AsNoTracking().FirstOrDefault(expression);
                if (data != null)
                {
                    return (true, data);
                }
                return (false, new T());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                return (false, new T());
            }

        }
        public async Task<(bool IsSuccess, T Data)> GetAsync(long id)
        {
            try
            {
                T data = await DatabaseContext.Set<T>().AsNoTracking().FirstOrDefaultAsync(entity => entity.Id == id);
                if (data != null)
                {
                    return (true, data);
                }
                return (false, new T());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                return (false, new T());
            }

        }
        public async Task<(bool IsSuccess, T Data)> GetAsync(Expression<Func<T, bool>> expression)
        {
            try
            {
                T data = await DatabaseContext.Set<T>().AsNoTracking().FirstOrDefaultAsync(expression);
                if (data != null)
                {
                    return (true, data);
                }
                return (false, new T());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                return (false, new T());
            }

        }
        public bool Exists(long id)
        {
            if (DatabaseContext.Set<T>().FirstOrDefault(entity => entity.Id == id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool Exists(Expression<Func<T, bool>> expression)
        {
            if (DatabaseContext.Set<T>().FirstOrDefault(expression) != null)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public bool Exists(T item)
        {
            if (DatabaseContext.Set<T>().FirstOrDefault(entity => entity.Id == item.Id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public async Task<bool> ExistsAsync(long id)
        {
            if (await DatabaseContext.Set<T>().FirstOrDefaultAsync(entity => entity.Id == id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public async Task<bool> ExistsAsync(Expression<Func<T, bool>> expression)
        {
            if (await DatabaseContext.Set<T>().FirstOrDefaultAsync(expression) != null)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public async Task<bool> ExistsAsync(T item)
        {
            if (await DatabaseContext.Set<T>().FirstOrDefaultAsync(entity => entity.Id == item.Id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void Create(T item)
        {
            DatabaseContext.Set<T>().Add(item);
        }
        public void Update(T item)
        {
            DatabaseContext.Set<T>().Update(item);
            SaveChanges();
        }
        public bool Patch(T source)
        {
            var (isSuccess, updatable) = Get(source.Id);
            if (isSuccess)
            {
                try
                {
                    var caster = new EntityCaster<T, T>();
                    updatable = caster.CastTo(updatable, source, false, true);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.StackTrace);
                    return false;
                }
                DatabaseContext.Set<T>().Update(updatable);
                SaveChanges();
            }
            return isSuccess;
        }
        public void Delete(T item)
        {
            DatabaseContext.Remove(item);
        }
        public bool Delete(long id)
        {
            var (isSuccess, item) = Get(id);
            if (isSuccess == true)
            {
                Delete(item);
            }
            return isSuccess;
        }
        public int SaveChanges()
        {
            return DatabaseContext.SaveChanges();
        }
        public async Task<int> SaveChangesAsync()
        {
            return await DatabaseContext.SaveChangesAsync();
        }
        public bool AnyExist()
        {
            return DatabaseContext.Set<T>().Any();
        }
        public async Task<bool> AnyExistAsync()
        {
            return await DatabaseContext.Set<T>().AnyAsync();
        }
        public T[] GetAllAsArray()
        {
            return DatabaseContext.Set<T>().AsNoTracking().ToArray();
        }
        public async Task<T[]> GetAllAsArrayAsync()
        {
            return await DatabaseContext.Set<T>().AsNoTracking().ToArrayAsync();
        }
    }
}