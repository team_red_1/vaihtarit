﻿namespace DatabaseConnection.Database.Repositories
{
    public class UserRepository : SoftDeletableRepository<User>, IUserRepository
    {
        public UserRepository(DatabaseContext databaseContext) : base(databaseContext) { }
        public (bool IsSuccess, User User) GetByUsername(string username)
        {
            var (isSuccess, data) = Query(x => x.Username == username);
            if (isSuccess)
            {
                return (isSuccess, data.ElementAt(0));
            }
            return (false, new User());
        }
        public async Task<(bool IsSuccess, User User)> GetByUsernameAsync(string username)
        {
            var (isSuccess, data) = await QueryAsync(x => x.Username == username);
            if (isSuccess)
            {
                return (isSuccess, data.ElementAt(0));
            }
            return (false, new User());
        }
        public (bool IsSuccess, User User) GetByEmail(string email)
        {
            var (isSuccess, data) = Query(x => x.Email == email);
            if (isSuccess)
            {
                return (isSuccess, data.First());
            }
            return (isSuccess, new User());
        }
        public async Task<(bool IsSuccess, User User)> GetByEmailAsync(string email)
        {
            var (isSuccess, data) = await QueryAsync(x => x.Email == email);
            if (isSuccess)
            {
                return (isSuccess, data.First());
            }
            return (false, new User());
        }
        public (bool IsSuccess, User User) GetByVerificationToken(string verificationToken)
        {
            var (isSuccess, data) = Query(x => x.VerificationToken == verificationToken);
            if (isSuccess)
            {
                return (isSuccess, data.ElementAt(0));
            }
            return (false, new User());
        }
        public async Task<(bool IsSuccess, User User)> GetByVerificationTokenAsync(string verificationToken)
        {
            var (isSuccess, data) = await QueryAsync(x => x.VerificationToken == verificationToken);
            if (isSuccess)
            {
                return (isSuccess, data.ElementAt(0));
            }
            return (false, new User());
        }
        public bool ExistsByUsername(string username)
        {
            return ExistsIncludingSoftDeleted(x => x.Username == username);
        }
        public async Task<bool> ExistsByUsernameAsync(string username)
        {
            return await ExistsIncludingSoftDeletedAsync(x => x.Username == username);
        }
        public bool ExistsByEmail(string email)
        {
            return ExistsIncludingSoftDeleted(x => x.Email == email);
        }
        public async Task<bool> ExistsByEmailAsync(string email)
        {
            return await ExistsIncludingSoftDeletedAsync(x => x.Email == email);
        }
    }
}
