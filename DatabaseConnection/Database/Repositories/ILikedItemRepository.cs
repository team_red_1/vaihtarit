﻿namespace DatabaseConnection.Database.Repositories
{
    public interface ILikedItemRepository : IBaseRepository<LikedItem>
    {
        (bool IsSuccess, LikedItem LikedItem) GetByUserId(long userId);
    }
}