﻿namespace DatabaseConnection.Database.Repositories
{
    public interface IItemImageRepository : ISoftDeletableRepository<ItemImage>
    {
    }
}
