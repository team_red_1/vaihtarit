﻿namespace DatabaseConnection.Database.Repositories
{
    public interface IItemCategoryRepository : IBaseRepository<ItemCategory>
    {
    }
}
