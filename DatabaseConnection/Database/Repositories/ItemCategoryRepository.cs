﻿namespace DatabaseConnection.Database.Repositories
{
    public class ItemCategoryRepository : BaseRepository<ItemCategory>, IItemCategoryRepository
    {
        public ItemCategoryRepository(DatabaseContext databaseContext) : base(databaseContext)
        {
        }
    }
}
