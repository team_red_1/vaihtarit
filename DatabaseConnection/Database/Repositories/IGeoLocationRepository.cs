﻿namespace DatabaseConnection.Database.Repositories
{
    public interface IGeoLocationRepository : ISoftDeletableRepository<GeoLocation>
    {
    }
}
