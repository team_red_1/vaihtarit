﻿namespace DatabaseConnection.Database.Repositories
{
    public class ItemImageRepository : SoftDeletableRepository<ItemImage>, IItemImageRepository
    {
        public ItemImageRepository(DatabaseContext databaseContext) : base(databaseContext)
        {
        }
    }
}
