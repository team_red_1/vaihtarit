﻿namespace DatabaseConnection.Database.Repositories
{
    public interface IUserRepository : ISoftDeletableRepository<User>
    {
        bool ExistsByEmail(string email);
        Task<bool> ExistsByEmailAsync(string email);
        bool ExistsByUsername(string username);
        Task<bool> ExistsByUsernameAsync(string username);
        (bool IsSuccess, User User) GetByEmail(string email);
        Task<(bool IsSuccess, User User)> GetByEmailAsync(string email);
        (bool IsSuccess, User User) GetByUsername(string username);
        Task<(bool IsSuccess, User User)> GetByUsernameAsync(string username);
        (bool IsSuccess, User User) GetByVerificationToken(string verificationToken);
        Task<(bool IsSuccess, User User)> GetByVerificationTokenAsync(string verificationToken);
    }
}