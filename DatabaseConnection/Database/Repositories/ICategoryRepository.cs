﻿namespace DatabaseConnection.Database.Repositories
{
    public interface ICategoryRepository : ISoftDeletableRepository<Category>
    {
        void DeleteCategory(long id);
        Task DeleteCategoryAsync(Category category);
        void DeleteCategory(Category category);
        Task DeleteCategoryAsync(long id);
        void DeleteChildCategories(Category parent);
        Task DeleteChildCategoriesAsync(Category parent);
        (bool IsSuccess, IEnumerable<Category> Data) GetChildCategories(Category parent);
        (bool IsSuccess, IEnumerable<Category> Data) GetChildCategories(long id);
        Task<(bool IsSuccess, IEnumerable<Category> Data)> GetChildCategoriesAsync(Category parent);
        Task<(bool IsSuccess, IEnumerable<Category> Data)> GetChildCategoriesAsync(long id);
    }
}