﻿namespace DatabaseConnection.Database.Repositories
{
    public class LikedItemRepository : BaseRepository<LikedItem>, ILikedItemRepository
    {
        public LikedItemRepository(DatabaseContext databaseContext) : base(databaseContext) {}
        public (bool IsSuccess, LikedItem LikedItem) GetByUserId(long userId)
        {
            var (isSuccess, data) = Query(x => x.UserId == userId);
            if (isSuccess)
            {
                return (isSuccess, data.ElementAt(0));
            }
            return (false, new LikedItem());
        }
    }
}
