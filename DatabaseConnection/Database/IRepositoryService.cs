﻿namespace DatabaseConnection.Database
{
    public interface IRepositoryService
    {
        ICategoryRepository CategoryRepository { get; }
        IGeoLocationRepository GeoLocationRepository { get; }
        IItemCategoryRepository ItemCategoryRepository { get; }
        IItemImageRepository ItemImageRepository { get; }
        IItemRepository ItemRepository { get; }
        ILikedItemRepository LikedItemRepository { get; }
        IMatchRepository MatchRepository { get; }
        IMessageRepository MessageRepository { get; }
        INotificationRepository NotificationRepository { get; }
        ISoftDeleteInfoRepository SoftDeleteInfoRepository { get; }
        IUserRepository UserRepository { get; }
    }
}