﻿namespace DatabaseConnection.Database
{
    public class RepositoryService : IRepositoryService
    {
        private readonly DatabaseContext _databaseContext;

        private ICategoryRepository? _categoryRepository;
        private IGeoLocationRepository? _geoLocationRepository;
        private IItemRepository? _itemRepository;
        private IItemCategoryRepository? _itemCategoryRepository;
        private IItemImageRepository? _itemImageRepository;
        private ILikedItemRepository? _likedItemRepository;
        private IMatchRepository? _matchRepository;
        private IMessageRepository? _messageRepository;
        private INotificationRepository? _notificationRepository;
        private ISoftDeleteInfoRepository? _softDeleteInfoRepository;
        private IUserRepository? _userRepository;

        public RepositoryService(DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }
        public ICategoryRepository CategoryRepository
        {
            get
            {
                if (_categoryRepository == null)
                {
                    _categoryRepository = new CategoryRepository(_databaseContext);
                }
                return _categoryRepository;
            }
        }
        public IGeoLocationRepository GeoLocationRepository
        {
            get
            {
                if (_geoLocationRepository == null)
                {
                    _geoLocationRepository = new GeoLocationRepository(_databaseContext);
                }
                return _geoLocationRepository;
            }
        }
        public IItemRepository ItemRepository
        {
            get
            {
                if (_itemRepository == null)
                {
                    _itemRepository = new ItemRepository(_databaseContext);
                }
                return _itemRepository;
            }
        }
        public IItemCategoryRepository ItemCategoryRepository
        {
            get
            {
                if (_itemCategoryRepository == null)
                {
                    _itemCategoryRepository = new ItemCategoryRepository(_databaseContext);
                }
                return _itemCategoryRepository;
            }
        }
        public IItemImageRepository ItemImageRepository
        {
            get
            {
                if (_itemImageRepository == null)
                {
                    _itemImageRepository = new ItemImageRepository(_databaseContext);
                }
                return _itemImageRepository;
            }
        }
        public ILikedItemRepository LikedItemRepository
        {
            get
            {
                if (_likedItemRepository == null)
                {
                    _likedItemRepository = new LikedItemRepository(_databaseContext);
                }
                return _likedItemRepository;
            }
        }
        public IMatchRepository MatchRepository
        {
            get
            {
                if (_matchRepository == null)
                {
                    _matchRepository = new MatchRepository(_databaseContext);
                }
                return _matchRepository;
            }
        }
        public IMessageRepository MessageRepository
        {
            get
            {
                if (_messageRepository == null)
                {
                    _messageRepository = new MessageRepository(_databaseContext);
                }
                return _messageRepository;
            }
        }
        public INotificationRepository NotificationRepository
        {
            get
            {
                if (_notificationRepository == null)
                {
                    _notificationRepository = new NotificationRepository(_databaseContext);
                }
                return _notificationRepository;
            }
        }
        public ISoftDeleteInfoRepository SoftDeleteInfoRepository
        {
            get
            {
                if (_softDeleteInfoRepository == null)
                {
                    _softDeleteInfoRepository = new SoftDeleteInfoRepository(_databaseContext);
                }
                return _softDeleteInfoRepository;
            }
        }
        public IUserRepository UserRepository
        {
            get
            {
                if (_userRepository == null)
                {
                    _userRepository = new UserRepository(_databaseContext);
                }
                return _userRepository;
            }
        }
    }
}