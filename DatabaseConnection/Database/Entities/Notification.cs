﻿namespace DatabaseConnection.Database.Entities
{
    public class Notification : ISoftDeletable
    {
        [Required]
        public long Id { get; set; }
        public int TypeOfMessage { get; set; }
        public long AccountHolderUserId { get; set; }
        public long OtherPersonUserId { get; set; }
        public long LikedItemId { get; set; }
        public long MatchId { get; set; }
        public bool IsSeen { get; set; } = false;
        public bool IsDeleted { get; set; } = false;
    }
}