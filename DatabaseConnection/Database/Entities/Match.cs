﻿namespace DatabaseConnection.Database.Entities
{
    public class Match : ISoftDeletable
    {
        [Required]
        public long Id { get; set; }
        [Required]
        public long ActiveLikerUserId { get; set; }
        [Required]
        public long TargetOfLikeUserId { get; set; }
        [Required]
        public bool ActiveLikerAgree { get; set; }
        [Required]
        public bool TargetOfLikeAgree { get; set; }
        [Required]
        public bool Completed { get; set; }
        [Required]
        public bool IsDeleted { get; set; } = false;
        public virtual User? ActiveLiker { get; set; }
        public virtual User? TargetOfLike { get; set; }
    }
}