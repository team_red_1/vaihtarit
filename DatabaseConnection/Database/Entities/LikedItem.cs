﻿namespace DatabaseConnection.Database.Entities
{
    public class LikedItem : IBaseEntity
    {
        [Required]
        public long Id { get; set; }
        [Required]
        public long UserId { get; set; }
        [Required]
        public long ItemId { get; set; }
        [Required]
        public bool ItemOwnerAcknowledge { get; set; }
        public virtual User? User { get; set; }
        public virtual Item? Item { get; set; }
    }
}