﻿namespace DatabaseConnection.Database.Entities
{
    public class ItemCategory : IBaseEntity
    {
        [Required]
        public long Id { get; set; }
        [Required]
        public long ItemId { get; set; }
        [Required]
        public long CategoryId { get; set; }
        public virtual Category? Category { get; set; }
        public virtual Item? Item { get; set; }
    }
}