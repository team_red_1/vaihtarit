﻿namespace DatabaseConnection.Database.Entities
{
    public interface ISoftDeletable : IBaseEntity
    {
        public bool IsDeleted { get; set; }
    }
}
