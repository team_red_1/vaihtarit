﻿namespace DatabaseConnection.Database.Entities
{
    public class Item : ISoftDeletable
    {
        [Required]
        public long Id { get; set; }
        [Required]
        public long UserId { get; set; }
        public DateTime? EditedOn { get; set; }
        [Required]
        public string? Title { get; set; }
        //[Required]  Commented out for now, will be required in final version.
        public string? MainImageLink { get; set; }
        [Required]
        [MaxLength(500)]
        public string? Description { get; set; }
        public short Condition { get; set; }
        public string? ConditionDescription { get; set; }
        public short YearOfPurchase { get; set; }
        public short Value { get; set; }
        [Required]
        public bool Hidden { get; set; } = false;
        [Required]
        public bool TradeCompleted { get; set; } = false;
        public short DeliveryMethod { get; set; }
        [Required]
        public bool IsDeleted { get; set; } = false;
        public virtual User? User { get; set; }
        public virtual GeoLocation? GeoLocation { get; set; }
        public virtual ICollection<ItemCategory>? ItemCategories { get; set; }
        public virtual ICollection<ItemImage>? ItemImages { get; set; }
        public virtual ICollection<LikedItem>? Likes { get; set; }
    }
}