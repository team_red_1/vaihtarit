﻿namespace DatabaseConnection.Database.Entities
{
    public class ItemImage : ISoftDeletable
    {
        [Required]
        public long Id { get; set; }
        [Required]
        public long ItemId { get; set; }
        [Required]
        public string ImageLink { get; set; } = String.Empty;
        [Required]
        public bool IsDeleted { get; set; } = false;
        public virtual Item? Item { get; set; }
    }
}