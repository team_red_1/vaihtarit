﻿namespace DatabaseConnection.Database.Entities
{
    public interface IBaseEntity
    {
        public long Id { get; set; }
    }
}