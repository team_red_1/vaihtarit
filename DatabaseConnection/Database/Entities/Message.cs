﻿namespace DatabaseConnection.Database.Entities
{
    public class Message : ISoftDeletable
    {
        [Required]
        public long Id { get; set; }
        [Required]
        public long MatchId { get; set; }
        [Required]
        public long SenderId { get; set; }
        [Required]
        public long ReceiverId { get; set; }
        [Required]
        public string MessageText { get; set; } = String.Empty;
        [Required]
        public bool IsDeleted { get; set; } = false;
        public virtual Match? Match { get; set; }
        public virtual User? Receiver { get; set; }
    }
}