﻿namespace DatabaseConnection.Database.Entities
{
    public class User : ISoftDeletable
    {
        [Required]
        public long Id { get; set; }
        [Required]
        public string Username { get; set; } = String.Empty;
        [Required]
        [EmailAddress]
        public string Email { get; set; } = String.Empty;
        [Required]
        public Role Role { get; set; }
        public string? VerificationToken { get; set; }
        public bool Activated { get; set; } = false;
        public string? UserImage { get; set; }
        [JsonIgnore]
        [Required]
        public string PasswordHash { get; set; } = String.Empty;
        [Required]
        public bool IsDeleted { get; set; } = false;
        public virtual GeoLocation? GeoLocation { get; set; }
        public virtual ICollection<Item>? Items { get; set; }
        public virtual ICollection<LikedItem>? LikedItems { get; set; }
        public virtual ICollection<Match>? OwnedMatches { get; set; }
        public virtual ICollection<Match>? ClientMatches { get; set; }
        public virtual ICollection<Message>? Messages { get; set; }
    }
}