﻿namespace DatabaseConnection.Database.Entities
{
    public class SoftDeleted : IBaseEntity
    {
        public long Id { get; set; }
        public DateTime DeletedAt { get; set; } = DateTime.UtcNow;
    }
}
