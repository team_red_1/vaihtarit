﻿namespace DatabaseConnection.Database.Entities
{
    public class GeoLocation : ISoftDeletable
    {
        [Required]
        public long Id { get; set; }
        public long? UserId { get; set; }
        public long? ItemId { get; set; }
        [Required]
        [Range(-180, 180)]

        public double Longitude { get; set; }
        [Required]
        [Range(-90, 90)]
        public double Latitude { get; set; }
        [Required]
        public bool IsDeleted { get; set; } = false;
        public virtual Item? Item { get; set; }
        public virtual User? User { get; set; }
    }
}