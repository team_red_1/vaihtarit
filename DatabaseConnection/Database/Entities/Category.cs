﻿namespace DatabaseConnection.Database.Entities
{
    public class Category : ISoftDeletable
    {
        [Required]
        public long Id { get; set; }
        public long? ParentId { get; set; }
        [Required]
        public string Title { get; set; } = String.Empty;
        [Required]
        public bool IsDeleted { get; set; } = false;
        public virtual Category? Parent { get; set; }
        public virtual ICollection<ItemCategory>? ItemCategories { get; set; }
        public virtual ICollection<Category>? ChildCategories { get; set; }
    }
}