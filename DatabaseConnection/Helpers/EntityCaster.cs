﻿namespace DatabaseConnection.Helpers
{
    public class EntityCaster<T, U>
        where T : class, new()
        where U : class, new()
    {
        public T CastTo(T destination, U source, bool overwriteWithNull = false, bool overwriteWithEmptyString = true)
        {
            var isExistingBaseEntity = IsExistingBaseEntity(destination, out long id);
            foreach (var property in source.GetType().GetProperties())
            {
                var sourceValue = property.GetValue(source);
                if (!overwriteWithNull && sourceValue == null)
                {
                    continue;
                }
                var destinationProperty = destination.GetType().GetProperty(property.Name);
                if (destinationProperty == null || destinationProperty.PropertyType != property.PropertyType)
                {
                    continue;
                }
                if (!overwriteWithEmptyString && sourceValue.GetType() == typeof(string))
                {
                    if (string.IsNullOrWhiteSpace((string?)sourceValue))
                    {
                        continue;
                    }
                }
                var destinationValue = destinationProperty.GetValue(destination);
                if (destinationValue == null || destinationValue != sourceValue)
                {
                    destinationProperty.SetValue(destination, sourceValue);
                }
            }
            if(isExistingBaseEntity)
            {
                destination.GetType().GetProperty("Id").SetValue(destination, id);
            }

            return destination;
        }
        private bool IsExistingBaseEntity(T destination, out long id)
        {
            id = 0;
            if(!(destination is IBaseEntity))
            {
                return false;
            }
            if(destination.GetType().GetProperty("Id") == null)
            {
                return false;
            }
            var temp = (long)destination.GetType().GetProperty("Id").GetValue(destination);
            if (temp < 1)
            {
                return false;
            }
            id = temp;
            return true;
        }
    }
}
