﻿namespace DatabaseConnection.Enums
{
    public enum Role
    {
        User,
        Admin
    }
}