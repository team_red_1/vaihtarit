﻿global using DatabaseConnection.Database.Entities;
global using DatabaseConnection.Database.Repositories;
global using DatabaseConnection.Enums;
global using DatabaseConnection.Helpers;
global using Microsoft.EntityFrameworkCore;
global using System.ComponentModel.DataAnnotations;
global using System.Text.Json.Serialization;