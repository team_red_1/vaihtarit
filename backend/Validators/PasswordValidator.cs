﻿namespace backend.Validators
{
    public class ValidPassword : ValidationAttribute
    {
        private bool _upper;
        private bool _lower;
        private bool _digit;
        private bool _special;

        public ValidPassword(bool ignoreUpper = false, bool ignoreLower = false, bool ignoreDigit = false, bool ignoreSpecial = false)
        {
            _upper = ignoreUpper;
            _lower = ignoreLower;
            _digit = ignoreDigit;
            _special = ignoreSpecial;
        }
        public override bool IsValid(object? value)
        {
            if (value == null)
                return false;
            var password = value.ToString();
            if (string.IsNullOrEmpty(password))
                return false;
            foreach (char c in password)
            {
                if (char.IsUpper(c))
                    _upper = true;
                if (char.IsLower(c))
                    _lower = true;
                if (char.IsNumber(c))
                    _digit = true;
                if (char.IsSymbol(c) || char.IsPunctuation(c))
                    _special = true;
            }
            if(_upper && _lower && _digit && _special)
            {
                return true;
            }
            else
            {
                ErrorMessage = "";
                if (!_upper)
                {
                    ErrorMessage += ValidationError.MustIncludeUppercase.GetDisplayName();
                }
                if (!_lower)
                {
                    ErrorMessage += ValidationError.MustIncludeLowercase.GetDisplayName();
                }
                if (!_digit)
                {
                    ErrorMessage += ValidationError.MustIncludeDigit.GetDisplayName();
                }
                if (!_special)
                {
                    ErrorMessage += ValidationError.MustIncludeSpecialCharacter.GetDisplayName();
                }
                return false;
            }
        }
    }
}
