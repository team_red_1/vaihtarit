﻿namespace backend.Dtos.LikedItem
{
    public class LikedItemCreateDto
    {
        [Required]
        public long UserId { get; set; }
        [Required]
        public long ItemId { get; set; }
        [Required]
        public bool ItemOwnerAcknowledge { get; set; }
    }
}
