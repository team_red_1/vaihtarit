﻿namespace backend.Dtos.LikedItem
{
    public class CheckIfLikedDto
    {
        [Required]
        public long ItemId { get; set; }
    }
}