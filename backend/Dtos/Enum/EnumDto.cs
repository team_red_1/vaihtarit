﻿namespace backend.Dtos.Enum
{
    public class EnumDto
    {
        public short Id { get; set; }
        public string? Title { get; set; }
    }
}