﻿namespace backend.Dtos.Item
{
    public class ItemGetDto
    {
        public long? Newest { get; set; }
        public long? Oldest { get; set; }
    }
}
