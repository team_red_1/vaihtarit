﻿namespace backend.Dtos.Item
{
    public class ItemCreateDto
    {
        [Required]
        public string? Title { get; set; }
        [Required]
        public string? Description { get; set; }
        public short Condition { get; set; }
        public string? ConditionDescription { get; set; }
        public short Value { get; set; }
        public long Category { get; set; }
        public string? Images { get; set; }
        public Coordinates? Coordinates { get; set; }
        public bool Hidden { get; set; } = false;
    }
}
