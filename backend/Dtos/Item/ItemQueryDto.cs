﻿namespace backend.Dtos.Item
{
    public class ItemQueryDto
    {
        public long? Newest { get; set; }
        public long? Oldest { get; set; }
        [Range(0, 10000)]
        public int? Distance { get; set; }
        [Range(0, 7)]
        public short? MinValue { get; set; }
        [Range(0, 7)]
        public short? MaxValue { get; set; }
        public short? MinCondition { get; set; }
        public short? MaxCondition { get; set; }
        public long? Category { get; set; }
        public string? SearchString { get; set; }
        public bool? IncludeDescription { get; set; }
    }
}
