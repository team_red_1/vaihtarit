﻿namespace backend.Dtos.Item
{
    public class ItemGetResponseDto
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public string? Username { get; set; }
        public string? Title { get; set; }
        public string? Description { get; set; }
        public string? Condition { get; set; }
        public string? ConditionDescription { get; set; }
        public short YearOfPurchase { get; set; }
        public string? Value { get; set; }
        public short DeliveryMethod { get; set; }
        public string? Category { get; set; }
        public long[] AdditionalImages { get; set; } = { };
    }
}
