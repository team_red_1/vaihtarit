﻿namespace backend.Dtos.Item
{
    public class ItemCreateDataResponseDto
    {
        public CategoryGetWithoutParentDto[]? Categories {get; set;}
        public EnumDto[]? Conditions { get; set; }
        public EnumDto[]? RoughValues { get; set; }
    }
}
