﻿namespace backend.Dtos.Item
{
    public class ItemPatchDto
    {
        [Required]
        public long Id { get; set; }
        public long GeoLocationId { get; set; }
        public string? Title { get; set; }
        [MaxLength(500)]
        public string? Description { get; set; }
        public short Condition { get; set; }
        public string? ConditionDescription { get; set; }
        public short YearOfPurchase { get; set; }
        public short Value { get; set; }
        public bool Hidden { get; set; } = false;
        public short DeliveryMethod { get; set; }
    }
}
