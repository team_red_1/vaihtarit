﻿namespace backend.Dtos.Category
{
    public class CategoryGetDto
    {
        public long Id { get; set; }
        public long ParentId { get; set; }
        public string? Title { get; set; }
    }
}
