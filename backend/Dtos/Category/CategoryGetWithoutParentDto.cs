﻿namespace backend.Dtos.Category
{
    public class CategoryGetWithoutParentDto
    {
        public long Id { get; set; }
        public string? Title { get; set; }
    }
}
