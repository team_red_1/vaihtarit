﻿namespace backend.Dtos.Category
{
    public class CategoryUpdateDto
    {
        [Required]
        public long Id { get; set; }
        public long ParentId { get; set; }
        public string? Title { get; set; }
    }
}
