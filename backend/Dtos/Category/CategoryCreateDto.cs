﻿namespace backend.Dtos.Category
{
    public class CategoryCreateDto
    {
        public long ParentId { get; set; }
        [Required]
        public string? Title { get; set; }
    }
}
