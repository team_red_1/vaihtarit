﻿namespace backend.Dtos.Message
{
    public class MessageGetMatchMessagesDto
    {
        [Required]
        public long UserId { get; set; }
        [Required]
        public long MatchId { get; set; }
    }
}
