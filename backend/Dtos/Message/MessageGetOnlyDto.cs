﻿namespace backend.Dtos.Message
{
    public class MessageGetOnlyDto
    {
        [Required]
        public long UserId { get; set; }
        [Required]
        public long MatchId { get; set; }
    }
}