﻿namespace backend.Dtos.Message
{
    public class MessageDeleteDto
    {
        [Required]
        public long MessageId { get; set; }
    }
}