﻿namespace backend.Dtos.Message
{
    public class MessageCreateDto
    {
        [Required]
        public long UserId { get; set; }
        [Required]
        public long MatchId { get; set; }
        [Required]
        public string MessageText { get; set; }
    }
}