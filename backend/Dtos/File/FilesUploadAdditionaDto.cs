﻿namespace backend.Dtos.File
{
    public class FilesUploadAdditionaDto
    {
        [Required]
        public string? Guid { get; set; }
        [Required]
        public IFormFileCollection? Files { get; set; }
    }
}
