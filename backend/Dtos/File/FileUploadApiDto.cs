﻿namespace backend.Dtos.File
{
    public class FileUploadApiDto
    {
        public IFormFile? File { get; set; }
    }
}
