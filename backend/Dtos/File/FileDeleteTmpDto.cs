﻿namespace backend.Dtos.File
{
    public class FileDeleteTmpDto
    {
        [Required]
        public string? Guid { get; set; }
        public int? Id { get; set; }
    }
}
