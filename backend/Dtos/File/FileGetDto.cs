﻿namespace backend.Dtos.File
{
    public class FileGetTmpDto
    {
        [Required]
        public string? Guid { get; set; }
        [Required]
        [Range(0, 9)]
        public int FileId { get; set; }
    }
}
