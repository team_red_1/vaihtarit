﻿namespace backend.Dtos.File
{
    public class FilesUploadApiDto
    {
        public IFormFileCollection? Files { get; set; }
    }
}
