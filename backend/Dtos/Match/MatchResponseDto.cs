﻿namespace backend.Dtos.Match
{
    public class MatchResponseDto
    {
        public long Id { get; set; }
        public long ActiveLikerUserId { get; set; }
        public long TargetOfLikeUserId { get; set; }
        public string ActiveLikerUsername { get; set; }
        public string TargetOfLikeUsername { get; set; }
        public bool ActiveLikerAgree { get; set; }
        public bool TargetOfLikeAgree { get; set; }
        public bool Completed { get; set; }
        public bool IsDeleted { get; set; } = false;
    }
}