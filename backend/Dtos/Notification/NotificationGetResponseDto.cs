﻿namespace backend.Dtos.Notification
{
    public class NotificationGetResponseDto
    {
        public long Id { get; set; }
        public int TypeOfMessage { get; set; }
        public long AccountHolderUserId { get; set; }
        public string AccountHolderUserName { get; set; }
        public long OtherPersonUserId { get; set; }
        public string OtherPersonUsername { get; set; }
        public long LikedItemId { get; set; }
        public long MatchId { get; set; }
        public bool IsSeen { get; set; }
    }
}