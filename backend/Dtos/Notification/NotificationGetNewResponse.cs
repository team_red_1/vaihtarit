﻿namespace backend.Dtos.Notification
{
    public class NotificationGetNewResponse
    {
        public List<NotificationGetResponseDto> ListOfNotifications { get; set; }
        public int NumberOfNewNotifications { get; set; }
    }
}