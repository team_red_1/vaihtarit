﻿namespace backend.Dtos.Notification
{
    public class NotificationGetDto
    {
        public long UserId { get; set; }
        public long? Newest { get; set; }
        public long? Oldest { get; set; }
    }
}