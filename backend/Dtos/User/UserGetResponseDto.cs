﻿namespace backend.Dtos.User
{
    public class UserGetResponseDto
    {
        public long Id { get; set; }
        public string? Username { get; set; }
        public string? Email { get; set; }
        public Role Role { get; set; }
        public string? UserImage { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
