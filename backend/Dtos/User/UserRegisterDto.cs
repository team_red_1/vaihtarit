﻿using backend.Validators;

﻿namespace backend.Dtos.User
{
    public class UserRegisterDto
    {
        [Required]
        [StringLength(32, MinimumLength = 2, ErrorMessage = "Username length must be between 2-32.")]
        public string? Username { get; set; }
        [Required]
        [StringLength(64, MinimumLength = 6, ErrorMessage = "Password length must be between 6-64.")]
        [ValidPassword(false, false, true, true)]
        public string? Password { get; set; }
        [Required]
        [EmailAddress]
        public string? Email { get; set; }
    }
}