namespace backend.Dtos.User
    // @ToDo Do we accept Username or Email (or both) when logging in?﻿
{
    public class UserAuthenticateRequest
    {
        [Required]
        public string? UsernameOrEmail { get; set; }
        [Required]
        public string? Password { get; set; }
    }
}