﻿using backend.Validators;

namespace backend.Dtos.User
{
    public class UserChangePasswordDto
    {
        [Required]
        public long Id { get; set; }
        [Required]
        public string? OldPassword { get; set; }
        [Required]
        [StringLength(64, MinimumLength = 6, ErrorMessage = "Password length must be between 6-64.")]
        [ValidPassword(false, false, true, true)]
        public string? NewPassword { get; set; }
    }
}
