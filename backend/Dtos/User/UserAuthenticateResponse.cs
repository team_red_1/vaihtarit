﻿namespace backend.Dtos.User
    // @ToDo finalize which attributes are needed
{
    public class UserAuthenticateResponse
    {
        public long Id { get; set; }
        public string? Username { get; set; }
        public string? Email { get; set; }
        public string? Token { get; set; }
    }
}