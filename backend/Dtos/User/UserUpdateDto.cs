﻿using backend.Validators;

namespace backend.Dtos.User
{
    public class UserUpdateDto
    {
        [Required]
        public long Id { get; set; }
        [StringLength(32, MinimumLength = 2, ErrorMessage = "Username length must be between 2-32.")]
        public string? Username { get; set; }
        [EmailAddress]
        public string? Email { get; set; }
    }
}