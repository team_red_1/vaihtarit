﻿namespace backend.Dtos.User
{
    public class UserDeleteDto
    {
        [Required]
        public string? Password { get; set; }
    }
}
