﻿using System.Reflection;

namespace backend.Extensions
{
    public static class EnumExtensions
    {
        public static string GetDisplayName(this Enum enumValue)
        {
            string? displayName;
            displayName = enumValue.GetType()
                .GetMember(enumValue.ToString())
                .First()
                .GetCustomAttribute<DisplayAttribute>()?
                .GetName();
            if(string.IsNullOrEmpty(displayName))
            {
                return enumValue.ToString();
            }
            return displayName;
        }
    }
}