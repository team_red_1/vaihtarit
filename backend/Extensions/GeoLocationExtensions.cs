﻿namespace backend.Extensions
{
    public static class GeoLocationExtensions
    {
        public static Coordinates GetCoordinates(this GeoLocation geoLocation)
        {
            var coordinates = new Coordinates
            {
                Latitude = geoLocation.Latitude,
                Longitude = geoLocation.Longitude
            };
            return coordinates;
        }
        public static int GetDistanceBetweenGeolocations(this GeoLocation geoLocation, GeoLocation distanceToThis)
        {
            var coordinatesRad = new Coordinates
            {
                Latitude = DegToRad(geoLocation.Latitude),
                Longitude = DegToRad(geoLocation.Longitude)
            };
            var distanceToThisRad = new Coordinates
            {
                Latitude = DegToRad(distanceToThis.Latitude),
                Longitude = DegToRad(distanceToThis.Longitude)
            };
            var distance_longitude = coordinatesRad.Longitude - distanceToThisRad.Longitude;
            var distance_latitude = coordinatesRad.Latitude - distanceToThisRad.Latitude;
            var a = Math.Pow(Math.Sin(distance_latitude / 2), 2) + Math.Cos(coordinatesRad.Latitude) * Math.Cos(distanceToThisRad.Latitude) * Math.Pow(Math.Sin(distance_longitude / 2), 2);
            var c = 2 * Math.Asin(Math.Sqrt(a));
            return Convert.ToInt32(6371 * c * 1000);
        }
        private static double DegToRad(double deg)
        {
            return Math.Cos(Math.PI * deg / 180.0);
        }
    }
}