﻿using System.Linq.Expressions;

namespace backend.Extensions
{
    public static class ItemQueryDtoExtensions
    {
        public static Expression<Func<Item, bool>> CreateExpression(this ItemQueryDto itemQueryDto, User? user)
        {
            if (itemQueryDto.Newest == null || itemQueryDto.Newest == 0)
            {
                itemQueryDto.Newest = long.MaxValue;
            }
            if (itemQueryDto.Oldest == null || itemQueryDto.Oldest == 0)
            {
                itemQueryDto.Oldest = long.MaxValue;
            }
            Expression<Func<Item, bool>> expression = i => i.Id > itemQueryDto.Newest || i.Id < itemQueryDto.Oldest;
            if (itemQueryDto.Category != null)
            {
                Expression<Func<Item, bool>> tempExpression = i => i.ItemCategories.Any(c => c.CategoryId == itemQueryDto.Category);
                expression = expression.And(tempExpression);
            }
            if (itemQueryDto.MinValue != null || itemQueryDto.MaxValue != null)
            {
                if (itemQueryDto.MinValue == null)
                {
                    itemQueryDto.MinValue = 0;
                }
                if (itemQueryDto.MaxValue == null)
                {
                    itemQueryDto.MaxValue = short.MaxValue;
                }
                Expression<Func<Item, bool>> tempExpression = i => i.Value >= itemQueryDto.MinValue && i.Value <= itemQueryDto.MaxValue;
                expression = expression.And(tempExpression);
            }
            if (itemQueryDto.MinCondition != null || itemQueryDto.MaxCondition != null)
            {
                if (itemQueryDto.MinCondition == null)
                {
                    itemQueryDto.MinCondition = 0;
                }
                if (itemQueryDto.MaxCondition == null)
                {
                    itemQueryDto.MaxCondition = short.MaxValue;
                }
                Expression<Func<Item, bool>> tempExpression = i => i.Condition >= itemQueryDto.MinCondition && i.Condition <= itemQueryDto.MaxCondition;
                expression = expression.And(tempExpression);
            }
            if (!string.IsNullOrEmpty(itemQueryDto.SearchString))
            {
                var searchStrings = itemQueryDto.SearchString.Split(' ');
                if (itemQueryDto.IncludeDescription == null || itemQueryDto.IncludeDescription == false)
                {
                    foreach (string s in searchStrings)
                    {
                        Expression<Func<Item, bool>> tempExpression = i => i.Title.Contains(s);
                        expression = expression.And(tempExpression);
                    }
                }
                else
                {
                    foreach (string s in searchStrings)
                    {
                        Expression<Func<Item, bool>> tempExpression = i => i.Title.Contains(s) || i.Description.Contains(s);
                        expression = expression.And(tempExpression);
                    }
                }
            }
            if (user != null && (itemQueryDto.Distance != null && user.GeoLocation != null))
            {
                Expression<Func<Item, bool>> tempExpression = i => i.GeoLocation.GetDistanceBetweenGeolocations(user.GeoLocation) <= itemQueryDto.Distance;
                expression = expression.And(tempExpression);
            }
            return expression;
        }
    }
}
