﻿namespace backend.Helpers
{
    public class AppSettings
    {
        public class AuthenticationOptions
        {
            public const string Authentication = "Authentication";
            public string Secret { get; set; } = String.Empty;
        }
        public class EmailOptions
        {
            public const string Email = "Email";
            public string EmailFrom { get; set; } = String.Empty;
            public string SmtpHost { get; set; } = String.Empty;
            public int SmtpPort { get; set; }
            public string SmtpUser { get; set; } = String.Empty;
            public string SmtpPass { get; set; } = String.Empty;
        }
        public class PasswordOptions
        {
            public const string Password = "Password";
            public HashType HashType { get; set; }
            public int WorkFactor { get; set; }
        }
    }
}