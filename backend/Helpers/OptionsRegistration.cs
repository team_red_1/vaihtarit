﻿namespace Microsoft.Extensions.DependencyInjection
{
    public static class OptionsRegistrationExtensions
    {
        public static IServiceCollection AddConfig(
            this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<AuthenticationOptions>
                (configuration.GetSection(AuthenticationOptions.Authentication));
            services.Configure<EmailOptions>
                (configuration.GetSection(EmailOptions.Email));
            services.Configure<PasswordOptions>
                (configuration.GetSection(PasswordOptions.Password));
            return services;
        }
    }
}
