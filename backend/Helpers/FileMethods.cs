﻿namespace backend.Helpers
{
    public static class FileMethods
    {
        public static async Task SaveImage(IFormFile formFile, string filePath)
        {
            using(FileStream fileStream = File.Create(filePath))
            {
                await formFile.CopyToAsync(fileStream);
                await fileStream.FlushAsync();
            }
        }
        public static void DeleteFolderWithFiles(string path)
        {
            var files = Directory.GetFiles(path);
            if (files.Any())
            {
                foreach (var file in files)
                {
                    File.Delete(file);
                }
            }
            Directory.Delete(path);
        }
        public static bool FileFromHdd(string path, out byte[]? file)
        {
            if (!System.IO.File.Exists(path))
            {
                file = null;
                return false;
            }
            file = File.ReadAllBytes(path);
            if (file.Length > 0)
            {
                return true;
            }
            return false;
        }
    }
}
