﻿using MailKit.Net.Smtp;
using MimeKit;

namespace backend.Helpers
{
    public class EmailService : IEmailService
    {
        private readonly EmailOptions _emailOptions;
        public EmailService(IOptions<EmailOptions> emailOptions)
        {
            _emailOptions = emailOptions.Value;
        }
        public void SendMail(string to, string subject, string textBody, string from = null)
        {
            var mail = new MimeMessage();
            mail.From.Add(MailboxAddress.Parse(from ?? _emailOptions.EmailFrom));
            mail.To.Add(MailboxAddress.Parse(to));
            mail.Subject = subject;
            mail.Body = new TextPart()
            {
                Text = textBody
            };
            using var smtp = new SmtpClient();
            {
                try
                {
                    smtp.Connect(_emailOptions.SmtpHost,
                         _emailOptions.SmtpPort,
                         MailKit.Security.SecureSocketOptions.StartTls);
                    smtp.Authenticate(_emailOptions.SmtpUser, _emailOptions.SmtpPass);
                    smtp.Send(mail);
                    smtp.Disconnect(true);
                }
                catch(SmtpCommandException ex)
                {
                    Console.WriteLine(ex.Message, ex.StatusCode);
                }
                catch (SmtpProtocolException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    smtp.Dispose();
                }
            }
        }
    }
}