﻿namespace backend.Helpers
{
    public interface IEmailService
    {
        void SendMail(string to, string subject, string textBody, string from = null );
    }
}