﻿namespace backend.Helpers
{
    public class DefaultCategoriesGenerator
    {
        private readonly ISnowFlakeGenerator _snowFlakeGenerator;
        public DefaultCategoriesGenerator(ISnowFlakeGenerator snowFlakeGenerator)
        {
            _snowFlakeGenerator = snowFlakeGenerator;
        }
        public void CreateDefaultCategories(ICategoryRepository repo)
        {
            if(repo.Exists(e => e.Title == "Autot"))
            {
                return;
            }    
            List<Category> categories = new List<Category>();
            var ajoneuvot = new Category()
            {
                Id = _snowFlakeGenerator.GenerateId(),
                Title = "Ajoneuvot ja koneet"
            };
            categories.Add(ajoneuvot);
            categories.Add(new Category()
            {
                Id = _snowFlakeGenerator.GenerateId(),
                Title = "Autot",
                ParentId = ajoneuvot.Id
            });
            categories.Add(new Category()
            {
                Id = _snowFlakeGenerator.GenerateId(),
                Title = "Moottoripyörät",
                ParentId = ajoneuvot.Id
            });
            categories.Add(new Category()
            {
                Id = _snowFlakeGenerator.GenerateId(),
                Title = "Pyörät ja -tarvikkeet",
                ParentId = ajoneuvot.Id
            });
            var koti = new Category()
            {
                Id = _snowFlakeGenerator.GenerateId(),
                Title = "Koti ja asuminen"
            };
            categories.Add(koti);
            categories.Add(new Category()
            {
                Id = _snowFlakeGenerator.GenerateId(),
                Title = "Puutarha",
                ParentId = koti.Id
            });
            categories.Add(new Category()
            {
                Id = _snowFlakeGenerator.GenerateId(),
                Title = "Sisustus",
                ParentId = koti.Id
            });
            categories.Add(new Category()
            {
                Id = _snowFlakeGenerator.GenerateId(),
                Title = "Kodinkoneet",
                ParentId = koti.Id
            });
            var vapaaAika = new Category()
            {
                Id = _snowFlakeGenerator.GenerateId(),
                Title = "Vapaa-aika ja harrastukset"
            };
            categories.Add(vapaaAika);
            categories.Add(new Category()
            {
                Id = _snowFlakeGenerator.GenerateId(),
                Title = "Musiikki",
                ParentId = vapaaAika.Id
            });
            categories.Add(new Category()
            {
                Id = _snowFlakeGenerator.GenerateId(),
                Title = "Pelit",
                ParentId = vapaaAika.Id
            });
            categories.Add(new Category()
            {
                Id = _snowFlakeGenerator.GenerateId(),
                Title = "Veneet",
                ParentId = vapaaAika.Id
            });
            categories.Add(new Category()
            {
                Id = _snowFlakeGenerator.GenerateId(),
                Title = "Kalastus",
                ParentId = vapaaAika.Id
            });
            categories.Add(new Category()
            {
                Id = _snowFlakeGenerator.GenerateId(),
                Title = "Eräily",
                ParentId = vapaaAika.Id
            });
            var elektroniikka = new Category()
            {
                Id = _snowFlakeGenerator.GenerateId(),
                Title = "Elektroniikka"
            };
            categories.Add(elektroniikka);
            categories.Add(new Category()
            {
                Id = _snowFlakeGenerator.GenerateId(),
                Title = "Tietokoneet",
                ParentId = elektroniikka.Id
            });
            categories.Add(new Category()
            {
                Id = _snowFlakeGenerator.GenerateId(),
                Title = "Puhelimet",
                ParentId = elektroniikka.Id
            });
            categories.Add(new Category()
            {
                Id = _snowFlakeGenerator.GenerateId(),
                Title = "Älylaitteet",
                ParentId = elektroniikka.Id
            });
            foreach(Category c in categories)
            {
                repo.Create(c);
            }
            repo.SaveChanges();
            return;
        }
    }
}