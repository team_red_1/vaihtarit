﻿namespace backend.Helpers
{
    public interface IPasswordService
    {
        string HashPasswordWithSalt(string password);
        bool VerifyPassword(User user, string password);
    }
    public class PasswordService : IPasswordService
    {
        private readonly PasswordOptions _passwordOptions;
        public PasswordService(IOptions<PasswordOptions> passwordOptions)
        {
            _passwordOptions = passwordOptions.Value;
        }

        public string HashPasswordWithSalt(string password)
        {
            return BCryptNet.EnhancedHashPassword(password,
                                                  _passwordOptions.WorkFactor,
                                                  hashType: _passwordOptions.HashType);
        }
        public bool VerifyPassword(User user, string password)
        {
            if(user == null || string.IsNullOrEmpty(password))
            {
                return false;
            }
            return BCryptNet.EnhancedVerify(password,
                                            user.PasswordHash,
                                            hashType: _passwordOptions.HashType);
        }
    }
}
