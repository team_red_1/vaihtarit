﻿using IdGen;

namespace backend.Helpers
{
    public interface ISnowFlakeGenerator
    {
        long GenerateId();
        DateTime GetCreatedTimeUTC(long id);
    }
    public class SnowflakeGenerator : ISnowFlakeGenerator
    {
        private readonly IdGenerator _generator;
        public SnowflakeGenerator()
        {
            var epoch = new DateTime(2022, 1, 24, 0, 0, 0, DateTimeKind.Utc);
            var structure = new IdStructure(45, 1, 17);
            var options = new IdGeneratorOptions(structure, new DefaultTimeSource(epoch));
            _generator = new IdGenerator(0, options);
        }
        public long GenerateId()
        {
            return _generator.CreateId();
        }
        public DateTime GetCreatedTimeUTC(long id)
        {
            var idInfo = _generator.FromId(id);
            return idInfo.DateTimeOffset.UtcDateTime;
        }
    }
}