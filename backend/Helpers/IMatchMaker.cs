﻿
namespace backend.Helpers
{
    public interface IMatchMaker
    {
        Task<(bool matchFound, List<Match>? matches)> CheckAllMatches(User client);
        Task<(bool matchFound, Match? match)> CheckMatches(LikedItem likedItem);
    }
}