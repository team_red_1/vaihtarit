﻿namespace backend.Helpers
{
    public class Directories
    {
        private static readonly string _imageRoot = Directory.GetCurrentDirectory() + "/Uploads/";
        private static readonly string _itemFolder = _imageRoot + "ItemImage/";
        private static readonly string _userFolder = _imageRoot + "UserImage/";
        private static readonly string _tempFolder = _imageRoot + "tmp/";
        public static string TempFolder()
        {
            return _tempFolder;
        }
        public static string ItemImageFolder()
        {
            return _itemFolder;
        }
        public static string UserImageFolder()
        {
            return _userFolder;
        }
        public static string ImageRootFolder()
        {
            return _imageRoot;
        }
    }
}
