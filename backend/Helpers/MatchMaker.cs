﻿namespace backend.Helpers
{// @ToDO needs comments
    public class MatchMaker : IMatchMaker
    {
        private readonly IRepositoryService _repositoryService;
        private readonly ISnowFlakeGenerator _snowFlakeGenerator;
        public MatchMaker(IRepositoryService repositoryService, ISnowFlakeGenerator snowFlakeGenerator)
        {
            _repositoryService = repositoryService;
            _snowFlakeGenerator = snowFlakeGenerator;
        }
        public async Task<(bool matchFound, List<Match>? matches)> CheckAllMatches(User targetOfLike)
        {
            var (likeQuerySuccess, likes) = await _repositoryService.LikedItemRepository.QueryAsync(x => x.UserId == targetOfLike.Id);
            if (!likeQuerySuccess)
            {
                return (false, null);
            }
            List<Match>? matches = new();
            foreach (var like in likes)
            {
                var (itemSuccess, ownerOfItem) = await _repositoryService.ItemRepository.GetAsync(like.ItemId);
                if (!itemSuccess)
                {
                    continue;
                }
                var (ownerGetSuccess, activeLiker) = await _repositoryService.UserRepository.GetAsync(ownerOfItem.UserId);
                if (!ownerGetSuccess)
                {
                    continue;
                }
                var addExistingToMatches = _repositoryService.MatchRepository.GetOpenMatch(targetOfLike, activeLiker);
                Match? existingMatch = addExistingToMatches.Result;
                if (existingMatch != null && !(matches.Exists(x => x.ActiveLikerUserId == activeLiker.Id) || (matches.Exists(x => x.TargetOfLikeUserId == activeLiker.Id))))
                {
                   
                    matches.Add(existingMatch);
                }
                if (!await _repositoryService.MatchRepository.OpenMatchExistsAsync(targetOfLike, activeLiker))
                {
                    var match = await CreateMatch(targetOfLike, activeLiker);
                    if (match != null && !matches.Any(x => (x.TargetOfLikeUserId == targetOfLike.Id && x.ActiveLikerUserId == activeLiker.Id) 
                        || (x.ActiveLikerUserId == targetOfLike.Id && x.TargetOfLikeUserId == activeLiker.Id)))
                    {
                        matches.Add(match);
                    }
                }
            }
            if (matches.Any())
            {
                return (true, matches);
            }
            else
            {
                return (false, null);
            }
        }
        public async Task<(bool matchFound, Match? match)> CheckMatches(LikedItem likedItem)
        {
            var (targetOfLikeSuccess, targetOfLike)
                = await _repositoryService.UserRepository.GetAsync(likedItem.UserId);
            var (itemSuccess, item)
                = await _repositoryService.ItemRepository.GetAsync(likedItem.ItemId);
            if(!itemSuccess || item == null)
            {
                return (false, null);
            }
            var (activeLikerSuccess, activeLiker)
                = await _repositoryService.UserRepository.GetAsync(item.UserId);
            if (!activeLikerSuccess || !targetOfLikeSuccess)
            {
                return (false, null);
            }
            var match = await CreateMatch(targetOfLike, activeLiker);
            if (match != null)
            {
                return (true, match);
            }
            else
            {
                return (false, null);
            }
        }
        private async Task<Match?> CreateMatch(User targetOfLike, User activeLiker)
        {
            if(targetOfLike.Id == activeLiker.Id)
            {
                return null;
            }
            var (successClientsLikes, targetOfLikeLikes) = await _repositoryService.LikedItemRepository.QueryAsync(x => x.UserId == targetOfLike.Id);
            var (successOwnerLikes, activeLikerLikes) = await _repositoryService.LikedItemRepository.QueryAsync(x => x.UserId == activeLiker.Id);

            List<Item> items = new();
            bool targetOfLikeHasActiveLikerItemLikes = false;
            bool activeLikerHasTargetOfLikeItemLikes = false;
            foreach(var like in targetOfLikeLikes)
            {
                var (itemSuccess, item) = await _repositoryService.ItemRepository.GetAsync(like.ItemId);
                if(itemSuccess && item.UserId == activeLiker.Id)
                {
                    activeLikerHasTargetOfLikeItemLikes = true;
                    items.Add(item);
                }
            }
            foreach(var like in activeLikerLikes)
            {
                var (itemSuccess, item) = await _repositoryService.ItemRepository.GetAsync(like.ItemId);
                if(itemSuccess && item.UserId == targetOfLike.Id)
                {
                    targetOfLikeHasActiveLikerItemLikes = true;
                    items.Add(item);
                }
            }
            if(!activeLikerHasTargetOfLikeItemLikes || !targetOfLikeHasActiveLikerItemLikes)
            {
                return null;
            }
            var match = await _repositoryService.MatchRepository.GetOpenMatch(targetOfLike, activeLiker);
            if(match == null)
            {
                match = new Match()
                {
                    Id = _snowFlakeGenerator.GenerateId(),
                    ActiveLikerUserId = activeLiker.Id,
                    TargetOfLikeUserId = targetOfLike.Id,
                    ActiveLikerAgree = false,
                    TargetOfLikeAgree = false,
                    Completed = false,
                    IsDeleted = false
                };
                _repositoryService.MatchRepository.Create(match);
                await _repositoryService.MatchRepository.SaveChangesAsync();
                return match;
            }
            return null;
        }   
    }
}