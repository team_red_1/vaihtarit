﻿namespace backend.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoryController : BaseController
    {
        private readonly ICategoryRepository _categoryRepository;
        public CategoryController(IJwtUtilities jwtUtilities, ISnowFlakeGenerator snowFlakeGenerator, IEmailService emailService, IRepositoryService repositoryService, IPasswordService passwordService)
            : base(jwtUtilities, snowFlakeGenerator, emailService, repositoryService, passwordService)
        {
            _categoryRepository = repositoryService.CategoryRepository;
        }
        [HttpGet]
        public async Task<IActionResult> GetCategories()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var (isSuccess, categories) = await _categoryRepository.GetAllAsync();
            if(isSuccess)
            {
                return Ok(categories);
            }
            return NotFound();
        }
        [HttpGet("{id:long}")]
        public async Task<IActionResult> GetCategory(long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var (isSuccess, category) = await _categoryRepository.GetAsync(id);
            if(isSuccess)
            {
                return Ok(category);
            }
            return NotFound();
        }
        [HttpGet("children/{id:long}")]
        public async Task<IActionResult> GetChildCategories(long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var (isSuccess, childCategories) = await _categoryRepository.QueryAsync(x => x.ParentId == id);
            if(isSuccess)
            {
                return Ok(childCategories);
            }
            else
            {
                return NotFound();
            }
        }
        [HttpGet("main")]
        public async Task<IActionResult> GetMainCategories()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var (isSuccess, mainCategories) = await _categoryRepository.QueryAsync(x => x.ParentId == null);
            if (isSuccess)
            {
                return Ok(mainCategories);
            }
            return NotFound();
        }
        [HttpPost]
        [Authorize(Role.Admin)]
        public async Task<IActionResult> CreateCategory(CategoryCreateDto newCategoryDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var caster = new EntityCaster<Category, CategoryCreateDto>();
            var category = new Category();
            category.Id = SnowflakeGenerator.GenerateId();
            category = caster.CastTo(category, newCategoryDto);
            _categoryRepository.Create(category);
            await _categoryRepository.SaveChangesAsync();
            return Ok();
        }
        [HttpPatch]
        [Authorize(Role.Admin)]
        public async Task<IActionResult> UpdateCategory(CategoryUpdateDto updateCategoryDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var (isSuccess, category) = await _categoryRepository.GetAsync(updateCategoryDto.Id);
            if (isSuccess)
            {
                var caster = new EntityCaster<Category, CategoryUpdateDto>();
                category = caster.CastTo(category, updateCategoryDto);
            }
            isSuccess = _categoryRepository.Patch(category);
            if (isSuccess)
            {
                await _categoryRepository.SaveChangesAsync();
                return Ok();
            }
            return NotFound();
        }
        [HttpDelete("{id:long}")]
        [Authorize(Role.Admin)]
        public async Task<IActionResult> DeleteCategory(long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var (isSuccess, category) = await _categoryRepository.GetAsync(id);
            if(isSuccess)
            {
                _categoryRepository.DeleteCategory(category);
                await _categoryRepository.SaveChangesAsync();
                return Ok();
            }
            return NotFound();
        }
    }
}