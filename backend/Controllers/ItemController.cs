﻿using System.Linq.Expressions;

namespace backend.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class ItemController : BaseController
    {
        private readonly IItemRepository _itemRepository;
        private readonly static int _itemBrowseCount = 20;
        public ItemController(IJwtUtilities jwtUtilities, ISnowFlakeGenerator snowFlakeGenerator, IEmailService emailService, IRepositoryService repositoryService, IPasswordService passwordService)
            : base(jwtUtilities, snowFlakeGenerator, emailService, repositoryService, passwordService)
        {
            _itemRepository = repositoryService.ItemRepository;
        }
        [HttpPost]
        public async Task<IActionResult> CreateItem(ItemCreateDto itemCreateDto)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest();
            }
            Item item = new();
            var caster = new EntityCaster<Item, ItemCreateDto>();
            item.Id = SnowflakeGenerator.GenerateId();
            item.UserId = User.Id;
            item = caster.CastTo(item, itemCreateDto);
            if (!itemCreateDto.Images.IsNullOrEmpty())
            {
                string imageDirectory = Directories.TempFolder() + itemCreateDto.Images + '/';
                if (!Directory.Exists(imageDirectory))
                {
                    return NotFound();
                }
                var images = Directory.GetFiles(imageDirectory).ToList();
                if (images.Any())
                {
                    var mainImage = images.First();
                    var imageLink = ImageToDb(item.Id, mainImage, true);
                    if (!imageLink.IsNullOrEmpty())
                    {
                        item.MainImageLink = imageLink;
                    }
                    images.Remove(mainImage);
                }
                if (images.Any())
                {
                    foreach (var image in images)
                    {
                        ImageToDb(item.Id, image);
                    }
                }
                FileMethods.DeleteFolderWithFiles(imageDirectory);
            }
            _itemRepository.Create(item);
            var itemCategory = new ItemCategory()
            {
                Id = SnowflakeGenerator.GenerateId(),
                ItemId = item.Id,
                CategoryId = itemCreateDto.Category
            };
            RepositoryService.ItemCategoryRepository.Create(itemCategory);
            await _itemRepository.SaveChangesAsync();
            return Ok();
        }
        [HttpGet("{id:long}")]
        [AllowAnonymous]
        public async Task<IActionResult> GetItem(long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var (isSuccess, item) = await _itemRepository.GetAsync(id);
            if (isSuccess)
            {
                var itemResponse = CreateItemResponse(item);
                return Ok(itemResponse);
            }
            return NotFound();
        }
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> GetItems([FromQuery] ItemGetDto itemGetDto)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest();
            }
            if(itemGetDto.Newest == null || itemGetDto.Newest == 0)
            {
                itemGetDto.Newest = long.MaxValue;
            }
            if (itemGetDto.Oldest == null || itemGetDto.Oldest == 0)
            {
                itemGetDto.Oldest = long.MaxValue;
            }
            var items = _itemRepository
                        .GetAllAsArray()
                        .Where(i => i.Id > itemGetDto.Newest || i.Id < itemGetDto.Oldest)
                        .OrderByDescending(i => i.Id)
                        .Take(_itemBrowseCount)
                        .ToList();
            List<ItemGetResponseDto> itemList = new();
            foreach(Item item in items)
            {
                itemList.Add(CreateItemResponse(item));
            }
            return Ok(itemList);
        }
        [HttpGet("query")]
        [AllowAnonymous] // Will be removed in final version, AllowAnonymous on for testing purposes
        public async Task<IActionResult> Query([FromQuery] ItemQueryDto itemQueryDto)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest();
            }
            Expression<Func<Item, bool>> expression = itemQueryDto.CreateExpression(User);
            var (isSuccess, items) = await _itemRepository.QueryAsync(expression);
            items = items.OrderByDescending(i => i.Id);
            items = items.Take(_itemBrowseCount);
            List<ItemGetResponseDto> itemsDto = new();
            foreach(Item i in items)
            {

                itemsDto.Add(CreateItemResponse(i));
            }
            return Ok(itemsDto);
        }
        [HttpPatch]
        public async Task<IActionResult> PatchItem(ItemPatchDto itemPatchDto)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest();
            }
            var (isSuccess, item) = await _itemRepository.GetAsync(itemPatchDto.Id);
            if(!isSuccess)
            {
                return NotFound();
            }
            if(item.UserId == User.Id || User.Role == Role.Admin)
            {
                var caster = new EntityCaster<Item, ItemPatchDto>();
                item = caster.CastTo(item, itemPatchDto);
                item.EditedOn = DateTime.UtcNow;
                isSuccess = _itemRepository.Patch(item);
                if(isSuccess)
                {
                    return Ok();
                }
                return BadRequest();
            }
            return Forbid();
        }
        [HttpDelete("{id:long}")]
        public async Task<IActionResult> DeleteItem(long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var (isSuccess, item) = await _itemRepository.GetAsync(id);
            if (!isSuccess)
            {
                return NotFound();
            }
            if (item.UserId != User.Id && User.Role == Role.User)
            {
                return Forbid();
            }
            _itemRepository.SoftDelete(item);
            await _itemRepository.SaveChangesAsync();
            return Ok();
        }
        [AllowAnonymous]
        [HttpGet("create-item-data")]
        public async Task<IActionResult> GetItemProperties()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var (isSuccess, categories) = await RepositoryService.CategoryRepository.GetAllAsync();
            if (!isSuccess)
            {
                return NotFound();
            }
            categories = SortCategoryList(categories);
            List<CategoryGetWithoutParentDto> categoryDtoList = new();
            var caster = new EntityCaster<CategoryGetWithoutParentDto, Category>();
            foreach(Category c in categories)
            {
                CategoryGetWithoutParentDto categoryGetDto = new();
                categoryGetDto = caster.CastTo(categoryGetDto, c, true, true);
                categoryDtoList.Add(categoryGetDto);
            }
            CategoryGetWithoutParentDto[] categoryGetArray = categoryDtoList.ToArray();
            var conditionList = new List<EnumDto>();
            var roughValueList = new List<EnumDto>();
            foreach (Condition condition in Enum.GetValues(typeof(Condition)))
            {
                EnumDto enumDto = new();
                enumDto.Id = (short)condition;
                enumDto.Title = condition.GetDisplayName();
                conditionList.Add(enumDto);
            }
            foreach(RoughValue roughValue in Enum.GetValues(typeof(RoughValue)))
            {
                EnumDto enumDto = new();
                enumDto.Id = (short)roughValue;
                enumDto.Title = roughValue.GetDisplayName();
                roughValueList.Add(enumDto);
            }
            ItemCreateDataResponseDto info = new()
            {
                Categories = categoryGetArray,
                Conditions = conditionList.ToArray(),
                RoughValues = roughValueList.ToArray()
            };
            return Ok(info);
        }
        [Authorize]
        [HttpGet("user-likes")]
        public async Task<IActionResult> GetUsersLikedItems()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var (likesSuccess, likes) = await RepositoryService.LikedItemRepository.QueryAsync(x => x.UserId == User.Id);
            if (!likesSuccess)
            {
                return Ok();
            }
            List<Item> items = new();
            foreach(var like in likes)
            {
                var (itemSuccess, item) = await _itemRepository.GetAsync(like.ItemId);
                if(itemSuccess)
                {
                    items.Add(item);
                }
            }
            List<ItemGetResponseDto> itemsDto = new();
            foreach(var item in items)
            {
                itemsDto.Add(CreateItemResponse(item));
            }
            return Ok(itemsDto);
        }
        [HttpGet("user-items/{id:long}")]
        [AllowAnonymous]
        public async Task<IActionResult> GetUsersItems(long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var (isSuccess, user) = await RepositoryService.UserRepository.GetAsync(id);
            if(!isSuccess)
            {
                return NotFound();
            }
            var (itemsSuccess, items) = await _itemRepository.QueryAsync(x => x.UserId == id);
            List<ItemGetResponseDto> itemsDto = new();
            if(!itemsSuccess)
            {
                return Ok();
            }
            foreach(Item item in items)
            {
                itemsDto.Add(CreateItemResponse(item));
            }
            return Ok(itemsDto);
        }
        [HttpGet("checkIfItemHasImage/{id:long}")]
        [AllowAnonymous]
        public async Task<IActionResult> CheckIfItemHasImageAttached(long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            bool doesImageExist = false;
            var (itemSuccess, item) = await _itemRepository.QueryAsync(x => x.Id == id);
            if (!itemSuccess)
            {
                NotFound();
            }
            if(item.ElementAt(0).MainImageLink != null)
            {
                doesImageExist = true;
            }
            return Ok(doesImageExist);
        }
        private ItemGetResponseDto CreateItemResponse(Item item)
        {
            var caster = new EntityCaster<ItemGetResponseDto, Item>();
            var itemResponse = new ItemGetResponseDto();
            itemResponse = caster.CastTo(itemResponse, item);
            var (userSuccess, itemOwner) = RepositoryService.UserRepository.Get(item.UserId);
            if (userSuccess)
            {
                itemResponse.Username = itemOwner.Username;
            }
            var (categoriesSuccess, itemCategories) = RepositoryService.ItemCategoryRepository.Query(e => e.ItemId == item.Id);
            if (categoriesSuccess)
            {
                var (categorySuccess, category) = RepositoryService.CategoryRepository.Get(itemCategories.FirstOrDefault().CategoryId);
                if(categorySuccess)
                {
                    itemResponse.Category = category.Title;
                }
            }
            var (imagesSuccess, additionalImages) = RepositoryService.ItemImageRepository.Query(e => e.ItemId == item.Id);
            if (imagesSuccess)
            {
                itemResponse.AdditionalImages = new long[additionalImages.Count()];
                for (int i = 0; i < additionalImages.Count(); i++)
                {
                    itemResponse.AdditionalImages[i] = additionalImages.ElementAt(i).Id;
                }
            }
            itemResponse.Condition = GetConditionString(item.Condition);
            itemResponse.Value = GetValueString(item.Value);
            return itemResponse;
        }
        private string GetConditionString(short id)
        {
            foreach (Condition condition in Enum.GetValues(typeof(Condition)))
            {
                if ((short)condition == id)
                {
                    return condition.GetDisplayName();
                }
            }
            return "";
        }
        private string GetValueString(short id)
        {
            foreach (RoughValue value in Enum.GetValues(typeof(RoughValue)))
            {
                if ((short)value == id)
                {
                    return value.GetDisplayName();
                }
            }
            return "";
        }
        private List<Category> SortCategoryList(IEnumerable<Category> categoryListEnumerable)
        {
            var categoryList = categoryListEnumerable.ToList();
            int length = categoryList.Count;
            categoryList.Sort(delegate (Category c1, Category c2)
            {
                return c1.Title.CompareTo(c2.Title);
            });
            int lastParent = 0;
            for (int i = 1; i < length; i++)
            {
                if (categoryList[i].ParentId == null)
                {
                    var tempCategory = categoryList[i];
                    categoryList.RemoveAt(i);
                    categoryList.Insert(lastParent + 1, tempCategory);
                    lastParent++;
                }
            }
            for (int i = 0; i < length; i++)
            {
                if (categoryList[i].ParentId == null)
                {
                    int childLocation = i + 1;
                    for (int j = childLocation; j < length; j++)
                    {
                        if (categoryList[j].ParentId == categoryList[i].Id)
                        {
                            var tempChild = categoryList[j];
                            tempChild.Title = "- " + categoryList[j].Title;
                            categoryList.RemoveAt(j);
                            categoryList.Insert(childLocation, tempChild);
                            childLocation++;
                        }
                    }
                }
            }
            return categoryList;
        }
        private string ImageToDb(long itemId, string source, bool mainImage = false)
        {
            try
            {
                if(!System.IO.File.Exists(source))
                {
                    return "";
                }
                var copyPath = Directories.ItemImageFolder();
                if (!Directory.Exists(copyPath))
                {
                    Directory.CreateDirectory(copyPath);
                }
                long imageId;
                if (mainImage)
                {
                    imageId = itemId;
                }
                else
                {
                    imageId = SnowflakeGenerator.GenerateId();
                }
                copyPath = copyPath + imageId.ToString() + ".jpg";
                if(System.IO.File.Exists(copyPath))
                {
                    System.IO.File.Delete(copyPath);
                }
                System.IO.File.Move(source, copyPath);
                if(!mainImage)
                {
                    var itemImage = new ItemImage()
                    {
                        Id = imageId,
                        ItemId = itemId,
                        ImageLink = copyPath
                    };
                    RepositoryService.ItemImageRepository.Create(itemImage);
                    RepositoryService.ItemImageRepository.SaveChanges();
                }
                return copyPath;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                return "";
            }
        }
    }
}