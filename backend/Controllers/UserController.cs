﻿namespace backend.Controllers
// @ToDo Maybe add Dto to GetUsers to limit important information access.
// GenerateDatabase will be moved to Admin controller once it's finished.
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : BaseController
    {
        private readonly IUserRepository _userRepository;
        public UserController(IJwtUtilities jwtUtilities, ISnowFlakeGenerator snowFlakeGenerator, IEmailService emailService, IRepositoryService repositoryService, IPasswordService passwordService)
            : base(jwtUtilities, snowFlakeGenerator, emailService, repositoryService, passwordService)
        {
            _userRepository = repositoryService.UserRepository;
        }
        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate(UserAuthenticateRequest model)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            IEnumerable<User> users;
            bool isSuccess;
            if(model.UsernameOrEmail.Contains('@'))
            {
                (isSuccess, users) = await _userRepository.QueryIncludingDeletedAsync(u => u.Email == model.UsernameOrEmail);
            }
            else
            {
                (isSuccess, users) = await _userRepository.QueryIncludingDeletedAsync(u => u.Username == model.UsernameOrEmail);
            }
            if(!isSuccess)
            {
                return Forbid();
            }
            var user = users.FirstOrDefault();
            if (!PasswordService.VerifyPassword(user, model.Password))
            {
                return Forbid();
            }
            if(user.IsDeleted == true)
            {
                _userRepository.UnDelete(user.Id);
                _userRepository.SaveChanges();
            }
            var caster = new EntityCaster<UserAuthenticateResponse, User>();
            var response = new UserAuthenticateResponse();
            response = caster.CastTo(response, user);
            response.Token = JwtUtilities.GenerateJwtToken(user);
            return Ok(response);
        }
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> RegisterNewUser(UserRegisterDto newUserDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (await _userRepository.ExistsByUsernameAsync(newUserDto.Username))
            {
                ModelState.AddModelError("username", DbError.UsernameTaken.GetDisplayName());
                return Conflict(ModelState);
            }
            if (await _userRepository.ExistsByEmailAsync(newUserDto.Email))
            {
                ModelState.AddModelError("email", DbError.EmailTaken.GetDisplayName());
                return Conflict(ModelState);
            }
            var caster = new EntityCaster<User, UserRegisterDto>();
            var newUser = new User();
            newUser = caster.CastTo(newUser, newUserDto);
            newUser.Id = SnowflakeGenerator.GenerateId();
            newUser.VerificationToken = JwtUtilities.RandomTokenString();
            newUser.PasswordHash = PasswordService.HashPasswordWithSalt(newUserDto.Password);
            _userRepository.Create(newUser);
            await _userRepository.SaveChangesAsync();
            SendVerificationEmail(newUser, Request.Headers["origin"]);
            return Ok(UserManagement.UserCreated.GetDisplayName());
        }
        [HttpPatch("confirm/{verificationToken}")]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string verificationToken)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var (isSuccess, user) = await _userRepository.GetByVerificationTokenAsync(verificationToken);
            if(!isSuccess)
            {
                return Forbid();
            }
            user.Activated = true;
            user.VerificationToken = null;
            _userRepository.Update(user);
            await _userRepository.SaveChangesAsync();
            return Ok(new { message = "Verification successful, you can now login!" });
        }
        [HttpPatch]
        public async Task<IActionResult> UpdateUser(UserUpdateDto updateUserDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var (isSuccess, user) = await _userRepository.GetAsync(updateUserDto.Id);
            if (!isSuccess)
            {
                return NotFound();
            }
            if (User.Id != updateUserDto.Id && User.Role == Role.User)
            {
                return Forbid();
            }
            if(!string.IsNullOrEmpty(updateUserDto.Username) && user.Username != updateUserDto.Username)
            {
                if(await _userRepository.ExistsByUsernameAsync(updateUserDto.Username))
                {
                    ModelState.AddModelError("username", DbError.UsernameTaken.GetDisplayName());
                    return Conflict(ModelState);
                }
            }
            if (!string.IsNullOrEmpty(updateUserDto.Email) && user.Email != updateUserDto.Email)
            {
                if (await _userRepository.ExistsByEmailAsync(updateUserDto.Email))
                {
                    ModelState.AddModelError("email", DbError.EmailTaken.GetDisplayName());
                    return Conflict(ModelState);
                }
            }
            var caster = new EntityCaster<User, UserUpdateDto>();
            user = caster.CastTo(user, updateUserDto, false, false);
            _userRepository.Update(user);
            await _userRepository.SaveChangesAsync();
            return Ok();
        }
        [HttpPut("password")]
        public async Task<IActionResult> ChangePassword(UserChangePasswordDto changePasswordDto)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var (isSuccess, user) = await _userRepository.GetAsync(changePasswordDto.Id);
            if(!isSuccess)
            {
                return NotFound();
            }
            if(!PasswordService.VerifyPassword(user, changePasswordDto.OldPassword) || User.Id != user.Id)
            {
                return Forbid();
            }
            user.PasswordHash = PasswordService.HashPasswordWithSalt(changePasswordDto.NewPassword);
            _userRepository.Update(user);
            await _userRepository.SaveChangesAsync();
            return Ok();
        }
        [HttpGet]
        [Authorize(Role.Admin)]
        public async Task<IActionResult> GetUsers()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var (isSuccess, users) = await _userRepository.GetAllAsync();
            if(isSuccess)
            {
                var usersDto = new List<UserGetResponseDto>();
                foreach(User user in users)
                {
                    var userDto = CreateGetUserDto(user);
                    usersDto.Add(userDto);
                }
                return Ok(usersDto);
            }
            else
            {
                return NotFound();
            }
        }
        [HttpGet("{id:long}")]
        public async Task<IActionResult> GetUser(long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id == User.Id || User.Role == Role.Admin)
            {
                var (isSuccess, user) = await _userRepository.GetAsync(id);
                if (isSuccess)
                {
                    var userDto = CreateGetUserDto(user);
                    return Ok(userDto);
                }
                else
                {
                    return NotFound();
                }
            }
            return Forbid();
        }
        [HttpDelete("{id:long}")]
        public async Task<IActionResult> DeleteUser(long id, UserDeleteDto userDeleteDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var (isSuccess, user) = await _userRepository.GetAsync(id);
            if (!isSuccess)
            {
                return NotFound();
            }
            if(!PasswordService.VerifyPassword(user, userDeleteDto.Password) || User.Id != id)
            {
                return Forbid();
            }
            _userRepository.SoftDelete(user);
            await _userRepository.SaveChangesAsync();
            return Ok();
        }
        [AllowAnonymous]
        [HttpGet("database")]
        public async Task<IActionResult> GenerateDatabase()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var generator = new DefaultCategoriesGenerator(SnowflakeGenerator);
            generator.CreateDefaultCategories(RepositoryService.CategoryRepository);
            return Ok();
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        private void SendVerificationEmail(User user, string origin)
        {
            string message;
            if (!string.IsNullOrEmpty(origin))
            {
                var verifyUrl = $"{origin}/User/confirm?token={user.VerificationToken}";
                message = $@"<p>Please click the link below to verify your email address:</p>
                            <p><a href=""{verifyUrl}"">{verifyUrl}</a></p>";
            }
            else
            {
                message = $@"<p>Please use the link below to verify your email address with
                            <code>https://localhost:5100/api/User/confirm/{user.VerificationToken}</code>:</p>";
            }
            EmailService.SendMail(
                to: user.Email,
                subject: "Sign-Up Verification API - Verify Email",
                textBody: $@"<h4>Verify Email</h4>
                            <p>Thanks for registering!</p>
                            {message}"
                );
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        private UserGetResponseDto CreateGetUserDto(User user)
        {
            var caster = new EntityCaster<UserGetResponseDto, User>();
            UserGetResponseDto userDto = new();
            userDto.CreatedAt = SnowflakeGenerator.GetCreatedTimeUTC(user.Id);
            userDto = caster.CastTo(userDto, user, false, true);
            return userDto;
        }
    }
}