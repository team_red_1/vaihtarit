﻿namespace backend.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MessageController : BaseController
    {
        private readonly IMessageRepository _messageRepository;
        public MessageController(IJwtUtilities jwtUtilities, ISnowFlakeGenerator snowFlakeGenerator, IEmailService emailService, IRepositoryService repositoryService, IPasswordService passwordService)
            : base(jwtUtilities, snowFlakeGenerator, emailService, repositoryService, passwordService)
        {
            _messageRepository = repositoryService.MessageRepository;
        }
        [HttpGet("myMessages")]
        public async Task<IActionResult> GetOnlyMyMessages([FromQuery] MessageGetOnlyDto messageGetDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var (isSuccessUser, user) = RepositoryService.UserRepository.Get(messageGetDto.UserId);
            if (!isSuccessUser && User.Role != Role.Admin)
            {
                return NotFound();
            }
            var (isSuccess, match) = await RepositoryService.MatchRepository.QueryAsync(x => x.Id == messageGetDto.MatchId);
            if (!isSuccess)
            {
                return NotFound();
            }
            var (isSuccess2, messagesOfUser) = _messageRepository.Query(x => x.MatchId == messageGetDto.MatchId && x.ReceiverId == user.Id);
            if (isSuccess2)
            {
                return Ok(messagesOfUser);
            }
            return NotFound();
        }
        [HttpGet("matchMessages")]
        public async Task<IActionResult> GetMatchMessages([FromQuery] MessageGetMatchMessagesDto matchMessagesGetDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var (isSuccess, match) = await RepositoryService.MatchRepository.QueryAsync(x => x.Id == matchMessagesGetDto.MatchId);
            if (!isSuccess) 
            {
                return NotFound();
            }
            var possibleActiveLiker = match.ElementAt(0).ActiveLikerUserId;
            var possibleTargetOfLike = match.ElementAt(0).TargetOfLikeUserId;

            if (possibleActiveLiker != matchMessagesGetDto.UserId && possibleTargetOfLike != matchMessagesGetDto.UserId)
            {
                return NotFound();
            }
            var (isSuccess2, messagesOfUser) = await _messageRepository.QueryAsync(x => x.MatchId == matchMessagesGetDto.MatchId && (x.ReceiverId == possibleActiveLiker || x.ReceiverId == possibleTargetOfLike));
            if (isSuccess2)
            {
                return Ok(messagesOfUser);
            }
            return NotFound();
        }
        [HttpPost]
        public async Task<IActionResult> CreateMessage([FromQuery] MessageCreateDto newMessageDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var (isSuccessUser, user) = await RepositoryService.UserRepository.QueryAsync(x => (x.Id == newMessageDto.UserId) && x.Id == User.Id);
            if (!isSuccessUser)
            {
                return NotFound();
            }
            var (isSuccessMatch, userFromMatch) = await RepositoryService.MatchRepository.GetAsync(newMessageDto.MatchId);
            var personNeededId = userFromMatch.TargetOfLikeUserId;

            if(userFromMatch.TargetOfLikeUserId == newMessageDto.UserId)
            {
                personNeededId = userFromMatch.ActiveLikerUserId;
            }
            var caster = new EntityCaster<Message, MessageCreateDto>();
            var message = new Message();
            message.Id = SnowflakeGenerator.GenerateId();
            message.MessageText = newMessageDto.MessageText;
            message = caster.CastTo(message, newMessageDto);
            message.SenderId = newMessageDto.UserId;
            message.ReceiverId = personNeededId;
            _messageRepository.Create(message);

            Notification notification = new Notification();
            notification.Id = SnowflakeGenerator.GenerateId();
            notification.TypeOfMessage = 3;
            notification.AccountHolderUserId = newMessageDto.UserId;
            notification.OtherPersonUserId = personNeededId;
            notification.MatchId = newMessageDto.MatchId;
            RepositoryService.NotificationRepository.Create(notification);
            _messageRepository.SaveChanges();
            return Ok();
        }
        [HttpDelete]
        [Authorize(Role.Admin)]
        public async Task<IActionResult> DeleteMessage([FromQuery] MessageDeleteDto messageDeleteDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var (isSuccessDelete, toDelete) = await RepositoryService.MessageRepository.GetAsync(messageDeleteDto.MessageId);
            if (isSuccessDelete)
            {
                _messageRepository.SoftDelete(toDelete.Id);
                _messageRepository.SaveChanges();
                return Ok();
            }
            return NotFound();
        }
    }
}