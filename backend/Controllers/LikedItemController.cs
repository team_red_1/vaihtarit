﻿namespace backend.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class LikedItemController : BaseController
    {
        private readonly ILikedItemRepository _likedItemRepository;
        private readonly IMatchMaker _matchMaker;
        public LikedItemController(IJwtUtilities jwtUtilities, ISnowFlakeGenerator snowFlakeGenerator,IEmailService emailService, IRepositoryService repositoryService, IPasswordService passwordService, IMatchMaker matchMaker)
            : base(jwtUtilities, snowFlakeGenerator, emailService, repositoryService, passwordService)
        {
            _likedItemRepository = repositoryService.LikedItemRepository;
            _matchMaker = matchMaker;
        }
        [HttpPost]
        public async Task<IActionResult> CreateLike(LikedItemCreateDto likedItemCreateDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var (isSuccessUser, checkUser) = await RepositoryService.UserRepository.QueryAsync(x => x.Id == likedItemCreateDto.UserId && x.Id == User.Id);
            if (!isSuccessUser)
            {
                return Forbid();
            }
            var (isSuccess, checkPrevious) = await _likedItemRepository.QueryAsync(x => x.ItemId == likedItemCreateDto.ItemId
            && x.UserId == User.Id);
            if (isSuccess || checkPrevious.Any())
            {
                return Ok("This item has already been liked by you.");
            }
            var (isSuccessItem, isUserOwner) = await RepositoryService.ItemRepository.GetAsync(likedItemCreateDto.ItemId);
            if (!isSuccessItem)
            {
                return NotFound("Cannot find item specified.");
            }
            if (isUserOwner.UserId == User.Id && isUserOwner.UserId == likedItemCreateDto.UserId)
            {
                return Ok("Cannot like your own item");
            }
            var caster = new EntityCaster<LikedItem, LikedItemCreateDto>();
            LikedItem likedItem = new();
            likedItem.Id = SnowflakeGenerator.GenerateId();
            likedItem.UserId = User.Id;
            likedItem = caster.CastTo(likedItem, likedItemCreateDto);
            _likedItemRepository.Create(likedItem);
            _likedItemRepository.SaveChanges();

            Notification notification = new Notification();
            notification.Id = SnowflakeGenerator.GenerateId();
            notification.TypeOfMessage = 1;
            notification.AccountHolderUserId = likedItem.UserId;
            notification.LikedItemId = likedItem.Id;
            notification.OtherPersonUserId = isUserOwner.UserId;
            if (notification.AccountHolderUserId == notification.OtherPersonUserId)
            {
                return Ok("Notification not needed for your own actions.");
            }
            IMatchMaker matchMaker = new MatchMaker(RepositoryService, SnowflakeGenerator);
            var (matchFound, match) = await matchMaker.CheckMatches(likedItem);
            if(matchFound && match != null)
            {
                Notification notificationMatchTarget = new Notification();
                notificationMatchTarget.Id = SnowflakeGenerator.GenerateId();
                notificationMatchTarget.AccountHolderUserId = likedItem.UserId;
                notificationMatchTarget.OtherPersonUserId = notification.OtherPersonUserId;
                notificationMatchTarget.LikedItemId = likedItem.Id;
                notification.MatchId = match.Id;
                notificationMatchTarget.MatchId = match.Id;
                notificationMatchTarget.TypeOfMessage = 2;
                RepositoryService.NotificationRepository.Create(notificationMatchTarget);
                Notification notificationMatchUser = new Notification();
                notificationMatchUser.Id = SnowflakeGenerator.GenerateId();
                notificationMatchUser.OtherPersonUserId = likedItem.UserId;
                notificationMatchUser.AccountHolderUserId = notification.OtherPersonUserId;
                notificationMatchUser.MatchId = match.Id;
                notificationMatchUser.TypeOfMessage = 2;
                RepositoryService.NotificationRepository.Create(notificationMatchUser);
            }
            RepositoryService.NotificationRepository.Create(notification);
            await RepositoryService.NotificationRepository.SaveChangesAsync();
            if (matchFound && match != null)
            {
                return Ok("LikedItem entry and a Match Notification generated."+match.Id);
            }
            return Ok("LikedItem entry and a Notification about it generated.");
        }
        [HttpGet("{id:long}")]
        [AllowAnonymous]
        public async Task<IActionResult> GetLikedItem(long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var (isSuccess, likedItem) = await _likedItemRepository.QueryAsync(x => x.ItemId == id);
            if (isSuccess)
            {
                return Ok(likedItem);
            }
            return NotFound();
        }
        [HttpGet("checkIfLiked/{id}")]
        public async Task<IActionResult> CheckIfLiked(long id)
        {
            if (!ModelState.IsValid)
            {
                BadRequest(ModelState);
            }
            bool isLiked = false;
            var (isSuccess, likedItem) = await _likedItemRepository.QueryAsync(x => x.ItemId == id && x.UserId == User.Id);
            if (isSuccess)
            {
                isLiked = true;
                return Ok(isLiked);
            }
            return Ok(isLiked);
        }
        [HttpDelete("{id:long}")]
        public async Task<IActionResult> DeleteLikedItem(long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var (isSuccess, likedItems) = await _likedItemRepository.QueryAsync(x => x.ItemId == id && x.UserId == User.Id);
            if (!isSuccess && User.Role != Role.Admin)
            {
                return NotFound();
            }
            var likedItem = likedItems.FirstOrDefault();
            _likedItemRepository.Delete(likedItem);

            var (isSuccess2, notificationFromRepo) = await RepositoryService.NotificationRepository.QueryAsync(x => (x.LikedItemId == likedItem.Id) && x.AccountHolderUserId == User.Id);
            if(!isSuccess2 && User.Role != Role.Admin)
            {
                return NotFound();
            }
            var notificationToDelete = notificationFromRepo.FirstOrDefault();
            RepositoryService.NotificationRepository.Delete(notificationToDelete);
            await _likedItemRepository.SaveChangesAsync();
            return Ok();
        }
        [HttpGet("getMatchesOfUser/{id:long}")]
        public async Task<IActionResult> GetMatchesOfUser(long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var (isSuccess, userSearch) = await RepositoryService.UserRepository.QueryAsync(x => x.Id == id && x.Id == User.Id);
            if (!isSuccess && User.Role != Role.Admin)
            {
                return Forbid();
            }
            User userToPass = userSearch.ElementAt(0);
            List<Match>? matchesOfUser = new List<Match>();
            var container = await _matchMaker.CheckAllMatches(userToPass);
            bool isMatchFound = container.matchFound;
            matchesOfUser = container.matches;
            List<MatchResponseDto>? emptyToReturn = new ();
            if (!isMatchFound && matchesOfUser == null)
            {
                return Ok(emptyToReturn);
            }
            List<MatchResponseDto>? returnPackage = await GetParticipantItemsAndNames(matchesOfUser, id);
            return Ok(returnPackage);
        }
        private async Task<List<MatchResponseDto>> GetParticipantItemsAndNames(List<Match>? matches, long id)
        {
            List<long> itemsTargetOfLike = new();
            List<long> itemsLiker = new();
            List<MatchResponseDto> returnList = new();
            var caster = new EntityCaster<MatchResponseDto, Match>();
            foreach (Match match in matches)
            {
                if (match.TargetOfLikeUserId == match.ActiveLikerUserId)
                {
                    continue;
                }
                var matchDto = new MatchResponseDto();
                var targetOfLike = await RepositoryService.UserRepository.GetAsync(match.TargetOfLikeUserId);
                var activeLiker = await RepositoryService.UserRepository.GetAsync(match.ActiveLikerUserId);

                var (isSuccessMatch, usersOfMatch) = await RepositoryService.MatchRepository.QueryAsync(x => x.Id == match.Id);
                if (isSuccessMatch)
                {
                    var likerUserId = usersOfMatch.FirstOrDefault().ActiveLikerUserId;
                    var targetUserId = usersOfMatch.FirstOrDefault().TargetOfLikeUserId;
                    var likerUser = await RepositoryService.UserRepository.GetAsync(likerUserId);
                    var targetUser = await RepositoryService.UserRepository.GetAsync(targetUserId);
                    matchDto = caster.CastTo(matchDto, match);
                    matchDto.ActiveLikerUsername = likerUser.Data.Username;
                    matchDto.TargetOfLikeUsername = targetUser.Data.Username;
                    returnList.Add(matchDto);
                }
            }
            return returnList;
        }
    }
}