﻿namespace backend.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ImageController : BaseController
    {
        private static readonly string _tempFolder = Directories.TempFolder();
        private static readonly string _itemFolder = Directories.ItemImageFolder();
        private static readonly string _userFolder = Directories.UserImageFolder();
        private readonly IItemImageRepository _itemImageRepository;
        public ImageController(IJwtUtilities jwtUtilities, ISnowFlakeGenerator snowFlakeGenerator, IEmailService emailService, IRepositoryService repositoryService, IPasswordService passwordService)
            : base(jwtUtilities, snowFlakeGenerator, emailService, repositoryService, passwordService)
        {
            _itemImageRepository = RepositoryService.ItemImageRepository;
        }
        [HttpPost("tmp")]
        public async Task<IActionResult> PostImage([FromForm] IFormFile file)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var guid = Guid.NewGuid().ToString();
            var path = _tempFolder + guid + '/';
            if (file.Length == 0 || Directory.Exists(path))
            {
                return BadRequest();
            }
            try
            {
                Directory.CreateDirectory(path);
                path += "0.jpg";
                await FileMethods.SaveImage(file, path);
                return Ok(guid);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                return BadRequest();
            }
        }
        [HttpDelete("tmp")]
        public async Task<IActionResult> DeleteTmpImage(FileDeleteTmpDto fileDeleteTmpDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            string path = _tempFolder + fileDeleteTmpDto.Guid + '/';
            if (!Directory.Exists(path))
            {
                return NotFound();
            }
            if (fileDeleteTmpDto.Id == null)
            {
                FileMethods.DeleteFolderWithFiles(path);
                return Ok();
            }
            List<string> files = Directory.GetFiles(path).ToList();
            string filePath = path + fileDeleteTmpDto.Id.ToString() + ".jpg";
            if (!System.IO.File.Exists(filePath))
            {
                return NotFound();
            }
            System.IO.File.Delete(filePath);
            if (files.Count >= fileDeleteTmpDto.Id)
            {
                for (int i = (int)fileDeleteTmpDto.Id; i < files.Count; i++)
                {
                    System.IO.File.Move(path + (i + 1).ToString() + ".jpg", path + i.ToString() + ".jpg");
                }
            }
            return Ok();
        }
        [HttpPost("tmp/{guid}")]
        public async Task<IActionResult> AddImage(string guid, [FromForm] IFormFile file)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (file.Length == 0)
            {
                return BadRequest();
            }
            string path = _tempFolder + guid + '/';
            if (!Directory.Exists(path))
            {
                return NotFound();
            }
            int fileCount = Directory.GetFiles(path).Count();
            if (fileCount > 9)
            {
                return BadRequest("Too many files.");
            }
            await FileMethods.SaveImage(file, path + fileCount.ToString() + ".jpg");
            return Ok();
        }
        [HttpGet("tmp")]
        public async Task<FileStreamResult?> GetTempImage(FileGetTmpDto fileGetTmpDto)
        {
            string path = _tempFolder
                        + fileGetTmpDto.Guid
                        + '/'
                        + fileGetTmpDto.FileId.ToString()
                        + ".jpg";
            var task = Task.Run(() => GetFile(path));
            task.Wait();
            return task.Result;
        }
        [HttpGet("item/{id:long}")]
        public async Task<FileStreamResult?> GetItemImage(long id)
        {
            var path = _itemFolder + id.ToString() + ".jpg";
            var task = Task.Run(() => GetFile(path));
            task.Wait();
            return task.Result;
        }
        [HttpGet("user/{id:long}")]
        public async Task<FileStreamResult?> GetUserImage(long id)
        {
            var path = _userFolder + id.ToString() + ".jpg";
            var result = Task.Run(() => GetFile(path));
            result.Wait();
            return result.Result;
        }
        private static FileStreamResult? GetFile(string path)
        {
            try
            {
                var fs = System.IO.File.OpenRead(path);
                return new FileStreamResult(fs, "image/jpg");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                return null;
            }
        }
    }
}