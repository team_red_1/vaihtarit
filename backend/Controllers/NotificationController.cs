﻿namespace backend.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class NotificationController : BaseController
    {
        private readonly INotificationRepository _notificationRepository;
        public NotificationController(IJwtUtilities jwtUtilities, ISnowFlakeGenerator snowFlakeGenerator, IEmailService emailService, IRepositoryService repositoryService, IPasswordService passwordService)
            : base(jwtUtilities, snowFlakeGenerator, emailService, repositoryService, passwordService)
        {
            _notificationRepository = repositoryService.NotificationRepository;
        }
        [HttpGet("getNotifications")]
        public async Task<IActionResult> GetNotifications([FromQuery] NotificationGetDto notificationGetDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (notificationGetDto.Newest == null || notificationGetDto.Newest == 0)
            {
                notificationGetDto.Newest = long.MaxValue;
            }
            if (notificationGetDto.Oldest == null || notificationGetDto.Oldest == 0)
            {
                notificationGetDto.Oldest = long.MaxValue;
            }
            var notificationsToSend = _notificationRepository.GetAllAsArray()
                .Where(x => (x.OtherPersonUserId == User.Id && x.OtherPersonUserId == notificationGetDto.UserId) && (x.Id > notificationGetDto.Newest || x.Id < notificationGetDto.Oldest))
                .OrderByDescending(i => i.Id)
                .ToList();
            int counterOfNew = 0;
            var caster = new EntityCaster<NotificationGetResponseDto, Notification>();
            List<NotificationGetResponseDto> secondList = new();
            foreach (var notification in notificationsToSend)
            {
                if (notification.IsSeen == false)
                {
                    counterOfNew++;
                }
                var notificationToDto = new NotificationGetResponseDto();
                var nameOfOtherPerson = await RepositoryService.UserRepository.GetAsync(notification.OtherPersonUserId);
                notificationToDto.OtherPersonUsername = nameOfOtherPerson.Data.Username;
                var nameOfAccountHolder = await RepositoryService.UserRepository.GetAsync(notification.AccountHolderUserId);
                notificationToDto.AccountHolderUserName = nameOfAccountHolder.Data.Username;
                notificationToDto = caster.CastTo(notificationToDto, notification);
                secondList.Add(notificationToDto);
            }
            NotificationGetNewResponse notificationGetNewResponse = new();
            notificationGetNewResponse.ListOfNotifications = secondList;
            notificationGetNewResponse.NumberOfNewNotifications = counterOfNew;
            return Ok(notificationGetNewResponse);
        }
        [HttpGet("switchNotificationIsSeen")]
        public async Task<IActionResult> SwitchIsSeen([FromQuery] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var (isSuccessUser, userFromRepo) = await RepositoryService.NotificationRepository.QueryAsync(x => (x.AccountHolderUserId == User.Id) || (x.OtherPersonUserId == User.Id));
            if (!isSuccessUser && Role.User != Role.Admin)
            {
                Forbid();
            }
            var (isSuccess, notificationFromRepo) = await RepositoryService.NotificationRepository.GetAsync(id);
            if (isSuccess)
            {
                notificationFromRepo.IsSeen = true;
                RepositoryService.NotificationRepository.Update(notificationFromRepo);
                await RepositoryService.NotificationRepository.SaveChangesAsync();
                return Ok(notificationFromRepo);
            }
            return Ok("No changes made.");
        }
    }
}