﻿namespace backend.Controllers
{
    [Controller]
    public class BaseController : ControllerBase
    {
        public ISnowFlakeGenerator SnowflakeGenerator { get; set; }
        public IJwtUtilities JwtUtilities { get; set; }
        public IEmailService EmailService { get; set; }
        public IPasswordService PasswordService { get; set; }
        public IRepositoryService RepositoryService { get; set; }
        public User User => (User)HttpContext.Items["User"];
        public BaseController(IJwtUtilities jwtUtilities, ISnowFlakeGenerator snowFlakeGenerator, IEmailService emailService, IRepositoryService repositoryService, IPasswordService passwordService)
        {
            SnowflakeGenerator = snowFlakeGenerator;
            JwtUtilities = jwtUtilities;
            EmailService = emailService;
            RepositoryService = repositoryService;
            PasswordService = passwordService;
        }
    }
}