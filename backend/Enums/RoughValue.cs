﻿namespace backend.Enums
{
    public enum RoughValue
    {
        [Display(Name = "0-20")]
        One,
        [Display(Name = "21-50")]
        Two,
        [Display(Name = "51-100")]
        Three,
        [Display(Name = "101-200")]
        Four,
        [Display(Name = "201-500")]
        Five,
        [Display(Name = "501-1000")]
        Six,
        [Display(Name = "1001-")]
        Seven
    }
}
