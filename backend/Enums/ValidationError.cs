﻿namespace backend.Enums
{
    public enum ValidationError
    {
        [Display(Name = "Password must include at least one uppercase letter.\n")]
        MustIncludeUppercase,
        [Display(Name = "Password must include at least one lowercase letter.\n")]
        MustIncludeLowercase,
        [Display(Name = "Password must include at least one digit (0-9).\n")]
        MustIncludeDigit,
        [Display(Name = "Password must include at least one special character.\n")]
        MustIncludeSpecialCharacter
    }
}