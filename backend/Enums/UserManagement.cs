﻿namespace backend.Enums
{
    public enum UserManagement
    {
        [Display(Name = "User created.")]
        UserCreated,
        [Display(Name = "User deleted.")]
        UserDeleted
    }
}