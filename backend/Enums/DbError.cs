﻿namespace backend.Enums
{
    public enum DbError
    {
        [Display(Name = "Unable to connect to the database.")]
        ConnectionError,
        [Display(Name = "Connection to the database OK.")]
        ConnectionOk,

        [Display(Name = "Category already exists.")]
        CategoryAlreadyExists,
        [Display(Name = "Category not found.")]
        CategoryNotFound,

        [Display(Name = "Email already taken.")]
        EmailTaken,

        [Display(Name = "GeoLocation already exits.")]
        GeoLocationAlreadyExists,
        [Display(Name = "GeoLocation not found.")]
        GeoLocationNotFound,

        [Display(Name = "Image not found.")]
        ImageNotFound,
        
        [Display(Name = "Item already has a category.")]
        ItemAlreadyHasCategory,
        [Display(Name = "Item does not have an assigned category.")]
        ItemNoAssignedCategory,
        [Display(Name = "Item not found.")]
        ItemNotFound,

        [Display(Name = "Liked item not found.")]
        LikedItemNotFound,

        [Display(Name = "Match not found.")]
        MatchNotFound,

        [Display(Name = "Message not found.")]
        MessageNotFound,

        [Display(Name = "Offer not found.")]
        OfferNotFound,

        [Display(Name = "User not found.")]
        UserNotFound,
        [Display(Name = "User not found with this username.")]
        UserNotFoundWithUsername,
        [Display(Name = "User not found with this email.")]
        UserNotFoundWithEmail,
        [Display(Name = "Username is taken.")]
        UsernameTaken
    }
}