﻿namespace backend.Enums
{
    public enum Condition
    {
        [Display(Name = "Uusi")]
        New,
        [Display(Name = "Vähän käytetty")]
        SlightlyUsed,
        [Display(Name = "Kulunut")]
        Worn,
        [Display(Name = "Varaosiksi")]
        SpareParts,
    }
}
