﻿namespace backend.Authorization
{
    public class JwtMiddleware
    {
        private readonly RequestDelegate _request;
        private readonly AuthenticationOptions _authenticationOptions;
        public JwtMiddleware(RequestDelegate request, IOptions<AuthenticationOptions> authenticationOptions)
        {
            _request = request;
            _authenticationOptions = authenticationOptions.Value;
        }
        public async Task Invoke(HttpContext context, IRepositoryService repository)
        {
            var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            if (token != null)
            {
                AttachUserToContext(context, repository, token);
            }
            await _request(context);
        }
        private void AttachUserToContext(HttpContext context, IRepositoryService repository, string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_authenticationOptions.Secret);
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);
                var jwtToken = (JwtSecurityToken)validatedToken;
                var userId = long.Parse(jwtToken.Claims.First(x => x.Type == "id").Value);
                var (isSuccess, user) = repository.UserRepository.Get(userId);
                if (isSuccess)
                {
                    context.Items["User"] = user;
                }
            }
            catch
            {
            }
        }
    }
}