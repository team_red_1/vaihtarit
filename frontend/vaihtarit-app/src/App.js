import React from "react";
import { ThemeProvider } from "@mui/material";
import AppContainer from "./Components/AppContainer";
import theme from "./Components/Themes/Theme";
import { UserProvider } from "./Components/UserContext";
import { SnackbarContext } from "./Components/Snackbar/SnackbarContext";

const App = () => (
	<ThemeProvider theme={theme}>
		<UserProvider>
			<SnackbarContext>
				<AppContainer />
			</SnackbarContext>
		</UserProvider>
	</ThemeProvider>
);

export default App;
