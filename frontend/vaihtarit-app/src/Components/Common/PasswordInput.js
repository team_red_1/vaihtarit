import React, { useEffect, useState } from "react";
import { IconButton, InputAdornment, TextField } from "@mui/material";
import { Visibility, VisibilityOff } from "@mui/icons-material";

export const PasswordInput = (props) => {
	const [showPassword, setShowPassword] = useState(false);
	const [pass, setPass] = useState("");

	const handleClickShowPassword = (e) => {
		e.preventDefault();
		setShowPassword(!showPassword);
	};

	useEffect(() => {
		props.changePasswordState(pass);
	}, [pass]);

	return (
		<TextField
			value={props.password}
			onChange={(e) => setPass(e.target.value)}
			fullWidth={true}
			id="userPassword"
			autoComplete="current-password"
			variant="filled"
			label={props.title}
			type={showPassword ? "text" : "password"}
			InputProps={{
				endAdornment: (
					<InputAdornment position="end">
						<IconButton
							aria-label="toggle password visibility"
							tabIndex={-1}
							onClick={handleClickShowPassword}
							edge="end"
						>
							{showPassword ? <VisibilityOff /> : <Visibility />}
						</IconButton>
					</InputAdornment>
				)
			}}
			sx={{ backgroundColor: "white" }}
		/>
	);
};
