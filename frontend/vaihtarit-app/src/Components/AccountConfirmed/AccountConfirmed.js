import React, { useState, useEffect, useContext } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { UserContext } from "../UserContext";

export const AccountConfirmed = () => {
	const [outputText, setOutputText] = useState("Tiliäsi vahvistetaan...");
	const navigate = useNavigate();
	const { id } = useParams();
	const {setShowLoginModal} = useContext(UserContext);

	const confirmationSuccessfull = () => {
		setOutputText(
			"Käyttäjätilisi on nyt vahvistettu ja olet valmis tekemään vaihtarit!"
		);
		setTimeout(() => {
			navigate("/");
			setShowLoginModal(true);
		} , 5000);
		
	};

	const confirmationDenied = (error) => {
		if (error.request)
			setOutputText("Yhteyden luominen palvelimeen epäonnistui");
		if (error.response.status === 403)
			setOutputText("Tapahtui virhe, yritä uudelleen.");
		else setOutputText("Jokin meni pieleen");
	};

	const sendConfirmation = () => {
		axios({
			method: "patch",
			url: `https://localhost:5100/api/User/confirm/${id}`
		})
			.then((res) => {
				if (res.status === 200) confirmationSuccessfull();
			})
			.catch((error) => {
				confirmationDenied(error);
			});
	};

	useEffect(() => {
		sendConfirmation();
	}, []);

	return <div>{outputText}</div>;
};
