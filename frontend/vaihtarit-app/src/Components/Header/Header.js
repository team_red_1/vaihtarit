import React, { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { UserContext } from "../UserContext";
import { AppBar, Box, Toolbar, Button, Stack } from "@mui/material";
import { Navimenu } from "./Navimenu/Navimenu";

import Logo from "../../Assets/logo.png";
import ItemPostForm from "../Item/ItemPostForm/ItemPostForm";
import AddIcon from "@mui/icons-material/Add";
import LoginIcon from "@mui/icons-material/Login";
import PersonAddAltIcon from "@mui/icons-material/PersonAddAlt";
import SearchIcon from "@mui/icons-material/Search";
import { NotificationList } from "./Notifications/NotificationList";

export const Header = () => {
	const navigate = useNavigate();
	const [showItemPostModal, setShowItemPostModal] = useState(false);
	const {
		setShowLoginModal,
		setShowRegisterModal,
		isLogged,
		setOpenFilters,
		setFilterAnchorEl
	} = useContext(UserContext);

	const openFilterForm = (event) => {
		setFilterAnchorEl(event.currentTarget);
		setOpenFilters(true);
	};

	return (
		<>
			<AppBar
				position="relative"
				style={{ justifyContent: "space-between", height: "60px" }}
			>
				<Toolbar>
					<Box
						style={{
							flex: 1,
							flexGrow: 1
						}}
					>
						<img
							src={Logo}
							style={{
								height: "3em",
								cursor: "pointer"
							}}
							onClick={() => navigate("/")}
						/>
					</Box>

					{isLogged && (
						<>
							<Box
								style={{
									flex: 0,
									flexGrow: 0,
									alignSelf: "auto",
									minWidth: "fit-content"
								}}
							>
								<Button
									color="secondary"
									variant="contained"
									onClick={() => setShowItemPostModal(true)}
									startIcon={<AddIcon />}
								>
									Luo ilmoitus
								</Button>
							</Box>

							<Box
								style={{
									flex: 1,
									flexGrow: 1,
									marginLeft: "auto"
								}}
							>
								<Stack
									direction="row"
									alignItems="center"
									justifyContent={"right"}
									spacing={1}
								>
									<SearchIcon
										onClick={() => openFilterForm(event)}
										cursor="pointer"
									/>
									<NotificationList />
									<Navimenu />
								</Stack>
							</Box>
						</>
					)}

					{!isLogged && (
						<Box>
							<Stack direction="row" spacing={3}>
								<Button
									color="secondary"
									variant="contained"
									onClick={() => setShowRegisterModal(true)}
									startIcon={<PersonAddAltIcon />}
								>
									Luo tunnus
								</Button>
								<Button
									color="inherit"
									onClick={() => setShowLoginModal(true)}
									startIcon={<LoginIcon />}
								>
									Kirjaudu
								</Button>
							</Stack>
						</Box>
					)}
				</Toolbar>
			</AppBar>

			<ItemPostForm
				showItemPostModal={showItemPostModal}
				setShowItemPostModal={setShowItemPostModal}
			/>
		</>
	);
};
