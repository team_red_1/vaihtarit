import React from "react";
import { Divider, MenuItem, Typography } from "@mui/material";
import MessageOutlinedIcon from "@mui/icons-material/MessageOutlined";
import FavoriteBorderOutlinedIcon from "@mui/icons-material/FavoriteBorderOutlined";
import DoneIcon from "@mui/icons-material/Done";

export const NotificationItem = ({
	notification,
	notifications,
	onClick,
	index
}) => (
	<div>
		<MenuItem onClick={onClick} sx={{ margin: "5px" }}>
			<Typography
				variant="body1"
				color={"text.secondary"}
				sx={{ paddingRight: "5px" }}
			>
				{notification.typeOfMessage === 1 && (
					<>
						<FavoriteBorderOutlinedIcon
							fontSize="small"
							sx={{ paddingRight: "5px" }}
						/>
						Tykkäys käyttäjältä {notification.accountHolderUserName}
					</>
				)}
				{notification.typeOfMessage === 2 && (
					<>
						<DoneIcon
							fontSize="small"
							sx={{ paddingRight: "5px" }}
						/>{" "}
						Mahdollinen vaihtari{" "}
						{notification.accountHolderUserName}:n kanssa!
					</>
				)}
				{notification.typeOfMessage === 3 && (
					<>
						<MessageOutlinedIcon
							fontSize="small"
							sx={{ paddingRight: "5px" }}
						/>
						Viesti käyttäjältä {notification.accountHolderUserName}
					</>
				)}
			</Typography>
			<Typography variant="caption" sx={{ paddingRight: "5px" }}>
				{!notification.isSeen && <>(Uusi)</>}
			</Typography>
		</MenuItem>
		{notifications.length > 1 && index < notifications.length - 1 && (
			<Divider />
		)}
	</div>
);
