import React, { useEffect, useState, useContext } from "react";
import { Alert, Badge, IconButton, Menu } from "@mui/material";
import NotificationsNoneIcon from "@mui/icons-material/NotificationsNone";
import NotificationsActiveTwoToneIcon from "@mui/icons-material/NotificationsActiveTwoTone";
import { NotificationItem } from "./NotificationItem";

import axios from "axios";
import { UserContext } from "../../UserContext";
import { useNavigate } from "react-router-dom";

export const NotificationList = () => {
	const [anchorEl, setAnchorEl] = React.useState(null);
	const [notificationCounter, setNotificationCounter] = useState(0);
	const [notifications, setNotifications] = useState([]);
	const [error, setError] = useState(false);
	const { accessToken, userId, setOtherUserId, setOtherUserName } =
		useContext(UserContext);
	const navigate = useNavigate();

	const styles = {
		notificationBellAnimation: {
			animation: "shake 5s",
			animationDelay: "2s",
			animationIterationCount: "infinite",
			"@keyframes shake": {
				"0%": {
					transform: "translate(1px, 1px) rotate(3deg)"
				},
				"10%": {
					transform: "translate(-2px, -1px) rotate(-5deg)"
				},
				"20%": {
					transform: "translate(-2px, 0px) rotate(5deg)",
					color: "green"
				},
				"30%": {
					transform: "translate(1px, 0px) rotate(-4deg)"
				}
			}
		},
		notificationList: {
			maxWidth: "500px",
			maxHeight: "800px",
			overflow: "auto"
		}
	};

	const getNotifications = () => {
		axios({
			method: "get",
			url: "https://localhost:5100/api/Notification/getNotifications",
			headers: {
				Authorization: "Bearer " + accessToken,
				"Content-Type": "application/json"
			},
			params: {
				UserId: userId,
				newest: null,
				oldest: null
			}
		})
			.then((res) => {
				setNotifications(res.data.listOfNotifications);
				setNotificationCounter(res.data.numberOfNewNotifications);
			})
			.catch(() => setError(true));
	};

	useEffect(() => {
		getNotifications();
		setInterval(getNotifications, 1000);
	}, []);

	const openNotifications = (event) => {
		setAnchorEl(event.currentTarget);
	};

	const closeNotifications = () => {
		setAnchorEl(null);
	};

	const clickNotification = (notification) => {
		if (!notification.isSeen) {
			axios({
				method: "get",
				url: "https://localhost:5100/api/Notification/switchNotificationIsSeen",
				headers: {
					Authorization: "Bearer " + accessToken,
					"Content-Type": "application/json"
				},
				params: {
					Id: notification.id
				}
			}).catch(() => setError(true));
			getNotifications();
		}
		setOtherUserId(notification.accountHolderUserId);
		setOtherUserName(notification.accountHolderUserName);

		if (notification.typeOfMessage === 1) navigate("/otheruserposts");
		if (
			notification.typeOfMessage === 2 ||
			notification.typeOfMessage === 3
		)
			navigate("/messages");

		closeNotifications();
	};

	return (
		<>
			<IconButton
				size="large"
				aria-controls="menu-appbar"
				aria-haspopup="true"
				onClick={openNotifications}
				color="inherit"
			>
				{notificationCounter > 0 && (
					<Badge badgeContent={notificationCounter} color="secondary">
						<NotificationsActiveTwoToneIcon
							sx={styles.notificationBellAnimation}
						/>
					</Badge>
				)}
				{notificationCounter === 0 && <NotificationsNoneIcon />}
			</IconButton>
			<Menu
				id="menu-appbar"
				anchorEl={anchorEl}
				anchorOrigin={{
					vertical: "bottom",
					horizontal: "right"
				}}
				keepMounted
				transformOrigin={{
					vertical: "top",
					horizontal: "right"
				}}
				open={Boolean(anchorEl)}
				onClose={closeNotifications}
			>
				{!error && notifications.length === 0 && (
					<div style={{ padding: "5px" }}>Ei tapahtumia</div>
				)}
				{error && <Alert severity="error">Jokin meni pieleen</Alert>}
				<div style={styles.notificationList}>
					{!error &&
						notifications.map((notification, index) => (
							<NotificationItem
								key={notification.id}
								notification={notification}
								notifications={notifications}
								onClick={() => clickNotification(notification)}
								index={index}
							/>
						))}
				</div>
			</Menu>
		</>
	);
};
