import React, { useContext } from "react";
import { Link } from "react-router-dom";
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import { UserContext } from "../../UserContext";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";

export const Navimenu = () => {
	const [anchorEl, setAnchorEl] = React.useState(null);
	const { logout, username } = useContext(UserContext);
	const handleClick = (event) => {
		setAnchorEl(event.currentTarget);
	};
	const handleClose = () => {
		setAnchorEl(null);
	};
	return (
		<>
			<Button
				size="large"
				aria-label="account of current user"
				aria-controls="menu-appbar"
				aria-haspopup="true"
				onClick={handleClick}
				color="inherit"
				startIcon={<AccountCircleIcon />}
			>
				{username}
			</Button>
			<Menu
				id="menu-appbar"
				anchorEl={anchorEl}
				anchorOrigin={{
					vertical: "bottom",
					horizontal: "right"
				}}
				keepMounted
				transformOrigin={{
					vertical: "top",
					horizontal: "right"
				}}
				open={Boolean(anchorEl)}
				onClose={handleClose}
			>
				<MenuItem
					onClick={handleClose}
					component={Link}
					to="/usersettings"
				>
					Tiliasetukset
				</MenuItem>
				<MenuItem onClick={handleClose} component={Link} to="/messages">
					Viestit
				</MenuItem>
				<MenuItem onClick={handleClose} component={Link} to="/myposts">
					Omat ilmoitukset
				</MenuItem>
				<MenuItem onClick={handleClose} component={Link} to="/mylikes">
					Omat tykkäykset
				</MenuItem>
				<MenuItem onClick={logout} component={Link} to="/">
					Kirjaudu ulos
				</MenuItem>
			</Menu>
		</>
	);
};
