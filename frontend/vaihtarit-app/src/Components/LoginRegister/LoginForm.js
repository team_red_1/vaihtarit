import { React, useState, useContext } from "react";
import axios from "axios";
import { UserContext } from "../UserContext";
import {
	Alert,
	Modal,
	Box,
	Button,
	Stack,
	TextField,
	IconButton
} from "@mui/material";
import CancelIcon from "@mui/icons-material/Cancel";
import { PasswordInput } from "../Common/PasswordInput";

export const LoginForm = () => {
	const [userNameOrEmail, setUserNameOrEmail] = useState("");
	const [userPassword, setUserPassword] = useState("");
	const [errorText, setErrorText] = useState("");

	const {
		loginUser,
		saveUserToLocalStorage,
		showLoginModal,
		setShowLoginModal,
		setShowRegisterModal
	} = useContext(UserContext);

	const modalStyle = {
		position: "absolute",
		top: "50%",
		left: "50%",
		transform: "translate(-50%, -50%)",
		width: 400,
		bgcolor: "secondary.main",
		boxShadow: 5,
		p: 4
	};

	const loginSuccesful = (res) => {
		loginUser(res.data.token, res.data.id, res.data.username, true);
		saveUserToLocalStorage(res.data.token, res.data.id, res.data.username);
		setUserNameOrEmail("");
		setUserPassword("");
		setShowLoginModal(false);
		setErrorText("");
	};

	const loginDenied = (error) => {
		if (error.response) {
			if (error.response.status === 403 || error.response.status === 400)
				setErrorText("Väärä käyttäjänimi tai salasana");
		} else if (error.request) {
			setErrorText("Yhteyden luominen palvelimeen epäonnistui");
		} else {
			setErrorText("Jokin meni pieleen");
		}
	};

	const handleLogin = (e) => {
		e.preventDefault();

		if (userNameOrEmail.length < 2) {
			setErrorText("Kirjoita käyttäjänimi tai sähköposti");
			return;
		}
		if (userPassword.length < 6) {
			setErrorText("Salasanan täytyy olla vähintään 6 merkkiä");
			return;
		}

		axios({
			method: "post",
			url: "https://localhost:5100/api/User/authenticate",
			data: {
				usernameOrEmail: userNameOrEmail,
				password: userPassword
			}
		})
			.then((res) => {
				if (res.status === 200) loginSuccesful(res);
			})
			.catch((error) => loginDenied(error));
	};

	const switchToRegistering = () => {
		setUserNameOrEmail("");
		setUserPassword("");
		setErrorText("");
		setShowLoginModal(false);
		setShowRegisterModal(true);
	};

	const closeLoginModal = () => {
		setUserNameOrEmail("");
		setUserPassword("");
		setErrorText("");
		setShowLoginModal(false);
	};

	const changePasswordState = (password) => {
		setUserPassword(password);
	};

	return (
		<Modal open={showLoginModal} onClose={closeLoginModal}>
			<Box sx={modalStyle}>
				<IconButton
					onClick={closeLoginModal}
					tabIndex={-1}
					style={{
						position: "absolute",
						right: 0,
						top: 0
					}}
				>
					<CancelIcon />
				</IconButton>
				<form onSubmit={handleLogin}>
					Kirjaudu sisään
					<br />
					<br />
					<TextField
						value={userNameOrEmail}
						onChange={(e) => setUserNameOrEmail(e.target.value)}
						fullWidth={true}
						id="username"
						autoComplete="username"
						variant="filled"
						label={"Tunnus"}
						sx={{ backgroundColor: "white" }}
					/>
					<label>
						<br />
						<br />
						<PasswordInput
							changePasswordState={changePasswordState}
							title={"Salasana"}
						/>
					</label>
					<br />
					<br />
					{errorText !== "" && (
						<Alert variant="filled" severity="error">
							{errorText}
						</Alert>
					)}
					<br />
					<Stack direction="row" spacing={2}>
						<Button
							type={"submit"}
							tabIndex={-1}
							sx={{
								color: "primary.buttonText"
							}}
							variant="contained"
						>
							Kirjaudu
						</Button>
						<Button
							sx={{ color: "primary.buttonText" }}
							tabIndex={-1}
							variant="contained"
							onClick={(e) => switchToRegistering(e)}
						>
							Luo tunnus
						</Button>
					</Stack>
				</form>
			</Box>
		</Modal>
	);
};
