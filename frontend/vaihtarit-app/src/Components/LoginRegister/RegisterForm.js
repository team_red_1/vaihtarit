import { React, useState, useContext } from "react";
import axios from "axios";
import {
	Alert,
	Button,
	Stack,
	Modal,
	Box,
	TextField,
	IconButton
} from "@mui/material";
import { UserContext } from "../UserContext";
import CancelIcon from "@mui/icons-material/Cancel";
import { PasswordInput } from "../Common/PasswordInput";
import { MySnackbarContext } from "../Snackbar/SnackbarContext";

export const RegisterForm = () => {
	const [userName, setUserName] = useState("");
	const [password, setPassword] = useState("");
	const [email, setEmail] = useState("");
	const [errorText, setErrorText] = useState("");

	const { setShowLoginModal, setShowRegisterModal, showRegisterModal } =
		useContext(UserContext);
	const { openSnackbar } = useContext(MySnackbarContext);

	const modalStyle = {
		position: "absolute",
		top: "50%",
		left: "50%",
		transform: "translate(-50%, -50%)",
		width: 400,
		bgcolor: "secondary.main",
		boxShadow: 5,
		p: 4
	};

	const signupSuccessfull = () => {
		openSnackbar("Tunnus luotu onnistuneesti");
		setUserName("");
		setEmail("");
		setPassword("");
		switchToLogin();
	};

	const signupDenied = (error) => {
		if (error.response.status === 409)
			setErrorText("Käyttäjänimi tai sähköposti on jo käytössä");
		else setErrorText("Jokin meni pieleen");
	};

	const containsSpecialChars = (str) => {
		const specialChars = "`!#$%^&*()+=[]{};':\"\\|,<>/?~";
		const result = specialChars.split("").some((specialChar) => {
			if (str.includes(specialChar)) return true;

			return false;
		});
		return result;
	};

	const handleSignup = (e) => {
		e.preventDefault();
		const regexEmail = new RegExp("[a-z0-9]+@[a-z]+.[a-z]{2,3}");
		const regexPasswordlower = new RegExp("(?=.*[a-z])");
		const regexPasswordupper = new RegExp("(?=.*[A-Z])");

		if (containsSpecialChars(userName)) {
			setErrorText("Käyttäjänimessä ei saa esiintyä erikoismerkkejä");
			return;
		}

		if (userName.length < 2) {
			setErrorText("Käyttäjänimen täytyy olla vähintään 2 merkkiä pitkä");
			return;
		}

		if (password.length < 6) {
			setErrorText("Salasanan täytyy olla vähintään 6 merkkiä");
			return;
		}

		if (
			!regexPasswordlower.test(password) ||
			!regexPasswordupper.test(password)
		) {
			setErrorText(
				"Salasanassa täytyy olla ainakin yksi pieni ja iso kirjain"
			);
			return;
		}

		if (!regexEmail.test(email)) {
			setErrorText("Sähköposti on virheellisessä muodossa");
			return;
		}

		axios({
			method: "post",
			url: "https://localhost:5100/api/User",
			data: {
				Username: userName,
				Password: password,
				Email: email
			}
		})
			.then((res) => {
				if (res.status === 200) signupSuccessfull();
			})
			.catch((error) => {
				signupDenied(error);
			});
	};

	const switchToLogin = () => {
		setUserName("");
		setEmail("");
		setPassword("");
		setErrorText("");
		setShowRegisterModal(false);
		setShowLoginModal(true);
	};

	const closeRegisterModal = () => {
		setUserName("");
		setEmail("");
		setPassword("");
		setErrorText("");
		setShowRegisterModal(false);
	};

	const changePasswordState = (password) => {
		setPassword(password);
	};

	return (
		<>
			<Modal open={showRegisterModal} onClose={closeRegisterModal}>
				<Box sx={modalStyle}>
					<IconButton
						onClick={closeRegisterModal}
						tabIndex={-1}
						style={{
							position: "absolute",
							right: 0,
							top: 0
						}}
					>
						<CancelIcon />
					</IconButton>
					<form onSubmit={handleSignup}>
						Luo tunnus
						<br />
						<br />
						<TextField
							value={userName}
							onChange={(e) => setUserName(e.target.value)}
							fullWidth={true}
							id="userName"
							autoComplete="username"
							variant="filled"
							label={"Käyttäjätunnus"}
							sx={{ backgroundColor: "white" }}
							inputProps={{ maxLength: 20 }}
							helperText={
								userName.length < 20
									? ""
									: "Käyttäjänimi ei voi olla pidempi kuin 20 merkkiä"
							}
						/>
						<br />
						<br />
						<PasswordInput
							changePasswordState={changePasswordState}
							title={"Salasana"}
						/>
						<br />
						<br />
						<TextField
							value={email}
							onChange={(e) => setEmail(e.target.value)}
							fullWidth={true}
							id="userEmail"
							autoComplete="email"
							variant="filled"
							label={"Sähköposti"}
							sx={{ backgroundColor: "white" }}
						/>
						<br />
						<br />
						{errorText !== "" && (
							<Alert variant="filled" severity="error">
								{errorText}
							</Alert>
						)}
						<br />
						<Stack direction="row" spacing={2}>
							{" "}
							<Button
								type={"submit"}
								tabIndex={-1}
								sx={{
									color: "primary.buttonText"
								}}
								variant="contained"
							>
								Tallenna
							</Button>
							<Button
								sx={{ color: "primary.buttonText" }}
								tabIndex={-1}
								variant="contained"
								onClick={(e) => switchToLogin(e)}
							>
								Sisäänkirjautuminen
							</Button>
						</Stack>
					</form>
				</Box>
			</Modal>
		</>
	);
};
