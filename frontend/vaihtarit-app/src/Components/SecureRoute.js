import React, { useContext } from "react";
import { Navigate } from "react-router-dom";
import { UserContext } from "./UserContext";

export const SecureRoute = ({ children, redirectTo }) => {
	const checkIfUser = useContext(UserContext);

	return checkIfUser.isLogged ? children : <Navigate to={redirectTo} />;
};
