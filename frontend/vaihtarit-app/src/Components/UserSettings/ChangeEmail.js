import React, { useState, useContext } from "react";
import { Alert, Modal, Box, Stack, Button, TextField } from "@mui/material";
import axios from "axios";
import { UserContext } from "../UserContext";
import { MySnackbarContext } from "../Snackbar/SnackbarContext";

export const ChangeEmail = ({ closeEmailChangeModal }) => {
	const [newEmail, setNewEmail] = useState("");
	const [newEmailConfirm, setNewEmailConfirm] = useState("");
	const [errorText, setErrorText] = useState("");
	const { userId, accessToken } = useContext(UserContext);
	const { openSnackbar } = useContext(MySnackbarContext);

	// to do: get modal styles from mui theming
	const modalStyle = {
		position: "absolute",
		top: "50%",
		left: "50%",
		transform: "translate(-50%, -50%)",
		width: 400,
		bgcolor: "secondary.main",
		boxShadow: 5,
		p: 4
	};

	const cancelEmailChange = () => {
		setNewEmail("");
		setNewEmailConfirm("");
		closeEmailChangeModal(false);
	};

	const handleSuccessfulChange = () => {
		setNewEmail("");
		setNewEmailConfirm("");
		closeEmailChangeModal(false);
		openSnackbar("Email vaihdettu onnistuneesti!");
	};

	const handleEmailChange = (e) => {
		e.preventDefault();

		const regex = new RegExp("[a-z0-9]+@[a-z]+.[a-z]{2,3}");

		if (!regex.test(newEmail)) {
			setErrorText("Sähköposti on väärässä muodossa!");
			return;
		}

		if (newEmail !== newEmailConfirm) {
			setErrorText("Sähköpostit eivät täsmää!");
			return;
		}

		axios({
			method: "patch",
			url: "https://localhost:5100/api/User/",
			headers: {
				Authorization: "Bearer " + accessToken
			},
			data: { Id: userId, email: newEmail }
		})
			.then(() => {
				handleSuccessfulChange();
			})
			.catch(() => {
				setErrorText(
					"Sähköpostin vaihtaminen epäonnistui, yritä uudelleen"
				);
			});
	};

	return (
		<>
			<Modal open={true} onClick={cancelEmailChange}>
				<Box sx={modalStyle}>
					<form onSubmit={handleEmailChange}>
						Vaihda sähköpostiosoite
						<br />
						<br />
						<TextField
							value={newEmail}
							onChange={(e) => setNewEmail(e.target.value)}
							fullWidth={true}
							id="filled-basic"
							variant="filled"
							label={"Uusi sähköposti"}
							sx={{ backgroundColor: "white" }}
						/>
						<br />
						<br />
						<TextField
							value={newEmailConfirm}
							onChange={(e) => setNewEmailConfirm(e.target.value)}
							fullWidth={true}
							id="filled-basic"
							variant="filled"
							label={"Toista uusi sähköposti"}
							sx={{
								backgroundColor: "white"
							}}
						/>
						<br />
						<br />
						{errorText !== "" && (
							<Alert variant="filled" severity="error">
								{errorText}
							</Alert>
						)}
						<br />
						<Stack direction="row" spacing={2}>
							<Button
								sx={{ color: "primary.buttonText" }}
								variant="contained"
								onClick={(e) => handleEmailChange(e)}
							>
								Tallenna
							</Button>

							<Button
								variant="text"
								sx={{ color: "secondary.buttonText" }}
								onClick={() => cancelEmailChange()}
							>
								Peruuta
							</Button>
						</Stack>
					</form>
				</Box>
			</Modal>
		</>
	);
};
