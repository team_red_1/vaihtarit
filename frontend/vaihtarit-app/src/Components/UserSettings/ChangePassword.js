import React, { useState, useContext } from "react";
import { Alert, Modal, Box, Button, Stack } from "@mui/material";
import axios from "axios";
import { UserContext } from "../UserContext";
import { MySnackbarContext } from "../Snackbar/SnackbarContext";
import { PasswordInput } from "../Common/PasswordInput";

export const ChangePassword = ({ closePasswordChangeModal }) => {
	const [newPassword, setNewPassword] = useState("");
	const [newPasswordConfirm, setNewPasswordConfirm] = useState("");
	const [oldPass, setOldPass] = useState("");
	const [errorText, setErrorText] = useState("");
	const { userId, accessToken } = useContext(UserContext);
	const { openSnackbar } = useContext(MySnackbarContext);

	// to do: get modal styles from mui theming
	const modalStyle = {
		position: "absolute",
		top: "50%",
		left: "50%",
		transform: "translate(-50%, -50%)",
		width: 400,
		bgcolor: "secondary.main",
		boxShadow: 5,
		p: 4
	};

	const handlePasswordChange = (e) => {
		e.preventDefault();

		if (newPassword.length < 8) {
			setErrorText("Salasanan täytyy olla vähintään 8 merkkiä");
			return;
		}

		// Todo: missing  error handling, error display when backend returns error, missing clear input fields & close modal after success

		if (newPassword === newPasswordConfirm)
			axios({
				method: "put",
				url: "https://localhost:5100/api/User/password",
				headers: {
					Authorization: "Bearer " + accessToken
				},
				data: {
					Id: userId,
					oldPassword: oldPass,
					// eslint-disable-next-line object-shorthand
					newPassword: newPassword
				}
			})
				.then(() => {
					handleSuccessfulPassChange();
				})
				.catch(() => {
					setErrorText(
						"Salasanan vaihtaminen epäonnistui, yritä uudelleen"
					);
				});
		else setErrorText("Salasanat eivät täsmää!");
	};

	const handleSuccessfulPassChange = () => {
		setNewPassword("");
		setNewPasswordConfirm("");
		setOldPass("");
		closePasswordChangeModal();
		openSnackbar("Salasana vaihdettu onnistuneesti!");
	};

	const cancelPasswordChange = () => {
		setNewPassword("");
		setNewPasswordConfirm("");
		setOldPass("");
		closePasswordChangeModal();
	};

	const changeNewPasswordState = (password) => {
		setNewPassword(password);
	};

	const changeRepeatPasswordState = (password) => {
		setNewPasswordConfirm(password);
	};

	const changeOldPasswordState = (password) => {
		setOldPass(password);
	};

	return (
		<>
			<Modal open={true} onClose={cancelPasswordChange}>
				<Box sx={modalStyle}>
					<form onSubmit={handlePasswordChange}>
						Vaihda salasana
						<br />
						<br />
						<PasswordInput
							changePasswordState={changeOldPasswordState}
							title={"Vanha salasana"}
						/>
						<br />
						<br />
						<PasswordInput
							changePasswordState={changeNewPasswordState}
							title={"Uusi salasana"}
						/>
						<br />
						<br />
						<PasswordInput
							changePasswordState={changeRepeatPasswordState}
							title={"Toista uusi salasana"}
						/>
						<br />
						<br />
						{errorText !== "" && (
							<Alert variant="filled" severity="error">
								{errorText}
							</Alert>
						)}
						<br />
						<Stack direction="row" spacing={2}>
							<Button
								sx={{
									bgcolor: "primary.main",
									color: "primary.buttonText"
								}}
								variant="contained"
								onClick={(e) => handlePasswordChange(e)}
							>
								Tallenna
							</Button>
							<Button
								variant="text"
								sx={{ color: "secondary.buttonText" }}
								onClick={() => cancelPasswordChange()}
							>
								Peruuta
							</Button>
						</Stack>
					</form>
				</Box>
			</Modal>
		</>
	);
};
