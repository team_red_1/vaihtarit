import React, { useState, useContext } from "react";
import { Alert, Button, Stack, Modal, Box } from "@mui/material";
import axios from "axios";
import { UserContext } from "../UserContext";
import { MySnackbarContext } from "../Snackbar/SnackbarContext";
import { PasswordInput } from "../Common/PasswordInput";

export const DeleteAccount = ({ closeDeleteAccountModal }) => {
	const [userPassword, setUserPassword] = useState("");
	const [userPasswordConfirm, setUserPasswordConfirm] = useState("");
	const [errorText, setErrorText] = useState("");

	const { userId, accessToken, logout } = useContext(UserContext);
	const { openSnackbar } = useContext(MySnackbarContext);

	const modalStyle = {
		position: "absolute",
		top: "50%",
		left: "50%",
		transform: "translate(-50%, -50%)",
		width: 400,
		bgcolor: "secondary.main",
		boxShadow: 5,
		p: 4
	};

	const deleteAccountDenied = (error) => {
		if (error.response) {
			if (error.response.status === 403 || error.response.status === 400)
				setErrorText("Väärä salasana");
		} else if (error.request) {
			setErrorText("Yhteyden luominen palvelimeen epäonnistui");
		} else {
			setErrorText("Jokin meni pieleen");
		}
	};

	const deleteAccountSuccessful = () => {
		logout();
		openSnackbar("Tili on poistettu onnistuneesti!");
	};

	const handleDeleteAccount = (e) => {
		e.preventDefault();

		if (userPassword.length === 0 || userPasswordConfirm.length === 0) {
			setErrorText("Kirjoita salasana molempiin kohtiin");
			return;
		}

		if (userPassword !== userPasswordConfirm) {
			setErrorText("Salasanat eivät täsmää keskenään");
			return;
		}

		axios({
			method: "delete",
			url: `https://localhost:5100/api/User/${userId}`,
			headers: {
				// "Content-type": "application/json",
				Authorization: "Bearer " + accessToken
			},
			data: { password: userPassword }
		})
			.then(deleteAccountSuccessful())
			.catch((error) => deleteAccountDenied(error));
	};

	const cancelDeleteAccount = () => {
		setUserPassword("");
		setUserPasswordConfirm("");
		closeDeleteAccountModal();
	};

	const changePasswordState = (password) => {
		setUserPassword(password);
	};

	const changeRepeatPasswordState = (password) => {
		setUserPasswordConfirm(password);
	};

	return (
		<>
			<Modal open={true} onClick={cancelDeleteAccount}>
				<Box sx={modalStyle}>
					<form onSubmit={handleDeleteAccount}>
						Hyväksy käyttäjätunnuksen poistaminen salasanalla
						<br />
						<br />
						<PasswordInput
							changePasswordState={changePasswordState}
							title={"Anna salasana"}
						/>
						<br />
						<br />
						<PasswordInput
							changePasswordState={changeRepeatPasswordState}
							title={"Toista salasana"}
						/>
						<br />
						<br />
						{errorText !== "" && (
							<Alert variant="filled" severity="error">
								{errorText}
							</Alert>
						)}
						<br />
						<Stack direction="row" spacing={2}>
							<Button
								sx={{ color: "primary.buttonText" }}
								variant="contained"
								onClick={(e) => handleDeleteAccount(e)}
							>
								Hyväksy
							</Button>
							<Button
								variant="text"
								sx={{ color: "secondary.buttonText" }}
								onClick={() => cancelDeleteAccount()}
							>
								Peruuta
							</Button>
						</Stack>
					</form>
				</Box>
			</Modal>
		</>
	);
};
