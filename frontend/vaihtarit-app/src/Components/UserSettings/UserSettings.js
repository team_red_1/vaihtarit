import React, { useContext, useState, useEffect } from "react";
import { Button, Stack, Box, Alert } from "@mui/material";
import axios from "axios";
import { ChangeEmail } from "./ChangeEmail";
import { DeleteAccount } from "./DeleteAccount";
import { ChangePassword } from "./ChangePassword";
import { UserContext } from "../UserContext";

const UserSettings = () => {
	const { username, accessToken, userId } = useContext(UserContext);
	const [likes, setLikes] = useState();
	const [posts, setPosts] = useState();
	const [matches, setMatches] = useState();
	const [error, setError] = useState(false);
	const [showEmailChangeModal, setShowEmailChangeModal] = useState(false);
	const [showDeleteAccountModal, setShowDeleteAccountModal] = useState(false);
	const [showPasswordChangeModal, setShowPasswordChangeModal] =
		useState(false);

	const closeDeleteAccountModal = () => {
		setShowDeleteAccountModal(false);
	};

	const closeEmailChangeModal = () => {
		setShowEmailChangeModal(false);
	};

	const closePasswordChangeModal = () => {
		setShowPasswordChangeModal(false);
	};

	const getUserItems = axios({
		method: "get",
		url: `https://localhost:5100/api/Item/user-items/${userId}`,
		headers: {
			Authorization: "Bearer " + accessToken,
			"Content-type": "application/json; charset=utf-8"
		}
	});

	const getUserLikes = axios({
		method: "get",
		url: "https://localhost:5100/api/Item/user-likes",
		headers: {
			Authorization: "Bearer " + accessToken,
			"Content-type": "application/json; charset=utf-8"
		}
	});

	const getUserMatches = axios({
		method: "GET",
		url: `https://localhost:5100/api/LikedItem/getMatchesOfUser/${userId}`,
		headers: {
			Authorization: "Bearer " + accessToken,
			"Content-type": "application/json; charset=utf-8"
		}
	});

	useEffect(() => {
		axios
			.all([getUserItems, getUserLikes, getUserMatches])
			.then((res) => {
				setPosts(res[0].data.length);
				setLikes(res[1].data.length);
				setMatches(res[2].data.length);
			})

			.catch(() => {
				setError(true);
			});
	}, []);

	const style = {
		background: {
			alignItems: "center",
			padding: "20px",
			backgroundColor: "#f5f5f5",
			height: "calc(100vh - 60px)",
			overflow: "auto"
		},
		box: {
			margin: "30px",
			background: "#FFFFFF",
			border: "1px solid #4CAF50",
			boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.25)",
			borderRadius: "15px",
			width: "600px",
			height: "260px"
		}
	};

	return (
		<>
			<Stack style={style.background}>
				<h1>Tiliasetukset</h1>
				<br />
				{!error && (
					<div>
						Hei <b>{username}</b>! Sinulla on <b>{posts + " "}</b>
						julkaistua ilmoitusta, olet tykännyt{" "}
						<b>{likes + " "}</b> ilmoituksesta ja sait{" "}
						<b>{matches + " "}</b>
						potentiaalista vaihtaria.
					</div>
				)}

				{error && (
					<Alert variant="filled" severity="error">
						Jokin meni pieleen, emmekä valitettavasti saaneet
						haettua tietojasi.
					</Alert>
				)}

				<Box style={style.box}>
					<Stack spacing={3} margin="30px">
						<Stack direction="row" justifyContent="space-between">
							<div>
								Vaihda sähköposti, johon haluat saada tiliisi
								liittyvät ilmoitukset ja uutiskirjeet.
							</div>
							<Button
								sx={{ minWidth: "165px", margin: "5px" }}
								variant="contained"
								color="secondary"
								onClick={() => setShowEmailChangeModal(true)}
							>
								Vaihda email
							</Button>
							{showEmailChangeModal && (
								<ChangeEmail
									closeEmailChangeModal={
										closeEmailChangeModal
									}
								/>
							)}
						</Stack>
						<Stack direction="row" justifyContent="space-between">
							<div>
								Vaihda tilisi salasana. Salasanassa on oltava
								pieniä ja isoja kirjaimia sekä vähintään yksi
								numero. Minimipituus 8 merkkiä.
							</div>
							<Button
								sx={{ minWidth: "165px", margin: "5px" }}
								variant="contained"
								color="secondary"
								onClick={() => setShowPasswordChangeModal(true)}
							>
								Vaihda salasana
							</Button>
							{showPasswordChangeModal && (
								<ChangePassword
									closePasswordChangeModal={
										closePasswordChangeModal
									}
								/>
							)}
						</Stack>
						<Stack direction="row" justifyContent="space-between">
							<div>Poista tilisi kokonaan. </div>
							<Button
								sx={{ minWidth: "165px", margin: "5px" }}
								variant="contained"
								color="error"
								onClick={() => setShowDeleteAccountModal(true)}
							>
								Poista tili
							</Button>
							{showDeleteAccountModal && (
								<DeleteAccount
									closeDeleteAccountModal={
										closeDeleteAccountModal
									}
								/>
							)}
						</Stack>
					</Stack>
				</Box>
			</Stack>
		</>
	);
};

export default UserSettings;
