import { useEffect, useState } from "react";
import axios from "axios";

export const useItemPostSearch = (
	pageNumber,
	searchText,
	newestId,
	oldestId,
	setNewestId,
	setOldestId,
	searchMinValue,
	searchMaxValue,
	searchMinCondition,
	searchMaxCondition,
	searchCategory,
	searchIncludeDescription
) => {
	const [loading, setLoading] = useState(true);
	const [error, setError] = useState(false);
	const [itemPosts, setItemPosts] = useState([]);
	const [hasMore, setHasMore] = useState(true);

	useEffect(() => {
		setLoading(true);
		setError(false);

		axios({
			method: "GET",
			url: "https://localhost:5100/api/Item/query",
			params: {
				newest: newestId,
				oldest: oldestId,
				SearchString: searchText,
				MinValue: searchMinValue,
				MaxValue: searchMaxValue,
				MinCondition: searchMinCondition,
				MaxCondition: searchMaxCondition,
				Category: searchCategory,
				IncludeDescription: searchIncludeDescription
			}
		})
			.then((res) => {
				if (res.data.length === 0) {
					setHasMore(false);

					return null;
				} else {
					setHasMore(true);
				}

				setItemPosts((prevItemPosts) => [
					...new Set([
						...prevItemPosts,
						...res.data.map((itemPost) => itemPost)
					])
				]);

				setLoading(false);

				if (res.data[0].id > newestId) setNewestId(res.data[0].id);
				if (
					res.data[res.data.length - 1].id < oldestId ||
					oldestId === null
				)
					setOldestId(res.data[res.data.length - 1].id);
			})
			.catch(() => {
				setError(true);
			});
		return;
	}, [pageNumber]);

	return { loading, error, itemPosts, hasMore, setItemPosts };
};
