import React, { useState, useRef, useCallback, useContext } from "react";
import { useItemPostSearch } from "./useItemPostSearch";
import { Alert } from "@mui/material";
import { FeedItemCard } from "./FeedItemCard";
import { FeedMessage } from "./FeedMessage";
import { UserContext } from "../../UserContext";
import FeedFilterForm from "../FeedFilterForm/FeedFilterForm";

export const ItemPostFeed = () => {
	const [pageNumber, setPageNumber] = useState(1);
	const [searchText, setSearchText] = useState("");
	const [searchMinValue, setSearchMinValue] = useState(null);
	const [searchMaxValue, setSearchMaxValue] = useState(null);
	const [searchMinCondition, setSearchMinCondition] = useState(null);
	const [searchMaxCondition, setSearchMaxCondition] = useState(null);
	const [searchCategory, setSearchCategory] = useState(null);
	const [searchIncludeDescription, setSearchIncludeDescription] =
		useState(false);

	const [newestId, setNewestId] = useState(null);
	const [oldestId, setOldestId] = useState(null);

	const { itemPosts, setItemPosts, hasMore, loading, error } =
		useItemPostSearch(
			pageNumber,
			searchText,
			newestId,
			oldestId,
			setNewestId,
			setOldestId,
			searchMinValue,
			searchMaxValue,
			searchMinCondition,
			searchMaxCondition,
			searchCategory,

			searchIncludeDescription
		);

	const { openFilters, setOpenFilters } = useContext(UserContext);

	const observer = useRef();
	const lastItemPostElementRef = useCallback(
		(node) => {
			if (loading) return;

			if (observer.current) observer.current.disconnect();
			observer.current = new IntersectionObserver((entries) => {
				if (entries[0].isIntersecting && hasMore)
					setPageNumber((prevPageNumber) => prevPageNumber + 1);
			});
			if (node) observer.current.observe(node);
		},
		[loading, hasMore]
	);

	const clearFeed = () => {
		setNewestId(null);
		setOldestId(null);
		setItemPosts([]);
	};

	return (
		<>
			<div
				style={{
					backgroundColor: "#f5f5f5",
					width: "100vw",
					height: "calc(100vh - 60px)",
					overflow: "auto"
				}}
			>
				{itemPosts.map((itemPost, index) => {
					if (itemPosts.length === index + 1)
						return (
							<div key={itemPost.id} ref={lastItemPostElementRef}>
								<FeedItemCard
									key={itemPost.id}
									itemPost={itemPost}
								/>
							</div>
						);
					else
						return (
							<FeedItemCard
								key={itemPost.id}
								itemPost={itemPost}
							/>
						);
				})}

				{loading && hasMore && (
					<FeedMessage
						spinner={true}
						message={"Ladataan ilmoituksia..."}
					/>
				)}

				<div>
					{error && (
						<Alert variant="filled" severity="error">
							Jokin meni pieleen
						</Alert>
					)}
				</div>
				<div>
					{!hasMore && !error && (
						<FeedMessage
							message={"Hakua vastaavat ilmoitukset loppuivat"}
							spinner={false}
						/>
					)}
				</div>
				<FeedFilterForm
					openFilters={openFilters}
					setOpenFilters={setOpenFilters}
					setSearchText={setSearchText}
					setPageNumber={setPageNumber}
					clearFeed={clearFeed}
					setSearchMaxCondition={setSearchMaxCondition}
					setSearchMinCondition={setSearchMinCondition}
					setSearchMaxValue={setSearchMaxValue}
					setSearchMinValue={setSearchMinValue}
					setSearchCategory={setSearchCategory}
					setSearchIncludeDescription={setSearchIncludeDescription}
					searchIncludeDescription={searchIncludeDescription}
				/>
			</div>
		</>
	);
};
