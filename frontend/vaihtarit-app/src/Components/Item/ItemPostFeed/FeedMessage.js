import React from "react";
import { Card, CircularProgress, Stack } from "@mui/material";

export const FeedMessage = ({ message, spinner }) => (
	<div
		style={{
			display: "flex",
			justifyContent: "center"
		}}
	>
		<Card
			sx={{
				display: "flex",
				width: 500,
				height: 100,
				margin: 2,
				padding: "6em 6em",
				justifyContent: "center",
				alignItems: "center",
				backgroundColor: "#fff",
				color: "#444"
			}}
		>
			<Stack spacing={2} style={{ alignItems: "center" }}>
				{spinner && (
					<div>
						<CircularProgress disableShrink={true} />
					</div>
				)}

				<div>{message}</div>
			</Stack>
		</Card>
	</div>
);
