import { useState, React, useContext, useEffect } from "react";
import ItemLikeButton from "./ItemLikeButton";
import { ItemPostModal } from "../ItemPostModal/ItemPostModal";
import { UserContext } from "../../UserContext";
import {
	CardActionArea,
	CardActions,
	Button,
	Alert,
	Typography,
	CardContent,
	Card,
	CardMedia
} from "@mui/material";
import axios from "axios";

export const FeedItemCard = ({ itemPost }) => {
	const [showItemPostModal, setShowItemPostModal] = useState(false);
	const [buttonError, setButtonError] = useState(false);
	const { isLogged, userId } = useContext(UserContext);
	const [showItemPostWithImage, setShowItemPostWithImage] = useState(false);

	useEffect(() => {
		axios({
			method: "get",
			url: `https://localhost:5100/api/Item/checkIfItemHasImage/${itemPost.id}`
		})
			.then((res) => setShowItemPostWithImage(res.data))
			.catch(() => setShowItemPostWithImage(false));
	}, []);

	return (
		<div
			style={{
				display: "flex",
				justifyContent: "center"
			}}
		>
			<Card sx={{ width: 500, height: 450, margin: 2 }}>
				<CardActionArea onClick={() => setShowItemPostModal(true)}>
					{showItemPostWithImage && (
						<CardMedia
							component="img"
							height="281"
							image={`https://localhost:5100/api/Image/item/${itemPost.id}`}
							alt={itemPost.title}
						/>
					)}
					{!showItemPostWithImage && (
						<CardMedia
							component="img"
							height="281"
							alt={itemPost.title}
						/>
					)}
					<CardContent sx={{ height: 115 }}>
						<Typography gutterBottom variant="h6" component="div">
							{itemPost.title}
						</Typography>

						<div
							style={{
								maxWidth: 466,
								overflowWrap: "break-word"
							}}
						>
							<Typography variant="body2" color="text.secondary">
								{itemPost.description.substring(0, 200).trim()}
								{itemPost.description.length >= 200 && "..."}
							</Typography>
						</div>
					</CardContent>
				</CardActionArea>

				<CardActions sx={{ justifyContent: "space-between" }}>
					<Button
						color="secondary"
						onClick={() => setShowItemPostModal(true)}
					>
						Katso lisää
					</Button>

					{buttonError && isLogged && (
						<Alert
							sx={{ height: 35, alignItems: "center" }}
							severity="error"
						>
							Jokin meni pieleen
						</Alert>
					)}
					{!buttonError && isLogged && itemPost.userId !== userId && (
						<ItemLikeButton
							id={itemPost.id}
							setButtonError={setButtonError}
							isLarge={false}
							showItemPostModal={showItemPostModal}
						/>
					)}
				</CardActions>
			</Card>
			<ItemPostModal
				itemPost={itemPost}
				showItemPostModal={showItemPostModal}
				setShowItemPostModal={setShowItemPostModal}
				showItemPostWithImage={showItemPostWithImage}
			/>
		</div>
	);
};
