import React, { useContext, useEffect, useState } from "react";
import { IconButton, Button, Box } from "@mui/material";
import FavoriteIcon from "@mui/icons-material/Favorite";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import axios from "axios";
import { UserContext } from "../../UserContext";

const ItemLikeButton = ({ id, setButtonError, isLarge, showItemPostModal }) => {
	const { userId, accessToken } = useContext(UserContext);
	const [liked, setLiked] = useState(false);

	useEffect(() => {
		axios({
			method: "GET",
			url: `https://localhost:5100/api/LikedItem/checkIfLiked/${id}`,
			headers: {
				Authorization: "Bearer " + accessToken,
				"Content-type": "application/json; charset=utf-8"
			}
		})
			.then((res) => (res.data ? setLiked(true) : setLiked(false)))
			.catch(() => setButtonError(true));
	}, [showItemPostModal]);

	const handleClick = () => {
		if (!liked)
			axios({
				method: "post",
				url: "https://localhost:5100/api/LikedItem",
				data: {
					userID: userId,
					itemID: id,
					itemOwnerAcknowledge: false
				},
				headers: {
					Authorization: "Bearer " + accessToken,
					"Content-type": "application/JSON; charset=utf-8"
				}
			})
				.then(() => setLiked((prev) => !prev))
				.catch(() => setButtonError(true));
		else
			axios({
				method: "delete",
				url: `https://localhost:5100/api/LikedItem/${id}`,
				headers: {
					Authorization: "Bearer " + accessToken,
					"Content-type": "application/JSON; charset=utf-8"
				}
			})
				.then(() => setLiked((prev) => !prev))
				.catch(() => setButtonError(true));
	};

	return (
		<div>
			{isLarge && (
				<Box onClick={handleClick}>
					{liked ? (
						<Button
							endIcon={<FavoriteIcon color="primary" />}
							variant="outlined"
							style={{
								boxShadow: "inset 1px 2px 2px",
								width: "115px",
								transform: "translate(1px, 2px)"
							}}
						>
							Tykätty
						</Button>
					) : (
						<Button
							endIcon={<FavoriteBorderIcon color="primary" />}
							variant="outlined"
							style={{
								boxShadow: "1px 2px 2px",
								width: "115px"
							}}
						>
							Tykkää
						</Button>
					)}
				</Box>
			)}

			{!isLarge && (
				<IconButton
					aria-label="Lähetä/poista tykkäys"
					onClick={handleClick}
				>
					{liked ? (
						<FavoriteIcon color="primary" />
					) : (
						<FavoriteBorderIcon color="primary" />
					)}
				</IconButton>
			)}
		</div>
	);
};

export default ItemLikeButton;
