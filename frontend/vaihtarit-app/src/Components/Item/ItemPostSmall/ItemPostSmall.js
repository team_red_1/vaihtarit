/* eslint-disable indent */
import React from "react";
import { Button, CardMedia, Stack, Tooltip, Typography } from "@mui/material";
import HeartBrokenIcon from "@mui/icons-material/HeartBroken";
import ItemPost from "../ItemPostForm/ItemPostForm";
import ItemLikeButton from "../ItemPostFeed/ItemLikeButton";
import { ItemPostModal } from "../ItemPostModal/ItemPostModal";
import { useEffect, useState } from "react";
import axios from "axios";

export const ItemPostSmall = ({
	id,
	title,
	description,
	deleteItemPost,
	deleteItemLike,
	deleteButton,
	itemLikeButton,
	itemPost,
	mini
}) => {
	const style = {
		background: {
			width: mini ? "95%" : "700px",
			height: mini ? "70px" : "100px",
			background: "#FFFFFF",
			border: "1px solid #4CAF50",
			boxSizing: "border-box",
			boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.25)",
			borderRadius: "15px",
			display: "flex",
			margin: mini ? "5px" : ""
		},
		image: {
			width: mini ? "124.44px" : "174.22px",
			height: "100%",
			borderRadius: "15px",
			cursor: "pointer"
		},
		textArea: {
			width: "100%",
			padding: mini ? "10px" : "20px 30px",
			display: mini ? "flex" : "",
			alignItems: "center",
			justifyContent: "center",
			cursor: "pointer"
		},
		buttonArea: {
			display: "flex",
			justifyContent: "flex-end",
			alignItems: "center",
			marginRight: "5%"
		}
	};

	const [showItemPostModal, setShowItemPostModal] = useState(false);
	const [showItemPostWithImage, setShowItemPostWithImage] = useState(false);

	useEffect(() => {
		axios({
			method: "get",
			url: `https://localhost:5100/api/Item/checkIfItemHasImage/${itemPost.id}`
		})
			.then((res) => setShowItemPostWithImage(res.data))
			.catch(() => setShowItemPostWithImage(false));
	}, []);

	return (
		<div style={style.background}>
			{showItemPostWithImage && (
				<CardMedia
					sx={style.image}
					component="img"
					image={`https://localhost:5100/api/Image/item/${id}`}
					alt={title}
					onClick={() => setShowItemPostModal(true)}
				/>
			)}
			{!showItemPostWithImage && (
				<CardMedia sx={style.image} component="img" alt={title} />
			)}

			<div
				style={style.textArea}
				onClick={() => setShowItemPostModal(true)}
			>
				<Stack spacing={1}>
					<Typography
						variant={"h5"}
						color={"text.primary"}
						component="div"
						sx={{ fontSize: mini ? "1em" : "h5" }}
					>
						{title}
					</Typography>
					<Typography component="div" variant="p">
						{description && description.substring(0, 40).trim()}
						{description && description.length >= 40 && "..."}
					</Typography>
				</Stack>
			</div>
			<div style={style.buttonArea}>
				{deleteButton === true &&
					window.location.pathname === "/myposts" && (
						<Button
							variant="contained"
							color="error"
							onClick={() => deleteItemPost(id)}
						>
							Poista
						</Button>
					)}
				{deleteButton === true &&
					window.location.pathname === "/mylikes" && (
						<Tooltip title="Poista tykkäys" arrow>
							<Button
								sx={{
									"&:hover": {
										color: "red",
										backgroundColor: "white"
									}
								}}
								id={ItemPost.title}
								variant="contained"
								color="error"
								onClick={() => deleteItemLike(id)}
							>
								<HeartBrokenIcon />
							</Button>
						</Tooltip>
					)}

				{itemLikeButton === true && (
					<ItemLikeButton id={id} isLarge={true} />
				)}
			</div>
			<ItemPostModal
				itemPost={itemPost}
				showItemPostModal={showItemPostModal}
				setShowItemPostModal={setShowItemPostModal}
				showItemPostWithImage={showItemPostWithImage}
			/>
		</div>
	);
};
