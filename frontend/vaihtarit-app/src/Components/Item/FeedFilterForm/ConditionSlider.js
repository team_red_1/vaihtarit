import React from "react";
import { Box } from "@mui/system";
import { Slider } from "@mui/material";
import { useState } from "react";

export const ConditionSlider = ({
	setSearchMinCondition,
	setSearchMaxCondition
}) => {
	const [conditionValue, setConditionValue] = useState([0, 100]);

	const handleConditionChange = (_, newConditionValue) => {
		setConditionValue(newConditionValue);
		// eslint & prettier clashing here, so this is quick fix
		/* eslint-disable indent */
		switch (newConditionValue[0]) {
			case 0:
				setSearchMinCondition(3);
				break;
			case 33:
				setSearchMinCondition(2);
				break;
			case 66:
				setSearchMinCondition(1);
				break;
			case 100:
				setSearchMinCondition(0);
				break;
			default:
		}
		switch (newConditionValue[1]) {
			case 0:
				setSearchMaxCondition(3);
				break;
			case 33:
				setSearchMaxCondition(2);
				break;
			case 66:
				setSearchMaxCondition(1);
				break;
			case 100:
				setSearchMaxCondition(0);
				break;
			default:
		}
	};
	/* eslint-enable indent */

	const conditionMarks = [
		{
			value: 0,
			label: "Varaosiksi"
		},
		{
			value: 33,
			label: "Kulunut"
		},
		{
			value: 66,
			label: "Vähän käytetty"
		},
		{
			value: 100,
			label: "Uusi"
		}
	];
	return (
		<>
			<h4>Kunto</h4>
			<Box sx={{ width: 425, marginLeft: "35px" }}>
				<Slider
					value={conditionValue}
					onChange={handleConditionChange}
					marks={conditionMarks}
					step={null}
				/>
			</Box>
		</>
	);
};
