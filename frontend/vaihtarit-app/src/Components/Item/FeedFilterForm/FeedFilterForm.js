import React, { useContext, useState, useEffect } from "react";
import axios from "axios";
import {
	Alert,
	Popover,
	Box,
	TextField,
	Switch,
	FormControlLabel,
	Button
} from "@mui/material";
import { ItemPropertyMenu } from "../ItemPostForm/ItemPropertyMenu";
import { PriceSlider } from "./PriceSlider";
import { ConditionSlider } from "./ConditionSlider";
import { UserContext } from "../../UserContext";

const FeedFilterForm = ({
	openFilters,
	setOpenFilters,
	setSearchText,
	setPageNumber,
	clearFeed,
	setSearchMinValue,
	setSearchMaxValue,
	setSearchMinCondition,
	setSearchMaxCondition,
	setSearchIncludeDescription,
	searchIncludeDescription,
	setSearchCategory
}) => {
	const [searchTextInput, setSearchTextInput] = useState("");
	const [categoryChoice, setCategoryChoice] = useState([]);
	const [filterOptions, setFilterOptions] = useState([]);
	const [error, setError] = useState(false);
	const { filterAnchorEl } = useContext(UserContext);

	const handleCategoryChange = (_, newCategoryChoice) => {
		setCategoryChoice(newCategoryChoice);
	};

	const handleIncludeDescriptionChange = () => {
		setSearchIncludeDescription((prev) => !prev);
	};

	const popoverStyle = {
		width: "555px",
		height: "460px",
		padding: "30px",
		bgcolor: "secondary.light"
	};

	const getFilterChoices = () => {
		axios({
			method: "get",
			url: "https://localhost:5100/api/Item/create-item-data"
		})
			.then((res) => {
				setFilterOptions(res.data);
			})
			.catch(() => setError(true));
	};

	const applyFiltering = (e) => {
		e.preventDefault();
		clearFeed();
		setSearchText(searchTextInput);
		setPageNumber((prevPageNumber) => prevPageNumber + 1);
		setOpenFilters(false);
		setSearchTextInput("");
		setSearchCategory(categoryChoice);
	};

	useEffect(() => getFilterChoices(), []);

	useEffect(() => {
		if (openFilters === true) {
			setSearchTextInput("");
			setSearchText("");
			setSearchIncludeDescription(false);
			setSearchMinValue(null);
			setSearchMaxValue(null);
			setSearchMinCondition(null);
			setSearchMaxCondition(null);
			setSearchCategory(null);
		}
	}, [openFilters]);

	return (
		<>
			<Popover
				open={openFilters}
				anchorEl={filterAnchorEl}
				onClose={() => setOpenFilters(false)}
				anchorOrigin={{
					vertical: "top",
					horizontal: "right"
				}}
				sx={{ top: "40px", left: "-150px" }}
			>
				{!error && (
					<Box sx={popoverStyle}>
						<form onSubmit={applyFiltering}>
							<h2>Suodata ilmoitukset</h2>
							<br />
							<TextField
								onChange={(e) =>
									setSearchTextInput(e.target.value)
								}
								fullWidth={true}
								id="filled-basic"
								variant="filled"
								label={"Vapaasanahaku"}
								sx={{ backgroundColor: "white" }}
								value={searchTextInput}
							/>
							<FormControlLabel
								control={<Switch color="primary" />}
								label="Hae myös kuvauksista"
								onChange={handleIncludeDescriptionChange}
								value={searchIncludeDescription}
							/>
							<br />
							<br />
							<PriceSlider
								setSearchMinValue={setSearchMinValue}
								setSearchMaxValue={setSearchMaxValue}
							/>
							<br />
							<ConditionSlider
								setSearchMinCondition={setSearchMinCondition}
								setSearchMaxCondition={setSearchMaxCondition}
							/>
							<br />
							<Box display="flex" justifyContent="space-between">
								<Box>
									<ItemPropertyMenu
										value={categoryChoice}
										menuItems={filterOptions.categories}
										menuName={"Kategoria"}
										onChange={handleCategoryChange}
										setItemPropertyForPost={
											setCategoryChoice
										}
									/>
								</Box>
								<Button
									type={"submit"}
									variant="contained"
									onClick={applyFiltering}
								>
									Tallenna
								</Button>
							</Box>
						</form>
					</Box>
				)}

				{error && (
					<Alert
						sx={{
							marginTop: "10px",
							marginBottom: "10px",
							height: 35,
							alignItems: "center"
						}}
						severity="error"
					>
						Jokin meni pieleen
					</Alert>
				)}
			</Popover>
		</>
	);
};

export default FeedFilterForm;
