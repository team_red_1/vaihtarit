import React from "react";
import { Box } from "@mui/system";
import { Slider } from "@mui/material";
import { useState } from "react";

export const PriceSlider = ({ setSearchMinValue, setSearchMaxValue }) => {
	const [priceValue, setPriceValue] = useState([0, 100]);
	const priceMarks = [
		{
			value: 0,
			label: "0-20"
		},
		{
			value: 14,
			label: "21-50"
		},
		{
			value: 28,
			label: "51-100"
		},
		{
			value: 45,
			label: "101-200"
		},
		{
			value: 62,
			label: "201-500"
		},
		{
			value: 82,
			label: "501-1000"
		},
		{
			value: 100,
			label: "1000 - "
		}
	];

	const handlePriceChange = (e, newPriceValue) => {
		setPriceValue(newPriceValue);
		// eslint & prettier clashing here, so this is quick fix
		/* eslint-disable indent */
		switch (newPriceValue[0]) {
			case 0:
				setSearchMinValue(0);
				break;
			case 14:
				setSearchMinValue(1);
				break;
			case 28:
				setSearchMinValue(2);
				break;
			case 45:
				setSearchMinValue(3);
				break;
			case 62:
				setSearchMinValue(4);
				break;
			case 82:
				setSearchMinValue(5);
				break;
			case 100:
				setSearchMinValue(6);
				break;

			default:
		}

		switch (newPriceValue[1]) {
			case 0:
				setSearchMaxValue(0);
				break;
			case 14:
				setSearchMaxValue(1);
				break;
			case 28:
				setSearchMaxValue(2);
				break;
			case 45:
				setSearchMaxValue(3);
				break;
			case 62:
				setSearchMaxValue(4);
				break;
			case 82:
				setSearchMaxValue(5);
				break;
			case 100:
				setSearchMaxValue(6);
				break;

			default:
		} /* eslint-enable indent */
	};

	return (
		<>
			<h4>Hinta-arvio (€)</h4>
			<Box sx={{ width: 425, marginLeft: "35px" }}>
				<Slider
					value={priceValue}
					onChange={handlePriceChange}
					marks={priceMarks}
					step={null}
				/>
			</Box>
		</>
	);
};
