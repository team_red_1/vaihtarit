import React from "react";
import { Button, Menu, MenuItem } from "@mui/material";
import { useState } from "react";

export const ItemPropertyMenu = ({
	setItemPropertyForPost,
	ariaLabel,
	ariaControls,
	menuId,
	menuName,
	menuItems
}) => {
	const [anchorEl, setAnchorEl] = useState(null);
	const [itemProperty, setItemProperty] = useState(menuName);

	const handleClick = (event) => {
		setAnchorEl(event.currentTarget);
	};

	const selectItemProperty = (selectedItemProperty) => {
		setItemProperty(selectedItemProperty.title);
		setItemPropertyForPost(selectedItemProperty.id);
		setAnchorEl(null);
	};

	return (
		<>
			<Button
				sx={{
					color: "#000",
					backgroundColor: "#e5e5e5",
					fontSize: "12px"
				}}
				variant="contained"
				size="large"
				aria-label={ariaLabel}
				aria-controls={ariaControls}
				aria-haspopup="true"
				onClick={handleClick}
			>
				{itemProperty}
			</Button>
			<Menu
				id={menuId}
				anchorEl={anchorEl}
				keepMounted
				open={Boolean(anchorEl)}
				onClose={() => setAnchorEl(null)}
				anchorOrigin={{
					vertical: "top",
					horizontal: "center"
				}}
				transformOrigin={{
					vertical: "top",
					horizontal: "center"
				}}
			>
				{menuItems.map((e) => (
					<MenuItem key={e.id} onClick={() => selectItemProperty(e)}>
						{e.title}
					</MenuItem>
				))}
			</Menu>
		</>
	);
};
