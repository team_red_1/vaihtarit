import {
	Alert,
	Button,
	Modal,
	Stack,
	TextField,
	IconButton,
	Input,
	Tooltip
} from "@mui/material";
import { Box } from "@mui/system";
import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { UserContext } from "../../UserContext";
import { ItemPropertyMenu } from "./ItemPropertyMenu";
import CancelIcon from "@mui/icons-material/Cancel";
import PhotoCamera from "@mui/icons-material/PhotoCamera";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import { MySnackbarContext } from "../../Snackbar/SnackbarContext";

const ItemPost = ({ setShowItemPostModal, showItemPostModal }) => {
	const [itemName, setItemName] = useState("");
	const [itemDesc, setItemDesc] = useState("");
	const [itemCategory, setItemCategory] = useState("");
	const [itemCondition, setItemCondition] = useState("");
	const [itemRoughValue, setItemRoughValue] = useState("");
	// const [itemLocation, setItemLocation] = useState(""); <- TODO
	const [errorText, setErrorText] = useState("");
	const [menuItems, setMenuItems] = useState([]);
	const [image, setImage] = useState([]);
	const [imageURL, setImageURL] = useState([]);
	const [imageData, setImageData] = useState([]);

	const { accessToken } = useContext(UserContext);
	const { openSnackbar } = useContext(MySnackbarContext);
	const navigate = useNavigate();
	const formData = new FormData();

	const modalStyle = {
		position: "absolute",
		top: "50%",
		left: "50%",
		transform: "translate(-50%, -50%)",
		width: "40vw",
		bgcolor: "secondary.main",
		boxShadow: 5,
		p: 4
	};

	const itemPosted = () => {
		openSnackbar("Uusi ilmoitus lisätty onnistuneesti!");
		setImage([]);
		setImageURL([]);
		setImageData([]);
		setItemName("");
		setItemDesc("");
		navigate("/");
	};

	const itemPostFailed = (error) => {
		// ToDo: paremmat virheviestit
		if (error.response) {
			if (error.response.status === 400)
				setErrorText(
					"Ilmoituksen luonti epäonnistui! Yritä uudelleen."
				);
		} else if (error.response.status === 401) {
			setErrorText(
				"Varmista että olet kirjautunut sisään ja yritä sitten uudestaan."
			);
		} else {
			setErrorText("Jokin meni pieleen");
		}
	};

	const handleItemPost = (e) => {
		e.preventDefault();

		if (itemName.length === 0) {
			setErrorText("Otsikko ei voi olla tyhjä!");
			return;
		} else if (itemName.length < 4) {
			setErrorText("Otsikko on oltava vähintään 4 merkkiä pitkä.");
			return;
		} else if (
			itemCategory === "" ||
			itemCondition === "" ||
			itemRoughValue === ""
			// itemLocation === "" <- TODO
		) {
			setErrorText("Valitse kaikki ominaisuudet vaihtotavaralle");
			return;
		} else if (imageData.length === 0) {
			setErrorText("Liitä kuva ilmoitukseesi!");
			return;
		}

		axios({
			method: "post",
			url: "https://localhost:5100/api/Item/",
			headers: {
				Authorization: "Bearer " + accessToken
			},
			data: {
				title: itemName,
				description: itemDesc,
				category: itemCategory,
				condition: itemCondition,
				value: itemRoughValue,
				images: imageData
				//	location: itemLocation <- todo
			}
		})
			.then((res) => {
				if (res.status === 200) itemPosted(res);
			})
			.catch((error) => itemPostFailed(error))
			.finally(closeItemPostForm);
	};

	const closeItemPostForm = () => {
		setShowItemPostModal(false);
		setErrorText("");
	};

	const getMenuItems = () => {
		axios({
			method: "get",
			url: "https://localhost:5100/api/Item/create-item-data"
		})
			.then((res) => {
				setMenuItems(res.data);
			})
			.catch((error) => itemPostFailed(error));
	};

	useEffect(() => {
		if (image.length < 1) return;

		const newImageUrl = [];

		image.forEach((image) => newImageUrl.push(URL.createObjectURL(image)));

		setImageURL(newImageUrl);
		setErrorText("");
	}, [image]);

	const clearImageData = () => {
		axios({
			method: "DELETE",
			url: "https://localhost:5100/api/Image/tmp",
			data: { guid: imageData }
		})
			.then(() => {
				setImage([]);
				setImageURL([]);
				setImageData([]);
			})
			.catch((error) => itemPostFailed(error));
	};

	const imageHandler = (e) => {
		setImage([...e.target.files]);

		formData.append("file", e.target.files[0]);

		axios({
			method: "POST",
			url: "https://localhost:5100/api/Image/tmp/",
			data: formData,
			headers: {
				"Content-Type": "multipart/form-data"
			}
		})
			.then((res) => {
				setImageData(res.data);
			})
			.catch((error) => itemPostFailed(error));
	};

	useEffect(() => getMenuItems(), []);

	return (
		<Modal open={showItemPostModal} onClose={closeItemPostForm}>
			<Box sx={modalStyle}>
				<IconButton
					onClick={() => closeItemPostForm()}
					tabIndex={-1}
					style={{
						position: "absolute",
						right: 0,
						top: 0
					}}
				>
					<CancelIcon />
				</IconButton>
				<form onSubmit={handleItemPost}>
					<h2>Lisää uusi vaihtotavara</h2>
					{/* ToDo: ifelse title switch if new item post or existing item edit */}
					<br />
					<TextField
						value={itemName}
						onChange={(e) => setItemName(e.target.value)}
						fullWidth={true}
						id="filled-basic"
						variant="filled"
						label="Otsikko"
						inputProps={{ maxLength: 30 }}
						// ToDo: korjaa muotoilu niin, ettei helperText kentän ilmestyminen työnnä alla olevia elementtejä alemmaksi.
						helperText={
							itemName.length < 30
								? ""
								: "Otsikko ei voi olla pidempi."
						}
						sx={{
							backgroundColor: "white"
						}}
					/>
					<br />
					<br />
					<TextField
						value={itemDesc}
						onChange={(e) => setItemDesc(e.target.value)}
						fullWidth={true}
						multiline
						rows={4}
						id="filled-basic"
						label="Kuvaus"
						variant="filled"
						sx={{ backgroundColor: "white" }}
						inputProps={{ maxLength: 500 }}
						helperText={
							itemDesc.length < 500
								? ""
								: "Kuvaus ei voi olla pidempi."
						}
					/>
					<br />
					<br />
					<Stack direction="row" spacing={2}>
						<ItemPropertyMenu
							setItemPropertyForPost={setItemCategory}
							ariaLabel={"category of item"}
							ariaControls={"menu-category"}
							menuId={"menu-category"}
							menuItems={menuItems.categories}
							menuName={"Kategoria +"}
						/>
						<ItemPropertyMenu
							setItemPropertyForPost={setItemRoughValue}
							ariaLabel={"rough value of item"}
							ariaControls={"menu-roughValue"}
							menuId={"menu-roughValue"}
							menuItems={menuItems.roughValues}
							menuName={"Arvo Suurinpiirtein +"}
						/>
						<ItemPropertyMenu
							setItemPropertyForPost={setItemCondition}
							ariaLabel={"condition of item"}
							ariaControls={"menu-condition"}
							menuId={"menu-condition"}
							menuItems={menuItems.conditions}
							menuName={"Kunto +"}
						/>
					</Stack>
					<br />

					{errorText !== "" && (
						<Alert variant="filled" severity="error">
							{errorText}
						</Alert>
					)}

					<Stack direction="row">
						{image.length === 0 && (
							<label htmlFor="contained-button-file">
								<Input
									type="file"
									single="true"
									id="contained-button-file"
									accept="image/*"
									sx={{ display: "none" }}
									onChange={imageHandler}
								/>
								<Button
									variant="outlined"
									sx={{ color: "black" }}
									component="span"
									endIcon={<PhotoCamera />}
								>
									Liitä kuva
								</Button>
							</label>
						)}
						{imageURL.map((imageSrc) => (
							<img
								src={imageSrc}
								key={imageSrc.toString()}
								style={{ width: "15%", height: "15%" }}
							/>
						))}
						{image.length > 0 && (
							<Tooltip
								title="Poista kuva"
								arrow
								placement="right"
							>
								<Button
									sx={{
										color: "gray",
										"&:hover": { color: "red" }
									}}
									endIcon={<DeleteForeverIcon />}
									onClick={() => clearImageData()}
								/>
							</Tooltip>
						)}
					</Stack>
					<br />
					<br />

					<Stack direction="row" spacing={2}>
						<Button
							type={"submit"}
							sx={{
								color: "primary.buttonText"
							}}
							variant="contained"
						>
							Tallenna
						</Button>
						<Button
							sx={{
								color: "primary.buttonText",
								"&:hover": { color: "red" }
							}}
							onClick={() => closeItemPostForm()}
						>
							Sulje
						</Button>
					</Stack>
				</form>
			</Box>
		</Modal>
	);
};

export default ItemPost;
