import React, { useContext } from "react";
import {
	Box,
	CardMedia,
	Grid,
	IconButton,
	Modal,
	Stack,
	Typography
} from "@mui/material";
import CancelIcon from "@mui/icons-material/Cancel";
import ArrowDownwardIcon from "@mui/icons-material/ArrowDownward";
import ItemLikeButton from "../ItemPostFeed/ItemLikeButton";
import { UserContext } from "../../UserContext";

export const ItemPostModal = ({
	setShowItemPostModal,
	showItemPostModal,
	itemPost,
	setButtonError,
	showItemPostWithImage
}) => {
	const { isLogged, userId } = useContext(UserContext);

	const modalStyle = {
		window: {
			position: "absolute",
			width: "1024px",
			height: "550px",
			background: "#FFFFFF",
			boxShadow: 5
		},
		header: {
			width: "1024px",
			height: "60px",
			background: "#C8E6C9",
			boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
			paddingTop: "6px",
			paddingLeft: "40px"
		},
		image: {
			width: "400px",
			height: "225px",
			borderRadius: "15px",
			marginTop: "28px",
			marginLeft: "40px",
			marginRight: "14px"
		},
		info: {
			width: "520px",
			height: "173px",
			background: "#E5E5E5",
			marginTop: "28px",
			marginLeft: "14px",
			marginRight: "28px",
			borderRadius: "15px",
			boxShadow: "0px 4px 5px rgba(0, 0, 0, 0.25)",
			padding: "20px 5px 20px 28px"
		},
		description: {
			width: "949px",
			height: "180px",
			background: "#E5E5E5",
			borderRadius: "15px",
			marginLeft: "40px",
			marginTop: "28px",
			boxShadow: "0px 4px 5px rgba(0, 0, 0, 0.25)",
			padding: "28px"
		}
	};

	return (
		<Modal
			sx={{
				display: "flex",
				justifyContent: "center",
				alignItems: "center"
			}}
			open={showItemPostModal}
			onClose={() => setShowItemPostModal(false)}
		>
			<Box sx={modalStyle.window}>
				<div style={modalStyle.header}>
					<Typography variant="h5" component="div">
						{itemPost.title}
					</Typography>
					<Typography
						variant="caption"
						color={"text.secondary"}
						component="div"
					>
						Sijainti
					</Typography>
					<IconButton
						onClick={() => setShowItemPostModal(false)}
						style={{
							position: "absolute",
							right: 0,
							top: 0
						}}
					>
						<CancelIcon />
					</IconButton>
				</div>

				<div style={{ display: "flex" }}>
					<div>
						{showItemPostWithImage && (
							<CardMedia
								component="img"
								image={`https://localhost:5100/api/Image/item/${itemPost.id}`}
								alt={itemPost.title}
								style={modalStyle.image}
							/>
						)}
						{!showItemPostWithImage && (
							<CardMedia
								component="img"
								alt={itemPost.title}
								style={modalStyle.image}
							/>
						)}
					</div>
					<div style={modalStyle.info}>
						<Grid container spacing={1}>
							<Grid item xs={9}>
								<Stack>
									<Typography
										variant="caption"
										color={"text.secondary"}
									>
										Kategoria
									</Typography>
									<div>{itemPost.category}</div>
								</Stack>
							</Grid>
							<Grid color={"text.secondary"} item xs={3}>
								Ilmianna
							</Grid>

							<Grid item xs={12}>
								<Stack>
									<Typography
										variant="caption"
										color={"text.secondary"}
									>
										Kunto
									</Typography>
									<div>{itemPost.condition}</div>
								</Stack>
							</Grid>
							<Grid item xs={12}>
								<Stack>
									<Typography
										variant="caption"
										color={"text.secondary"}
									>
										Arvo
									</Typography>
									<div>{itemPost.value}</div>
								</Stack>
							</Grid>
						</Grid>
						<div
							style={{
								paddingTop: "60px",
								paddingLeft: "375px"
							}}
						>
							{isLogged && itemPost.userId !== userId && (
								<ItemLikeButton
									id={itemPost.id}
									setButtonError={setButtonError}
									isLarge={true}
								/>
							)}
						</div>
					</div>
				</div>
				<div style={modalStyle.description}>
					<div style={{ overflow: "auto", height: "170px" }}>
						{itemPost.description}
					</div>

					{itemPost.description.length > 1100 && (
						<ArrowDownwardIcon
							sx={{
								color: "text.secondary",
								position: "absolute",
								left: 960,
								top: 590
							}}
						/>
					)}
				</div>
			</Box>
		</Modal>
	);
};
