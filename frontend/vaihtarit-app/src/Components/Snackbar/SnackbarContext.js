import React, { createContext, useState } from "react";

export const MySnackbarContext = createContext();

export const SnackbarContext = ({ children }) => {
	const [snackbarMessage, setSnackbarMessage] = useState("");
	const [displaySnackbar, setDisplaySnackbar] = useState(false);

	const openSnackbar = (message) => {
		setSnackbarMessage(message);
		setDisplaySnackbar(true);
	};

	return (
		<MySnackbarContext.Provider
			value={{
				snackbarMessage,
				setSnackbarMessage,
				openSnackbar,
				displaySnackbar,
				setDisplaySnackbar
			}}
		>
			{children}
		</MySnackbarContext.Provider>
	);
};
