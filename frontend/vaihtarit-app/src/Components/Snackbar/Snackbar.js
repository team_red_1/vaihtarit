import React, { useContext } from "react";
import { MySnackbarContext } from "./SnackbarContext";
import { Snackbar } from "@mui/material";

const SnackbarProvider = () => {
	const { snackbarMessage, displaySnackbar, setDisplaySnackbar } =
		useContext(MySnackbarContext);

	return (
		<Snackbar
			open={displaySnackbar}
			onClose={() => {
				setDisplaySnackbar(false);
			}}
			message={snackbarMessage}
			autoHideDuration={5000}
		/>
	);
};

export default SnackbarProvider;
