import { createTheme } from "@mui/material";

const theme = createTheme({
	palette: {
		primary: {
			main: "#4caf50",
			buttonText: "#fff"
		},
		secondary: {
			main: "#81c784",
			buttonText: "#2e7d31"
		}
	}
});

export default theme;
