import { Button, TextField } from "@mui/material";
import MessageOutlinedIcon from "@mui/icons-material/MessageOutlined";
import { Box } from "@mui/system";
import React, { useContext, useEffect, useState } from "react";
import axios from "axios";
import { UserContext } from "../UserContext";
import { ChatBubble } from "./ChatBubble";
import { MySnackbarContext } from "../Snackbar/SnackbarContext";

export const ChatForm = ({ matchId, matchedWith }) => {
	const { userId, accessToken } = useContext(UserContext);
	const { openSnackbar } = useContext(MySnackbarContext);
	const [chatMessage, setChatMessage] = useState("");
	const [messageLog, setMessageLog] = useState([]);
	const [updateTrigger, setUpdateTrigger] = useState(false);
	const [previousMatchId, setPreviousMatchId] = useState("");

	const errorHandle = (error) => {
		if (error.response.status === 404) {
			console.clear(); // Todo: better method to deal with 404 error messages when match doesn't have any messages yet than this dirty way
			openSnackbar("Ei viestejä");
			setMessageLog([
				{
					messageText:
						"Tässä keskustelussa ei ole vielä yhtään viestiä. Olehan rohkea ja aloita!",
					receiver: "t.pääsiäispupu",
					id: "pupu"
				}
			]);
		} else {
			openSnackbar("Jokin meni pieleen");
		}
	};

	const getMessages = () => {
		axios({
			method: "GET",
			url: "https://localhost:5100/api/Message/matchMessages/",
			params: {
				UserId: userId,
				MatchId: matchId
			},
			headers: {
				Authorization: "Bearer " + accessToken,
				"Content-Type": "application/json"
			}
		})
			.then((res) => {
				setMessageLog(res.data);
				setPreviousMatchId(matchId);
				setUpdateTrigger(true);
			})
			.catch((error) => {
				errorHandle(error);
			});
	};

	useEffect(() => {
		if (messageLog.length === 0) getMessages();
		if (matchId !== previousMatchId) getMessages();
		if (messageLog.receiver === "t.pääsiäispupu") interval();

		const interval = setInterval(() => {
			getMessages();
			setUpdateTrigger(false);
		}, 1000);

		return () => {
			clearInterval(interval);
		};
	}, [matchId, updateTrigger]);

	const handleSubmit = () => {
		axios({
			method: "POST",
			url: "https://localhost:5100/api/Message",
			params: {
				userId,
				matchId,
				messageText: chatMessage
			},
			headers: {
				Authorization: "Bearer " + accessToken,
				"Content-Type": "application/json"
			}
		})
			.then(() => {
				setChatMessage("");
				setUpdateTrigger(true);
			})
			.catch((error) => {
				errorHandle(error);
			});
	};

	const checkMessageLog = (message) => {
		if (message.receiver === "t.pääsiäispupu") return "t.pääsiäispupu";
		else if (message.senderId !== userId) return matchedWith;
		else return "Sinä";
	};

	return (
		<>
			<Box
				sx={{
					width: "auto",
					height: "calc(100vh - 12rem)",
					borderBottom: "groove 1px gray",
					marginBottom: "1.5vh",
					overflow: "auto",
					"&::-webkit-scrollbar": {
						width: "2px"
					},
					"&::-webkit-scrollbar-thumb": {
						backgroundColor: "#17B169"
					}
				}}
			>
				{messageLog.map((message) => {
					const checkId = checkMessageLog(message);

					const checkIfUser =
						message.receiverId === userId ? true : false;

					return (
						<ChatBubble
							message={message.messageText}
							receiver={checkId}
							checkIfUser={checkIfUser}
							stamp={""} // ToDo/wishlist: get message creation timestamp from endpoint
							key={message.id}
						/>
					);
				})}
			</Box>

			<Box sx={{ display: "flex", flexDirection: "row" }}>
				<TextField
					label="Kirjoita viestisi tähän"
					rows={2}
					multiline
					fullWidth
					value={chatMessage}
					onChange={(e) => setChatMessage(e.target.value)}
				/>

				<Button
					variant="contained"
					endIcon={<MessageOutlinedIcon />}
					onClick={handleSubmit}
				>
					Lähetä
				</Button>
			</Box>
		</>
	);
};
