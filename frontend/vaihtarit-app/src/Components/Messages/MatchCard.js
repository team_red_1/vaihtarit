import React, { useState } from "react";
import axios from "axios";
import { Button, Alert } from "@mui/material";

const MatchCard = ({
	setTheirItems,
	activeLikerUserId,
	activeLikerUsername,
	userId,
	targetOfLikeUsername,
	targetOfLikeUserId,
	passMatchData,
	matchData,
	passUsername
}) => {
	const [error, setError] = useState(false);

	const card = {
		height: "40px",
		width: "95%",
		padding: "10px",
		alignItems: "center",
		justifyContent: "start",
		border: "1px solid #4CAF50",
		boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.25)",
		borderRadius: "15px",
		margin: "5px 5px"
	};

	const matchedWith =
		activeLikerUserId === userId
			? targetOfLikeUsername
			: activeLikerUsername;

	const theirUserId =
		activeLikerUserId === userId ? targetOfLikeUserId : activeLikerUserId;

	const getMatchItems = () => {
		passUsername(matchedWith);
		passMatchData(matchData);
		getTheirItems();
	};

	const getTheirItems = () => {
		axios({
			method: "GET",
			url: `https://localhost:5100/api/Item/user-items/${theirUserId}`
		})
			.then((res) => {
				setTheirItems(res.data);
			})
			.catch(() => setError(true));
	};

	return (
		<div>
			{error && (
				<Alert variant="filled" severity="error">
					Jokin meni pieleen
				</Alert>
			)}
			<Button sx={card} onClick={() => getMatchItems()}>
				<h3>{matchedWith}</h3>
			</Button>
		</div>
	);
};
export default MatchCard;
