import React, { useEffect, useContext, useState } from "react";
import { Grid, Stack, Box, Alert } from "@mui/material";
import { UserContext } from "../UserContext";
import axios from "axios";
import MatchCard from "./MatchCard";
import { ItemPostSmall } from "../Item/ItemPostSmall/ItemPostSmall";
import { ChatForm } from "./ChatForm";

const ChatContainer = () => {
	const { accessToken, userId } = useContext(UserContext);
	const [matches, setMatches] = useState([]);
	const [noMatches, setNoMatches] = useState();
	const [error, setError] = useState(false);
	const [theirItems, setTheirItems] = useState(false);
	const [myItems, setMyItems] = useState(false);
	const [matchData, setMatchData] = useState([]);
	const [showChat, setShowChat] = useState(false);
	const [matchUsername, setMatchUsername] = useState("");

	const getMyItems = () => {
		axios({
			method: "GET",
			url: `https://localhost:5100/api/Item/user-items/${userId}`
		})
			.then((res) => {
				setMyItems(res.data);
			})
			.catch(() => setError(true));
	};

	useEffect(() => {
		axios({
			method: "GET",
			url: `https://localhost:5100/api/LikedItem/getMatchesOfUser/${userId}`,
			headers: {
				Authorization: "Bearer " + accessToken,
				"Content-type": "application/json; charset=utf-8"
			}
		})
			.then((res) => {
				if (res.data === "No matches found.") setNoMatches(true);
				else setMatches(res.data);
				getMyItems();
			})
			.catch(() => setError(true));
	}, []);

	const passMatchData = (data) => {
		setMatchData(data);
		setShowChat(true);
	};

	const passUsername = (username) => {
		setMatchUsername(username);
	};

	return (
		<>
			<Grid container alignItems="stretch" height="calc(100vh - 60px)">
				<Grid item xs={2} sx={{ overflowX: "hidden" }}>
					<h2 style={{ textAlign: "center", margin: "20px" }}>
						Keskustelut
					</h2>
					<Stack>
						{noMatches && (
							<h3 style={{ textAlign: "center" }}>
								Sinulla ei ole matcheja
							</h3>
						)}
						{error && (
							<Alert variant="filled" severity="error">
								Jokin meni pieleen
							</Alert>
						)}
						{matches.map((match) => (
							<MatchCard
								key={match.id}
								setTheirItems={setTheirItems}
								theirItems={theirItems}
								userId={userId}
								activeLikerUserId={match.activeLikerUserId}
								targetOfLikeUserId={match.targetOfLikeUserId}
								activeLikerUsername={match.activeLikerUsername}
								targetOfLikeUsername={
									match.targetOfLikeUsername
								}
								passMatchData={passMatchData}
								passUsername={passUsername}
								matchData={match}
							/>
						))}
					</Stack>
				</Grid>
				<Grid
					item
					xs={7}
					sx={{
						p: 2,
						borderRight: "1px solid",
						borderLeft: "1px solid"
					}}
				>
					{showChat && (
						<Box>
							<ChatForm
								matchId={matchData.id}
								matchedWith={matchUsername}
							/>
						</Box>
					)}
				</Grid>
				<Grid
					item
					xs={3}
					sx={{
						textAlign: "center",
						overflowX: "hidden"
					}}
				>
					{theirItems && myItems && (
						<>
							<Stack sx={{ alignItems: "center" }}>
								<h2 style={{ margin: "20px" }}>
									Vaihtotavarat
								</h2>
								<h4
									style={{
										margin: "10px",
										alignSelf: "start"
									}}
								>
									Hänen tuotteet:
								</h4>
								{theirItems.map((item) => (
									<ItemPostSmall
										id={item.id}
										key={item.id}
										title={item.title}
										mini={true}
										itemPost={item}
									/>
								))}
								<h4
									style={{
										margin: "10px",
										alignSelf: "start"
									}}
								>
									Sinun tuotteet:
								</h4>
								{myItems.map((item) => (
									<ItemPostSmall
										id={item.id}
										key={item.id}
										title={item.title}
										mini={true}
										itemPost={item}
									/>
								))}
							</Stack>
						</>
					)}
				</Grid>
			</Grid>
		</>
	);
};
export default ChatContainer;
