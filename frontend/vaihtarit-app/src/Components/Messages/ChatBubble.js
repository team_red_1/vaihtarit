import React from "react";
import { Paper } from "@mui/material";
import { Box } from "@mui/system";

export const ChatBubble = ({ message, checkIfUser, stamp, receiver }) => {
	const style = {
		cardSide: {
			display: "flex",
			justifyContent: checkIfUser ? "flex-start" : "flex-end"
		},
		bubbleDisplay: {
			flexFlow: "column nowrap",
			minWidth: "45vh",
			maxWidth: "70vh",
			minHeight: "2em",
			backgroundColor: checkIfUser ? "#00FF5C" : "#0FCF6B",
			marginBottom: "5px",
			marginRight: "3px",
			borderRadius: checkIfUser ? "1em 1em 1em 0em" : "1em 1em 0em 1em"
		},
		bubbleMessage: {
			padding: "0.8em"
		},
		bubbleWatermark: {
			float: "right",
			fontSize: 10,
			color: "gray",
			padding: "0.5em",
			marginRight: "1em",
			marginBottom: "0.5em"
		}
	};

	return (
		<Box sx={style.cardSide}>
			<Paper elevation={3} sx={style.bubbleDisplay}>
				<Box sx={style.bubbleMessage}>{message}</Box>
				<Box sx={style.bubbleWatermark}>
					{stamp} {receiver}
				</Box>
			</Paper>
		</Box>
	);
};
