/* eslint-disable react/prop-types */
import React, { createContext, useState } from "react";

export const UserContext = createContext();

export const UserProvider = ({ children }) => {
	const [username, setUsername] = useState(null);
	const [email, setEmail] = useState(null);
	const [isLogged, setIsLogged] = useState(false);
	const [accessToken, setAccessToken] = useState(null);
	const [admin, setAdmin] = useState(null); // ToDo: not in mvp
	const [userId, setUserId] = useState(null);
	const [showLoginModal, setShowLoginModal] = useState(false);
	const [showRegisterModal, setShowRegisterModal] = useState(false);
	const [openFilters, setOpenFilters] = useState(false);
	const [otherUserId, setOtherUserId] = useState(false);
	const [otherUserName, setOtherUserName] = useState(false);
	const [filterAnchorEl, setFilterAnchorEl] = useState(false);

	const loginUser = (accessToken, id, username, loggedIn) => {
		setAccessToken(accessToken);
		setUserId(parseInt(id));
		setUsername(username);
		setIsLogged(loggedIn);
	};

	const saveUserToLocalStorage = (accessToken, id, username) => {
		localStorage.setItem("accessToken", accessToken);
		localStorage.setItem("id", id);
		localStorage.setItem("username", username);
	};

	const logout = () => {
		setUsername(null);
		setEmail(null);
		setIsLogged(false);
		setAccessToken(null);
		setUserId(null);
		localStorage.clear();
	};

	return (
		<UserContext.Provider
			value={{
				username,
				setUsername,
				email,
				isLogged,
				setIsLogged,
				accessToken,
				setAccessToken,
				admin,
				setAdmin,
				userId,
				setUserId,
				loginUser,
				saveUserToLocalStorage,
				showLoginModal,
				showRegisterModal,
				setShowLoginModal,
				setShowRegisterModal,
				logout,
				openFilters,
				setOpenFilters,
				otherUserId,
				setOtherUserId,
				otherUserName,
				setOtherUserName,
				filterAnchorEl,
				setFilterAnchorEl
			}}
		>
			{children}
		</UserContext.Provider>
	);
};
