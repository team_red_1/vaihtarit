import React, { useEffect, useContext, useState } from "react";
import { ItemPostSmall } from "../Item/ItemPostSmall/ItemPostSmall";
import { Alert, Stack } from "@mui/material";
import axios from "axios";
import { UserContext } from "../UserContext";

export const MyPosts = () => {
	const { userId, accessToken, userCreatedNewPost } = useContext(UserContext);
	const [posts, setPosts] = useState([]);
	const [error, setError] = useState(false);

	const deleteItemPost = (itemId) => {
		axios({
			method: "delete",
			url: `https://localhost:5100/api/Item/${itemId}`,
			headers: {
				Authorization: "Bearer " + accessToken,
				"Content-type": "application/json; charset=utf-8"
			}
		})
			.then(() => {
				const postsNew = posts.filter((post) => post.id !== itemId);
				setPosts(postsNew);
			})
			.catch(() => setError(true));
	};

	useEffect(() => {
		axios({
			method: "get",
			url: `https://localhost:5100/api/Item/user-items/${userId}`,
			headers: {
				Authorization: "Bearer " + accessToken,
				"Content-type": "application/json; charset=utf-8"
			}
		})
			.then((res) => {
				if (res.data !== "") setPosts(res.data);
			})
			.catch(() => setError(true));
	}, [userCreatedNewPost]);

	return (
		<>
			<Stack alignItems="center" spacing={2} margin="20px">
				<h1 style={{ marginTop: "10px" }}>
					Omat ilmoitukset
				</h1>
				{!error && posts.length === 0 && (
					<div>Et ole luonut yhtään ilmoitusta</div>
				)}
				{!error &&
					posts.length > 0 &&
					posts.map((post) => (
						<ItemPostSmall
							key={post.id}
							id={post.id}
							title={post.title}
							description={post.description}
							deleteItemPost={deleteItemPost}
							deleteButton={true}
							itemPost={post}
						/>
					))}
				{error && (
					<Alert variant="filled" severity="error">
						Jokin meni pieleen
					</Alert>
				)}
			</Stack>
		</>
	);
};
