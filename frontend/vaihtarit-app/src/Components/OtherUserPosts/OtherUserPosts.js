import React, { useEffect, useContext, useState } from "react";
import { ItemPostSmall } from "../Item/ItemPostSmall/ItemPostSmall";
import { Alert, Stack } from "@mui/material";
import axios from "axios";
import { UserContext } from "../UserContext";

export const OtherUserPosts = () => {
	const { otherUserId, accessToken, otherUserName } = useContext(UserContext);
	const [posts, setPosts] = useState([]);
	const [error, setError] = useState(false);

	useEffect(() => {
		setPosts([]);
		axios({
			method: "get",
			url: `https://localhost:5100/api/Item/user-items/${otherUserId}`,
			headers: {
				Authorization: "Bearer " + accessToken,
				"Content-type": "application/json; charset=utf-8"
			}
		})
			.then((res) => {
				if (res.data !== "") setPosts(res.data);
			})
			.catch(() => setError(true));
	}, [otherUserId]);

	return (
		<>
			<Stack alignItems="center" spacing={2}>
				<h1 style={{ marginTop: "30px" }}>
					Käyttäjän {otherUserName} ilmoitukset
				</h1>
				{!error && posts.length === 0 && (
					<div>{otherUserName} ei ole luonut yhtään ilmoitusta</div>
				)}
				{!error &&
					posts.length > 0 &&
					posts.map((post) => (
						<ItemPostSmall
							key={post.id}
							id={post.id}
							title={post.title}
							description={post.description}
							itemLikeButton={true}
							itemPost={post}
						/>
					))}
				{error && (
					<Alert variant="filled" severity="error">
						Jokin meni pieleen
					</Alert>
				)}
			</Stack>
		</>
	);
};
