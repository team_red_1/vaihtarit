import React, { useContext, useEffect } from "react";
import { BrowserRouter } from "react-router-dom";
import { Header } from "./Header/Header";
import { LoginForm } from "./LoginRegister/LoginForm";
import Router from "./Router";
import { UserContext } from "./UserContext";
import { RegisterForm } from "./LoginRegister/RegisterForm";
import SnackbarProvider from "./Snackbar/Snackbar";

const AppContainer = () => {
	const { loginUser } = useContext(UserContext);

	useEffect(() => {
		if (localStorage.getItem("accessToken") !== null)
			loginUser(
				localStorage.getItem("accessToken"),
				localStorage.getItem("id"),
				localStorage.getItem("username"),
				true
			);
	}, []);

	return (
		<>
			<LoginForm />
			<RegisterForm />
			<BrowserRouter>
				<Header />
				<Router />
			</BrowserRouter>
			<SnackbarProvider />
		</>
	);
};

export default AppContainer;
