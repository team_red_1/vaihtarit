import React from "react";
import { Routes, Route } from "react-router-dom";
import UserSettings from "./UserSettings/UserSettings";
import { SecureRoute } from "./SecureRoute";
import { AccountConfirmed } from "./AccountConfirmed/AccountConfirmed";
import { ItemPostFeed } from "./Item/ItemPostFeed/ItemPostFeed";
import { MyPosts } from "./MyPosts/MyPosts";
import ChatContainer from "./Messages/ChatContainer";
import { MyLikes } from "./MyLikes/MyLikes";
import { OtherUserPosts } from "./OtherUserPosts/OtherUserPosts";

const reactRouter = () => (
	<Routes>
		<Route
			path="/usersettings"
			element={
				<SecureRoute redirectTo="/">
					<UserSettings />
				</SecureRoute>
			}
		/>
		<Route
			path="/myposts"
			element={
				<SecureRoute redirectTo="/">
					<MyPosts />
				</SecureRoute>
			}
		/>
		<Route
			path="/messages"
			element={
				<SecureRoute redirectTo="/">
					<ChatContainer />
				</SecureRoute>
			}
		/>
		<Route
			path="/mylikes"
			element={
				<SecureRoute redirectTo="/">
					<MyLikes />
				</SecureRoute>
			}
		/>
		<Route
			path="/otheruserposts"
			element={
				<SecureRoute redirectTo="/">
					<OtherUserPosts />
				</SecureRoute>
			}
		/>
		<Route path="/" element={<ItemPostFeed />} />
		<Route path="/accountconfirmed/:id" element={<AccountConfirmed />} />
		<Route path="*" element={<></>} />
	</Routes>
);

export default reactRouter;
