cd backend
dotnet ef database drop
cd..
cd DatabaseConnection
del Migrations
cd ..
cd backend
dotnet ef migrations add Initial -p "../DatabaseConnection/DatabaseConnection.csproj"
dotnet ef database update
dotnet run