# Vaihtarit

ASP.Net Core(6.0) with a React app frontend

A service for users to easily trade their old and unused items to other users and receive other items of their interest in return. The users communicate by sending messages to finalized the details of the exchange

## Features:

-   User account creation with email confirmation with a random string token
-   Item post creation for items a user wishes to trade
-   The post contains one or more images of the item, a free description, description of the condition and the location
-   An item post feed with infinite scroll functionality that shows all items in the database
-   Search functionality for items based on name, category, price window, condition
-   Users can like other users' items to show their interest for trading
-   Match generation after two users have liked each other's items
-   Messaging functionality between the two users in a match where they can discuss the particulars of the item exchange
-   Notifications for the user when an item they posted receives a like, a new match is generated or when they receive a message

## Example images of basic functionality

Create a user ![](ReadMeImages/createUser.png)
-  User registration is simple, username is limited to 20 characters, @ . - _ are allowed characters
Create an item ![](ReadMeImages/createItem.png)
- An item is created after a user is logged in
Inspect an item ![](ReadMeImages/inspectItem.png)
- An item can be inspected by clicking the item entry
Item feed with infinite scroll ![](ReadMeImages/itemFeed.png)
- Item posts are automatically pulled from the database for viewing as the user scrolls down
Search for items ![](ReadMeImages/search.png)
- items can be searched for by name, category, condition, etc..
Messages between users in a match ![](ReadMeImages/messages.png)
- after two users are in a match they can communicate with messages
Notifications for a user ![](ReadMeImages/notifications.png)
- users receive notifications about liked items, matches and messages received
New message from another user ![](ReadMeImages/notificationsMessage.png)
- a message was received from another user

## Frontend features:

-  User can create an account, log in, change their password and email and remove their account in a user settings-page
-  A user can create a post of an item they would like to trade for another item or give it away
-  The post contains one image of the item, a title, a free description field, a category, an estimation of value and the item's condition
-  Category, condition and value are selected from menus (all the properties come from backend)
-  Users can remove their own posts
-  User can browse all posts in a scrollable "infinite feed"
-  The feed can be filtered with a form containing text fields for title and description, menu for category and sliders for value and condition
-  Users can submit "likes" by clicking the heart icon button attached to the item posts by other users
-  The "likes" can also be removed from the same button
-  When a user receives a like or a message from another user or two users have liked each others' item posts (called a match), the user will get a notification and the bell icon changes to an animated version
-  The amount of new notifications is fetched from backend and is shown with a badge counter on the bell icon
-  User can see all their notifications in a list by clicking the bell icon button
-  Frontend is repeatedly getting the notification data from backend
-  All new notifications are marked with "Uusi" ("New")
-  When a like-notification is clicked, the user will be taken to a page where they can see a list of all item posts by the liker and submit likes to their posts (if they wish) and make a match
-  When the message- and match-notifications are clicked, the user will be taken to the messages-page where they can find all the matches on the left of the screen
-  When clicking a match, a chat view and a text form is shown and now the user can send messages to the other participant in the match
-  All the messages are being fetched repeatedly from backend and displayed with chat bubbles 
-  The user can see item posts from both participants on the right side
-  All features have implemented error handling and displaying an error message to the user if needed

## Backend features:

-  Database with PostgreSQL that stores all relevant information
-  Endpoints to access and modify any needed data with authentication and authorization functionality with JWT tokens
-  Password hashing with BCrypt.Net, includes randomly generated salt. Current settings are SHA384 and workfactor 11
-  A BaseController that is inherited by other Controllers to reduce duplication of Dependency Injections
-  Snowflake ID-generator
-  Enums for Database Errors, User Management and Validation Errors
-  Partially implemented Geolocation to allow users to set locations for themselves or their items
-  Custom EntityMapper to aid in changing object types when needed
-  Email service that provides a verification token to an email address to confirm user registration
-  Password validator to validate password to our standards
-  Users can upload an image when creating an item
-  Item categories for item creation, requires data injection with the use of GET "api/User/database" route
-  UserController for the creation, authentication, email confirmation, retrieval or modification of a user or users
-  LikedItemController controls the management of likes that users give to items. Also relevant notification creation and retrieval of match data information per user.
-  Usage of Dtos instead of Entities to limit the visibility of personal data
-  ItemController controls the management of items and enables the "infinite feed" of items
-  ImageController to control uploading images to project folder
-  MatchMaking functionality to enable generation of matches between two users that have liked an item the other has created
-  Needed connection strings and other information is stored in an AppSettings-file not in the project repository
-  RandomEntityGenerator that fills the database with dummy data, also creates the categories (GET "api/User/database")
-  Database related services and functions separated from the original project to DatabaseConnection to try to limit unneeded complexity and enable further functionality  
-  MessageController to enable getting and posting new messages relating to a match between two users
-  NotificationController to provide the frontend with notifications about a User's liked items, possible matches and messages

## Endpoints

https://gitlab.com/team_red_1/vaihtarit/-/wikis/Backend-Endpoints

## Future plans for the project:

Frontend
-  Clicking on item image inside modal window opens up a full size image
-  Dark theme mode
-  Hiding account and users posts
-  Hide filter icon from header when not on homepage
-  “Shopping cart” or another system on chat view instead of showing all items from matched users.
-  Last chat message on MatchCards
-  Sending images in a chat
-  Uploading more than one image on item posts
-  Editing posts
-  Reporting posts
-  Item location implementation
-  item exchange process finalization after both parties are happy and agreed to trade 

Backend
- Users can approve or deny any matches that are generated
- User can upload multiple images to their item post
- When users in a Match agree to a trade, the match entry will be marked as closed in the database
- Proper implementation of a Geolocation component to enable users in a match to compare the distance between their locations
- Refining the look of the site
- Admin rights and functions for managing categories, banning users, removing item posts etc.
- Further email functions to, for example, enable recovery of a forgotten password, get notifications about likes, matches and messages
- Functionality for hard deletion of all relevant data every * days

## Frontend install & run instructions:

git clone https://gitlab.com/team_red_1/vaihtarit.git

    cd vaihtarit/frontend/vaihtarit-app/

    npm install

    npm start

Prettier: Install Prettier VSCode extension. Make sure that the config path in Prettier settings is empty and open

the project with VSCode in vaihtarit/frontend/vaihtarit-app/ folder. This will allow Prettier to load the specific

configurations for the project and the code formatting will be done correctly.

ESLint: Install ESLint extension to VSCode

## Backend install & run instructions:

Install .NET 6 SDK.
Install PostgreSQL. Make note of the database password during installing!

Edit appsettings.EXAMPLE.json to match your system settings. Primarily the connection string for connection to PostgreSQL database.

To initalize the database, EntityFramework tools are needed. Type in the command prompt you use:

    dotnet tool install --global dotnet-ef

If you want to update the tools to a newer version, type:

    dotnet tool update --global dotnet-ef

Run createDatabase.bat in the project root folder (vaihtarit) to create a new database in a more efficient manner. The command does the following:
    1. Navigates to the backend-folder
    2. drops the database (if it exists)
    3. Prompts the user to press Y and then Enter
    4. navigates to the DatabaseConnection-folder and deletes Migrations-folder that contains existing Migrations
    5. Prompts the user to press Y and then Enter
    6. Navigates back to the backend-folder and creates a new Migration named Initial
    7. Updates the database
    8. Starts the backend with the command "dotnet run"

You can also do everything manually by following the instructions below

To initialize the database, type the following while in the backend folder "vaihtarit\backend". If you are using anything other than Windows, replace any "\" with "/" in the command:

    dotnet ef migrations add yourMigrationName -p "..\DatabaseConnection\DatabaseConnection.csproj"

    dotnet ef database update

If you need to drop(delete) the database type the following in "vaihtarit\backend":

    dotnet ef database drop

To delete an existing Migration, navigate to "vaihtarit\DatabaseConnection\" and type the following:

    del Migrations

Confirm by pressing "Y" and Enter when prompted

To run the backend in backend folder:

    dotnet run